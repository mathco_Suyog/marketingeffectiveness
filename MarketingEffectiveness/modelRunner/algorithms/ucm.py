from datetime import datetime
import datetime
import pandas as pd
import numpy as np
import itertools
import statsmodels.api as sm
import impyute as impy
from modelRunner.models.fc_schema import ModelForecastMetrics, ModelForecasts
from statsmodels.tsa.statespace.structural import UnobservedComponents
from ..log_helpers import request_add_log_message
from ..modelRunException import MAPEException, DataSufficiencyException, NoOptimalSolutionException


class UCM(object):
    @staticmethod
    def grid_search_UCM(self,dep_var):
        """
        This function runs a grid search for UCM
        """
        s_params = [7, 14, 30]
        ar_params = list(range(1, 8))  # [1,2,3,4,5,6,7]
        # Generate all different combinations of s, and ar cuplets   
        models = list(itertools.product(s_params, ar_params))
        config = None
        least_error = np.inf
        # Create config instances
        for s, ar in models:
            cfg = [s, ar]
            try:
                modelFit = UnobservedComponents(self[dep_var], trend=True, seasonal=s, exog=None, freq_seasonal=None,
                                                cycle=False, autoregressive=ar, irregular=True,
                                                stochastic_level=False,stochastic_trend=False, stochastic_seasonal=True,
                                                stochastic_freq_seasonal=None,stochastic_cycle=False,
                                                damped_cycle=False, cycle_period_bounds=None, mle_regression=None,
                                                start_params=None, transformed=True, cov_type='opg', cov_kwds=None,
                                                method='lbfgs', maxiter=50, full_output=1, disp=5, callback=None,
                                                return_params=False, optim_score=None, optim_complex_step=None,
                                                optim_hessian=None, flags=None).fit()
                self['predicted_kpi'] = modelFit.predict(start=0, end=len(self))

                current_error = np.mean(np.abs((self[dep_var]-self['predicted_kpi']) / self[dep_var]))
                if (current_error < least_error) and (current_error > 0.1):
                    least_error = current_error
                    config = cfg
            except Exception:
                continue

        if config is None:
            raise NoOptimalSolutionException(len(models))

        return config, least_error
        
    @staticmethod
    def model_fit_UCM(self, depVar=None, trend=True, seasonal=None, exog=None, freq_seasonal=None, cycle=False, 
                      autoregressive=None, irregular=True, stochastic_level=False, stochastic_trend=False, 
                      stochastic_seasonal=True, stochastic_freq_seasonal=None, stochastic_cycle=False, 
                      damped_cycle=False, cycle_period_bounds=None, mle_regression=True, start_params=None, 
                      transformed=True, cov_type='opg', cov_kwds=None, method='lbfgs', maxiter=50, 
                      full_output=1, disp=5, callback=None, return_params=False, optim_score=None, 
                      optim_complex_step=None, optim_hessian=None, flags=None):
        """
        This function runs UCM and returns the model fit
        """
        

        model_fit = UnobservedComponents(self[depVar], trend=trend, exog=exog, seasonal=seasonal, 
                                    freq_seasonal=freq_seasonal, cycle=cycle, autoregressive=autoregressive, 
                                    irregular=irregular, stochastic_level=stochastic_level, 
                                    stochastic_trend=stochastic_trend, stochastic_seasonal=stochastic_seasonal, 
                                    stochastic_freq_seasonal=stochastic_freq_seasonal, stochastic_cycle=stochastic_cycle, 
                                    damped_cycle=damped_cycle, cycle_period_bounds=cycle_period_bounds, 
                                    mle_regression=mle_regression)
        return (model_fit.fit(method='powell', disp=False))
    
    def mape(self,dep_var,model_fit):
        """
        This function runs moving average for test
        """
   
        residuals = model_fit.resid
        mape = np.mean(np.abs((residuals)/self[dep_var]))
        return(mape)
    
    def train_pred_UCM(self,dep_var,model_fit):
        """
        The predicted values of the train data
        """
        self['predicted_kpi']=model_fit.predict(start=0)
        mape = np.mean(np.abs((self[dep_var]-self['predicted_kpi']) / self[dep_var]))
        return (self,mape)

    @staticmethod
    def test_pred_UCM(self,dep_var,model_fit):

        """
        The predicted values on the test data
        """
        self['predicted']=np.array(model_fit.forecast(len(self)))
        mape = np.mean(np.abs((self[dep_var]-self['predicted']) / self[dep_var]))
        return (self)

    @staticmethod
    def decomposition_function(df,adjusted_kpi):
        """
        This function splits the data into trend, seasonal and random components
        """
        df_series = pd.Series(data=df[adjusted_kpi])
        numpyArrayKPI = np.array(df_series.resample('D').sum(),dtype=float)
        decomposition = sm.tsa.seasonal_decompose(numpyArrayKPI, model='multiplicative',freq=365)
        return(decomposition)

    @staticmethod
    def trend_resid_data(df,trend_series,residual_series,seasonal_series,observed_series):
        """
        This is used to get the data with just trend and residuals
        """
        # Multiplicative Seasonality            
        df_trend_resid = pd.DataFrame({'trend_residual':observed_series/seasonal_series, 'observed':observed_series})
        # Imputing the data using KNN imputation            
        # df_trend_resid = round(impy.fast_knn(df_trend_resid,k=3),0)
        # df_trend_resid.columns = ["observed","trend_residual"]
        df['train_date'] = df.index
        df_trend_resid = pd.concat([df.reset_index(drop=True),df_trend_resid],axis=1)
        df_trend_resid = df_trend_resid.set_index('train_date')
        return(df_trend_resid)

    @staticmethod
    def seasonalizing_data(df,predicted,seasonal_series):
        """
        Adding seasonality back into the data after predicting
        """
        df.insert(0, 'merge_key', range(0,len(df)))
        seasonal_comp = pd.DataFrame({'seasonal':seasonal_series})
        seasonal_comp.insert(0, 'merge_key', range(0,len(seasonal_comp)))
        df =  df.join(seasonal_comp,on=['merge_key'], how='left',lsuffix='_left', rsuffix='_right')
        # Multiplicative Seasonality
        df['predicted_kpi'] =  df[predicted] * df.seasonal
        return(df)

    @staticmethod
    def residual_approach(test_data):
        """
        Implementing residual approach
        """
        residual_df = test_data.groupby(['period'],as_index = False)['adjusted_kpi','predicted_kpi'].sum()
        test_residual_factor_df = residual_df[residual_df['period']=='Pre']
        residual_factor = (test_residual_factor_df['adjusted_kpi'].iloc[0] - test_residual_factor_df['predicted_kpi'].iloc[0]) / test_residual_factor_df['predicted_kpi'].iloc[0]
        test_absolute_value = residual_df[residual_df['period']=='Post']
        absolute_value = sum(test_absolute_value['predicted_kpi'], (test_absolute_value['predicted_kpi']*residual_factor))
        test_data['pct_contribution'] = test_data['predicted_kpi']/test_absolute_value['predicted_kpi'].sum()
        for i in range(len(test_data)):
            if test_data['period'][i] == 'Pre':
                test_data['pct_contribution'][i] = np.NaN
        test_data['absolute_value'] = absolute_value.iloc[0]
        test_data['pred_kpi'] = test_data['pct_contribution'] * test_data['absolute_value']
        test_data_pre = test_data[test_data['period']=='Pre']
        test_data_post = test_data[test_data['period']=='Post']
        return test_data_pre, test_data_post    

    @staticmethod
    def uplift(test_data):
        """
        A function to calculate the total lift in units (both positive or negative) for test timeframe
        """
        uplift = test_data['adjusted_kpi'].sum()-test_data['pred_kpi'].sum()
        return uplift

    @staticmethod
    def lift_pct(test_data):
        """
        A function to calculate the total lift % (both positive or negative) for test timeframe
        """
        lift = (test_data['adjusted_kpi'].sum()-test_data['pred_kpi'].sum())/test_data['pred_kpi'].sum()
        return lift

def ucm_code(train_data,test_data,data_sufficiency_key,train_data_adj,session):

    request_id = train_data.request_id.iloc[0]
    if data_sufficiency_key != 'Pass':
        raise DataSufficiencyException()

    model_train_ucm = train_data.copy()
    model_test_ucm = test_data.copy()

    # Setting index to test and train data
    model_train_ucm = model_train_ucm.set_index('train_date')
    model_test_ucm = model_test_ucm.set_index('test_date')

    # Decomposing the data into trend seasonality and irregularity
    decompose = UCM.decomposition_function(model_train_ucm,'adjusted_kpi')
    df_trend_resid = UCM.trend_resid_data(model_train_ucm,decompose.trend,decompose.resid,decompose.seasonal,
                                    decompose.observed)

    # Running UCM for the trend and seasonal components
    best_param,mape_ucm = UCM.grid_search_UCM(df_trend_resid,'trend_residual')
    model_fit = UCM.model_fit_UCM(df_trend_resid, 'trend_residual',seasonal=best_param[0],autoregressive=best_param[1])
    test_pred_trend_resid = UCM.test_pred_UCM(model_test_ucm,'adjusted_kpi',model_fit)
    model_test_ucm = UCM.seasonalizing_data(test_pred_trend_resid,'predicted',decompose.seasonal)

    # Residual component correction
    model_test_ucm_pre, model_test_ucm_post = UCM.residual_approach(model_test_ucm)

    confidence = ''
    # Confidence Level
    if mape_ucm > 0.3:
        confidence = 'Low'
    else:
        confidence = 'High'

    # Metric calculation
    uplift = UCM.uplift(model_test_ucm_post)
    lift = UCM.lift_pct(model_test_ucm_post)
    avg_lift = uplift/len(model_test_ucm_post)
    KPI = model_train_ucm['performance_metric'][0]

    param_mapping = {
        'total_sales': 'SALES',
        'total_customer': 'CUSTOMER COUNT',
        'total_orders': 'ORDER COUNT',
        'total_visits': 'VISIT COUNT',
        'conversion rate': 'CONVERSION RATE'
    }

    units_mapping = {
        'total_sales': 'USD',
        'conversion rate': 'PERCENTAGE'
    }

    METRICS = param_mapping.get(KPI, '')
    UNITS = units_mapping.get(KPI, '')

    model_results = pd.DataFrame(columns=['ID',
                                          'REQUEST_ID',
                                          'FORECAST_AVG_LIFT',
                                          'FORECAST_AVG_TRAIN_ERROR',
                                          'FORECAST_TECHNIQUE',
                                          '_ROW_CREATED_AT',
                                          '_ROW_UPDATED_AT',
                                          'FORECAST_ACTUALS',
                                          'FORECAST_EXPECTED',
                                          'CONFIDENCE',
                                          'ABS_UPLIFT',
                                          'AVG_LIFT_PER_DAY'])

    model_graph_train = pd.DataFrame(columns=['ID',
                                             'REQUEST_ID',
                                             'ACTUAL_VALUE',
                                             'FORECAST_DATE',
                                             'FORECAST_PARAM',
                                             'FORECAST_TECHNIQUE',
                                             'FORECAST_UNITS',
                                             'FORECAST_VALUE',
                                             '_ROW_CREATED_AT',
                                             '_ROW_UPDATED_AT',
                                             'FORECAST_RESIDUAL_VALUE'])

    model_graph_test = pd.DataFrame(columns=['ID',
                                             'REQUEST_ID',
                                             'ACTUAL_VALUE',
                                             'FORECAST_DATE',
                                             'FORECAST_PARAM',
                                             'FORECAST_TECHNIQUE',
                                             'FORECAST_UNITS',
                                             'FORECAST_VALUE',
                                             '_ROW_CREATED_AT',
                                             '_ROW_UPDATED_AT',
                                             'FORECAST_RESIDUAL_VALUE'])

    if mape_ucm >= 0.5:
        raise MAPEException(mape_ucm)

    model_results = model_results.append({'ID':list(range(0 , len(model_graph_train))),
                                          'REQUEST_ID':train_data_adj['request_id'][0],
                                          'FORECAST_AVG_LIFT':lift,
                                          'FORECAST_AVG_TRAIN_ERROR':1-mape_ucm,
                                          'FORECAST_TECHNIQUE':'UCM',
                                          '_ROW_CREATED_AT':datetime.datetime.utcnow(),
                                          '_ROW_UPDATED_AT':datetime.datetime.utcnow(),
                                          'FORECAST_ACTUALS':model_test_ucm_post['adjusted_kpi'].sum(),
                                          'FORECAST_EXPECTED':model_test_ucm_post['pred_kpi'].sum(),
                                          'CONFIDENCE':confidence,
                                          'ABS_UPLIFT': uplift,
                                          'AVG_LIFT_PER_DAY':avg_lift
                                        },ignore_index=True)

    model_graph_train['ACTUAL_VALUE'] = model_test_ucm_pre['adjusted_kpi']
    model_graph_train['FORECAST_DATE'] = model_test_ucm_pre.index.values
    model_graph_train['REQUEST_ID'] = model_test_ucm_pre['request_id'][0]
    model_graph_train['FORECAST_PARAM'] = METRICS
    model_graph_train['FORECAST_TECHNIQUE'] = 'UCM'
    model_graph_train['FORECAST_UNITS'] = UNITS
    model_graph_train['_ROW_CREATED_AT'] = str(datetime.datetime.utcnow())
    model_graph_train['_ROW_UPDATED_AT'] = str(datetime.datetime.utcnow())
    model_graph_train['FORECAST_VALUE'] = model_test_ucm_pre['predicted_kpi']
    model_graph_train['FORECAST_RESIDUAL_VALUE'] = None

    model_graph_test['ACTUAL_VALUE'] = model_test_ucm_post['adjusted_kpi']
    model_graph_test['FORECAST_DATE'] = model_test_ucm_post.index.values
    model_graph_test['REQUEST_ID'] = model_test_ucm_post['request_id'][0]
    model_graph_test['FORECAST_PARAM'] = METRICS
    model_graph_test['FORECAST_TECHNIQUE'] = 'UCM'
    model_graph_test['FORECAST_UNITS'] = UNITS
    model_graph_test['_ROW_CREATED_AT'] = str(datetime.datetime.utcnow())
    model_graph_test['_ROW_UPDATED_AT'] = str(datetime.datetime.utcnow())
    model_graph_test['FORECAST_VALUE'] = model_test_ucm_post['predicted_kpi']
    model_graph_test['FORECAST_RESIDUAL_VALUE'] = model_test_ucm_post['pred_kpi']

    model_graph = model_graph_train.append(model_graph_test,ignore_index=True)

    for row in model_results.itertuples():
        row_forecast_metrics = ModelForecastMetrics(
            request_id = row.REQUEST_ID,
            forecast_avg_lift = row.FORECAST_AVG_LIFT,
            forecast_avg_train_error = row.FORECAST_AVG_TRAIN_ERROR,
            forecast_technique = row.FORECAST_TECHNIQUE,
            _row_created_at = datetime.datetime.utcnow(),
            _row_updated_at = datetime.datetime.utcnow(),
            forecast_actuals = row.FORECAST_ACTUALS,
            forecast_expected = row.FORECAST_EXPECTED,
            confidence = row.CONFIDENCE,
            abs_uplift = row.ABS_UPLIFT,
            forecast_avg_per_day = row.AVG_LIFT_PER_DAY
        )

        session.add(row_forecast_metrics)

    model_graph_rows = []
    for row in model_graph.itertuples():
        row_forecast_values = ModelForecasts(
            request_id = row.REQUEST_ID,
            actual_value = row.ACTUAL_VALUE,
            forecast_date = datetime.datetime.strptime(str(row.FORECAST_DATE), '%Y-%m-%d %H:%M:%S'),
            forecast_param = row.FORECAST_PARAM,
            forecast_technique = row.FORECAST_TECHNIQUE,
            forecast_units = row.FORECAST_UNITS,
            forecast_value = row.FORECAST_VALUE,

            _row_created_at = datetime.datetime.utcnow(),
            _row_updated_at = datetime.datetime.utcnow(),
            forecast_residual_value = row.FORECAST_RESIDUAL_VALUE
        )

        model_graph_rows.append(row_forecast_values)
    session.bulk_save_objects(model_graph_rows)
    session.commit()

    # Update request log
    log_message = ('UCM run completed, {} result rows inserted, '
                   'MAPE: {:.2f}'.format(len(model_graph_rows), mape_ucm))
    request_add_log_message(request_id, log_message, 0, session)
