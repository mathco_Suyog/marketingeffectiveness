import json

from fc_schema import VisitFilters, TransactionFilters

def get_regions(event, context, session):
    event_measure = event["queryStringParameters"]["measure_performance"]
    if event_measure.lower() == 'visits':
        query_table = VisitFilters
    else:
        query_table = TransactionFilters

    event_country = event["queryStringParameters"]["country"]

    qry = session.query(query_table.region).filter(query_table.country == event_country).distinct().order_by(query_table.region).all()

    out_list = [ list(x) for x in qry if x[0] is not None ]
    out_list.sort()
    out_list.insert(0, ['All'])

    out_list = json.dumps(out_list, separators=(',',':'))

    response =  {
        'statusCode':200,
        'body': out_list,
        'headers':{},
        'isBase64Encoded':False
        }

    return response

