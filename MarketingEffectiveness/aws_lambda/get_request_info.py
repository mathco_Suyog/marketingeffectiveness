import json

from fc_schema import RequestDetails, RequestStatusCodes, RequestFilters
import arrow


def get_request_status(event, context, session):
    """
    Returns the status code and status message for a 'request_id'
    """
    event_request_id = event["queryStringParameters"]["request_id"]

    qry = (
        session
        .query(RequestDetails.request_id, RequestDetails.status_code, RequestStatusCodes.status_message,
            RequestDetails.models_run_completed, RequestDetails.models_run_total)
        .join(RequestStatusCodes)
        .filter(RequestDetails.request_id == event_request_id)
    )

    request_status = qry.first()
    final_dict = {k:v for k,v in zip(request_status.keys(), request_status)}
    final_dict['progress'] = int(round(final_dict['models_run_completed']/final_dict['models_run_total'], 2) *100)
    if final_dict['status_code'] in (0,3):
        final_dict['progress'] = 100


    response =  {
        'statusCode':200,
        'body': json.dumps(final_dict, separators=(',',':')),
        'headers':{},
        'isBase64Encoded':False
        }

    return response


def get_iterations(event, context, session):
    """
    Returns the last 'n' iterations' requestid, their status and the timestamp when these were created.
    Results are limited to last 100, default 5
    """
    user_id = event["queryStringParameters"]["user_id"]
    iter_count = event["queryStringParameters"].get('n', 10)  # Send 10 iterations by default

    try:
        iter_count = int(iter_count)
    except ValueError:
        iter_count = 10
    
    iter_count = min(iter_count, 100)

    qry = (
        session
        .query(RequestDetails.created_at_ts_utc, RequestDetails.request_id, RequestDetails.status_code, RequestStatusCodes.status_message,
            RequestFilters.performance_metric, RequestFilters.comparison_technique)
        .join(RequestStatusCodes, RequestFilters)
        .filter(RequestDetails.user_id == user_id)
        .order_by(RequestDetails.created_at_ts_utc.desc())
        .limit(iter_count)
    )

    # result_set = qry.all()
    final_dict = {}
    final_list = []

    for i, result in enumerate(qry):
        result_dict = {k: v for k, v in zip(result.keys(), result)}

        arrow_dt = arrow.get(result_dict['created_at_ts_utc'], 'Etc/Greenwich').humanize()
        result_dict['created_at_ts_utc'] = (result_dict['created_at_ts_utc']).strftime('%c')
        result_dict['iter_text'] = f'{i+1:02}) {arrow_dt} ({result_dict["performance_metric"]} / {result_dict["comparison_technique"]}) [{result_dict["status_message"]}]'
        final_dict[i] = result_dict
        final_list.append(result_dict)

    response =  {
        'statusCode':200,
        'body': json.dumps(final_list, separators=(',',':')),
        'headers':{},
        'isBase64Encoded':False
        }

    return response


