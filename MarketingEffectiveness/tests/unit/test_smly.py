from modelRunner.algorithms.testControlSMLY import *
import pytest
from modelRunner.modelRunException import DataSufficiencyException
import pandas as pd
from modelRunner.models.fc_schema import TestControlMetrics, TestControlRecords, TestControlStatsMetrics
from datetime import datetime

@pytest.mark.parametrize('request_id',['6fab34b8-68fa-4be6-853a-9430f8bc0ec4',	'72315a2c-8ab0-45ec-9132-f2abc794c63d',	'f3d239bf-817c-4e3a-9754-ab3ab4fbfa3f',	'16f45d37-cb92-4e2a-90fd-c658b407063f',	'fc13c470-3de0-4b33-8c7b-c2fe3720062f',	'99a1ca30-9236-42f6-8ad4-d92d7832ed6f',	'763e683b-6c82-488e-98c5-c1d04f07b242',	'33fcc407-9d28-4953-99c2-c81ba7aace0a',	'31053d86-8737-49fa-a184-a4748411e110'])
def test_run_smly(session, request_id):
    id_ = request_id
    input_df = pd.read_csv('../data/smly_input.csv')  
    input_df['date'] = pd.to_datetime(input_df['date'])
    input_df = input_df[input_df['request_id']==id_]

    output_metrics = pd.read_csv('../data/smly_metrics.csv')
    output_metrics = output_metrics[output_metrics.request_id ==  id_]

    output_stats = pd.read_csv('../data/smly_stats.csv')
    output_stats = output_stats[output_stats.request_id == id_]

    performanceMetric = input_df.loc[input_df['request_id']==id_,'performance_metric'].iloc[0]
    run_smly(input_df,performanceMetric,session)

    tc_metrics = (
                session
                .query(TestControlMetrics)
                .filter(TestControlMetrics.request_id == id_ )
                .first()
            )

    assert round(tc_metrics.abs_uplift,4) == round(output_metrics.abs_uplift.iloc[0],4)
    assert tc_metrics.abs_lift_per_day == output_metrics.abs_lift_per_day.iloc[0]
    assert tc_metrics.control_pre_vs_post == output_metrics.control_pre_vs_post.iloc[0]
    assert round(tc_metrics.pct_uplift,4) == round(output_metrics.pct_uplift.iloc[0],4)
    assert tc_metrics.t_test_p_value == output_metrics.t_test_p_value.iloc[0]
    assert tc_metrics.test_pre_vs_post == output_metrics.test_pre_vs_post.iloc[0]

    tc_stats_metrics = (
                       session
                       .query(TestControlStatsMetrics)
                       .filter(TestControlStatsMetrics.request_id == id_)
                    )
    stats_df = pd.read_sql(tc_stats_metrics.statement,tc_stats_metrics.session.bind)

    
    assert stats_df.loc[(stats_df['market']=='ThisYear') & (stats_df['period']=='Pre') &
                        (stats_df['metric']=='Mean'),'value'].values[0]\
                         == output_stats.loc[(output_stats['market']=='ThisYear') &
                                             (output_stats['period']=='Pre') &
                                             (output_stats['metric']=='Mean'),'value'].values[0]

    assert stats_df.loc[(stats_df['market']=='ThisYear') & (stats_df['period']=='Pre') &
                        (stats_df['metric']=='SD'),'value'].values[0]\
                         == output_stats.loc[(output_stats['market']=='ThisYear') &
                                             (output_stats['period']=='Pre') &
                                             (output_stats['metric']=='SD'),'value'].values[0]

    assert stats_df.loc[(stats_df['market']=='ThisYear') & (stats_df['period']=='Pre') &
                        (stats_df['metric']=='t-stat'),'value'].values[0]\
                         == output_stats.loc[(output_stats['market']=='ThisYear') &
                                             (output_stats['period']=='Pre') &
                                             (output_stats['metric']=='t-stat'),'value'].values[0]
    
    assert stats_df.loc[(stats_df['market']=='ThisYear') & (stats_df['period']=='Pre') &
                        (stats_df['metric']=='p-value'),'value'].values[0]\
                         == output_stats.loc[(output_stats['market']=='ThisYear') &
                                             (output_stats['period']=='Pre') &
                                             (output_stats['metric']=='p-value'),'value'].values[0]

    assert stats_df.loc[(stats_df['market']=='ThisYear') & (stats_df['period']=='Post') &
                        (stats_df['metric']=='Mean'),'value'].values[0]\
                         == output_stats.loc[(output_stats['market']=='ThisYear') &
                                             (output_stats['period']=='Post') &
                                             (output_stats['metric']=='Mean'),'value'].values[0]

    assert stats_df.loc[(stats_df['market']=='ThisYear') & (stats_df['period']=='Post') &
                        (stats_df['metric']=='SD'),'value'].values[0]\
                         == output_stats.loc[(output_stats['market']=='ThisYear') &
                                             (output_stats['period']=='Post') &
                                             (output_stats['metric']=='SD'),'value'].values[0]

    assert stats_df.loc[(stats_df['market']=='ThisYear') & (stats_df['period']=='Post') &
                        (stats_df['metric']=='t-stat'),'value'].values[0]\
                         == output_stats.loc[(output_stats['market']=='ThisYear') &
                                             (output_stats['period']=='Post') &
                                             (output_stats['metric']=='t-stat'),'value'].values[0]

    assert stats_df.loc[(stats_df['market']=='ThisYear') & (stats_df['period']=='Post') &
                        (stats_df['metric']=='p-value'),'value'].values[0]\
                         == output_stats.loc[(output_stats['market']=='ThisYear') &
                                             (output_stats['period']=='Post') &
                                             (output_stats['metric']=='p-value'),'value'].values[0]

    assert stats_df.loc[(stats_df['market']=='LastYear') & (stats_df['period']=='Pre') &
                        (stats_df['metric']=='Mean'),'value'].values[0]\
                         == output_stats.loc[(output_stats['market']=='LastYear') &
                                             (output_stats['period']=='Pre') &
                                             (output_stats['metric']=='Mean'),'value'].values[0]

    assert stats_df.loc[(stats_df['market']=='LastYear') & (stats_df['period']=='Pre') &
                        (stats_df['metric']=='SD'),'value'].values[0]\
                         == output_stats.loc[(output_stats['market']=='LastYear') &
                                             (output_stats['period']=='Pre') &
                                             (output_stats['metric']=='SD'),'value'].values[0]

    assert stats_df.loc[(stats_df['market']=='LastYear') & (stats_df['period']=='Post') &
                        (stats_df['metric']=='Mean'),'value'].values[0]\
                         == output_stats.loc[(output_stats['market']=='LastYear') &
                                             (output_stats['period']=='Post') &
                                             (output_stats['metric']=='Mean'),'value'].values[0]

    assert stats_df.loc[(stats_df['market']=='LastYear') & (stats_df['period']=='Post') &
                        (stats_df['metric']=='SD'),'value'].values[0]\
                         == output_stats.loc[(output_stats['market']=='LastYear') &
                                             (output_stats['period']=='Post') &
                                             (output_stats['metric']=='SD'),'value'].values[0]


def test_run_sydm_exception():
    with pytest.raises(Exception) as exc_info:
        run_smly(pd.DataFrame(), None, None)
    exc_message = exc_info.value.args[0]
    assert exc_message == "Undefined performance metric encountered."

        
def test_data_exists_smly_exception():
    with pytest.raises(DataSufficiencyException):
        sydm = ThisYear_LastYear(pd.DataFrame(), 'total_customer')
        ThisYear_LastYear.data_exists(sydm.df, sydm.performance_metric)

@pytest.mark.parametrize('request_id',['6fab34b8-68fa-4be6-853a-9430f8bc0ec4',	'72315a2c-8ab0-45ec-9132-f2abc794c63d',	'f3d239bf-817c-4e3a-9754-ab3ab4fbfa3f',	'16f45d37-cb92-4e2a-90fd-c658b407063f',	'fc13c470-3de0-4b33-8c7b-c2fe3720062f',	'99a1ca30-9236-42f6-8ad4-d92d7832ed6f',	'763e683b-6c82-488e-98c5-c1d04f07b242',	'33fcc407-9d28-4953-99c2-c81ba7aace0a',	'31053d86-8737-49fa-a184-a4748411e110'])
def test_smly_data_exists(request_id):
    id_ = request_id
    input_df = pd.read_csv(r"..\data\smly_input.csv")
    input_df['date'] = pd.to_datetime(input_df['date'])
    input_df = input_df[input_df['request_id']==id_]

    performanceMetric = input_df.loc[input_df['request_id']==id_,'performance_metric'].iloc[0]
    perf_met={'Sales':'total_sales','Orders':'total_orders','Customers':'total_customer','Visits':'total_visits'}
    performance_metric=perf_met.get(performanceMetric,None)

    output_records = pd.read_csv(r"..\data\smly_records.csv")
    output_records = output_records[output_records['request_id'] == id_]

    smly = ThisYear_LastYear(input_df, performance_metric)
    df = smly.data_exists(smly.df, smly.performance_metric)
    assert df.shape[0] == output_records.data_exists.values[0]

@pytest.mark.parametrize('request_id',['6fab34b8-68fa-4be6-853a-9430f8bc0ec4',	'72315a2c-8ab0-45ec-9132-f2abc794c63d',	'f3d239bf-817c-4e3a-9754-ab3ab4fbfa3f',	'16f45d37-cb92-4e2a-90fd-c658b407063f',	'fc13c470-3de0-4b33-8c7b-c2fe3720062f',	'99a1ca30-9236-42f6-8ad4-d92d7832ed6f',	'763e683b-6c82-488e-98c5-c1d04f07b242',	'33fcc407-9d28-4953-99c2-c81ba7aace0a',	'31053d86-8737-49fa-a184-a4748411e110'])
def test_smly_subset(request_id):
    id_= request_id
    input_df = pd.read_csv(r"..\data\smly_input.csv")
    input_df['date'] = pd.to_datetime(input_df['date'])
    input_df = input_df[input_df['request_id']==id_]

    output_records = pd.read_csv(r"..\data\smly_records.csv")
    output_records = output_records[output_records['request_id']==id_]

    df_test_pre, df_test_post, df_control_pre, df_control_post = ThisYear_LastYear.subset(input_df)
    assert df_test_pre.shape[0] == output_records.pre_test.values[0]
    assert df_test_post.shape[0] == output_records.post_test.values[0]
    assert df_control_pre.shape[0] == output_records.pre_control.values[0]
    assert df_control_post.shape[0] == output_records.post_control.values[0]

    




    



