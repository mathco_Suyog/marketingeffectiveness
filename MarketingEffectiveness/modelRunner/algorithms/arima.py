from datetime import datetime
import datetime
import pandas as pd
import numpy as np
from modelRunner.models.fc_schema import ModelForecastMetrics
from modelRunner.models.fc_schema import ModelForecasts
import itertools
from statsmodels.tsa.arima_model import ARIMA
from ..log_helpers import request_add_log_message
from ..modelRunException import MAPEException, DataSufficiencyException, NoOptimalSolutionException


class ARIMAfns (object):
    @staticmethod
    def grid_search_ARIMA(df,dep_var,max_p=5,max_d=2,max_q=5,silent=True):
        """
        This function runs a grid search for ARIMA
         """   
        # Initializng the parameters
        p=range(0,max_p)
        d=range(0,max_d)
        q=range(0,max_q)

        # Generate all different combinations of p, q and q triplets
        pdq = list(itertools.product(p, d, q))
        best_mape = np.inf
        best_param = None

        # Gridsearch to look for optimial arima parameters

        for param in pdq:       
            try:
                results = ARIMA(df[dep_var],order=param,exog=None, dates=None, freq='D', missing='none')
                results = results.fit(disp=0)

                residuals = results.resid2
                mape = np.mean( np.abs( residuals / df[dep_var] ) )

                # If current run of MAPE is better than the best one so far, overwrite it
                if (mape < best_mape) and (mape > 0.1):
                    best_mape = mape
                    best_param = param
            except Exception as e:
                continue

        if best_param is None or best_mape == np.inf:
            raise NoOptimalSolutionException(len(pdq))

        return best_param, best_mape
        
    @staticmethod
    def model_fit_arima(df, dep_var, best_param):
        """
        This function runs ARIMA and returns the model fit
        """
        model_fit = ARIMA(df[dep_var], order=(best_param[0],best_param[1],best_param[2]), 
                          exog=None, dates=None, freq='D', missing='none').fit(disp=0)
        return model_fit
    
    def mape(self,dep_var,model_fit):
        """
        This function runs moving average for test
        """
   
        residuals = model_fit.resid
        mape = np.mean(np.abs((residuals)/self[dep_var]))
        return(mape)
    
    def train_pred(self,self_var,model_fit,diff):
        """
        This function predicts for train data
        """
        residuals = model_fit.resid
        if dep_var is not None:
            self['PREDICTED'] =model_fit.predict(start=diff,end=len(self))
        else:
            self['PREDICTED'] =self['adjusted_kpi']-residuals
        return (self)

    @staticmethod
    def test_pred(df,dep_var,model_fit):

        """
        This function forecasts the on test data
        @params: 
            df: Test Data
            dep_var: Independent Variable
            model_fit: fitted ARIMA model
        """

        if dep_var is not None:
            df['predicted_kpi']=model_fit.forecast(df.shape[0])[0] #- This was there.
            # df['predicted_kpi']=np.array(model_fit.forecast(len(df)))
                # except:
                #     pass
            # mape = np.mean(np.abs((df[dep_var]-df['predicted_kpi']) / df[dep_var]))
        else:
            # mape = np.inf
            pass
        return (df)


############################################### ARIMA Model Runner ##########################################################

def arima_code(train_data,test_data,data_sufficiency_key,session):

    request_id = train_data.request_id.iloc[0]
    if data_sufficiency_key != 'Pass':
        raise DataSufficiencyException()

    train_data_arima = train_data.copy()
    test_data_arima = test_data.copy()
    train_data_arima.set_index('train_date', inplace=True)
    test_data_arima.set_index('test_date', inplace=True)

    # Grid search for ARIMA
    best_param, mape_ARIMA = ARIMAfns.grid_search_ARIMA(train_data_arima, 'adjusted_kpi') # ,max_p=6,max_d=2,max_q=6,silent=False)

    model_fit = ARIMAfns.model_fit_arima(train_data_arima,'adjusted_kpi',best_param)

    #predictions = []
    predictions = ARIMAfns.test_pred(test_data_arima,'adjusted_kpi',model_fit)
    # test_data['predicted_kpi'] = predictions
    confidence = ''

    # Confidence Level
    if mape_ARIMA > 0.3:
        confidence = 'Low'
    else:
        confidence = 'High'

    # Metric calculation
    uplift = test_data_arima['adjusted_kpi'].sum()-test_data_arima['predicted_kpi'].sum()
    lift = (test_data_arima['adjusted_kpi'].sum()-test_data_arima['predicted_kpi'].sum())/test_data_arima['predicted_kpi'].sum()
    KPI = train_data_arima['performance_metric'][0]

    param_mapping = {
        'total_sales': 'SALES',
        'total_customer': 'CUSTOMER COUNT',
        'total_orders': 'ORDER COUNT',
        'total_visits': 'VISIT COUNT',
        'conversion rate': 'CONVERSION RATE'
    }

    units_mapping = {
        'total_sales': 'USD',
        'conversion rate': 'PERCENTAGE'
    }

    METRICS = param_mapping.get(KPI, '')
    UNITS = units_mapping.get(KPI, '')


    model_results = pd.DataFrame(columns=['ID',
                                          'REQUEST_ID',
                                          'FORECAST_AVG_LIFT',
                                          'FORECAST_AVG_TRAIN_ERROR',
                                          'FORECAST_TECHNIQUE',
                                          '_ROW_CREATED_AT',
                                          '_ROW_UPDATED_AT',
                                          'FORECAST_ACTUALS',
                                          'FORECAST_EXPECTED',
                                          'CONFIDENCE',
                                          'ABS_UPLIFT'])

    model_graph_train = pd.DataFrame(columns=['ID',
                                             'REQUEST_ID',
                                             'ACTUAL_VALUE',
                                             'FORECAST_DATE',
                                             'FORECAST_PARAM',
                                             'FORECAST_TECHNIQUE',
                                             'FORECAST_UNITS',
                                             'FORECAST_VALUE',
                                             '_ROW_CREATED_AT',
                                             '_ROW_UPDATED_AT'])

    model_graph_test = pd.DataFrame(columns=['ID',
                                             'REQUEST_ID',
                                             'ACTUAL_VALUE',
                                             'FORECAST_DATE',
                                             'FORECAST_PARAM',
                                             'FORECAST_TECHNIQUE',
                                             'FORECAST_UNITS',
                                             'FORECAST_VALUE',
                                             '_ROW_CREATED_AT',
                                             '_ROW_UPDATED_AT'])

    if mape_ARIMA >= 0.5:
        raise MAPEException(mape_ARIMA)

    model_results = model_results.append({'ID':list(range(0 , len(model_graph_train))),
                                          'REQUEST_ID':train_data_arima['request_id'][0],
                                          'FORECAST_AVG_LIFT':lift,
                                          'FORECAST_AVG_TRAIN_ERROR':1-mape_ARIMA,
                                          'FORECAST_TECHNIQUE':'ARIMA',
                                          '_ROW_CREATED_AT':datetime.datetime.utcnow(),
                                          '_ROW_UPDATED_AT':datetime.datetime.utcnow(),
                                          'FORECAST_ACTUALS':test_data_arima['adjusted_kpi'].sum(),
                                          'FORECAST_EXPECTED':test_data_arima['predicted_kpi'].sum(),
                                          'CONFIDENCE':confidence,
                                          'ABS_UPLIFT': uplift
                                        },ignore_index=True)

    model_graph_train['ACTUAL_VALUE'] = train_data_arima[-len(test_data_arima):]['adjusted_kpi']
    model_graph_train['FORECAST_DATE'] = train_data_arima[-len(test_data_arima):].index.values
    model_graph_train['REQUEST_ID'] = train_data_arima['request_id'][0]
    model_graph_train['FORECAST_PARAM'] = METRICS
    model_graph_train['FORECAST_TECHNIQUE'] = 'ARIMA'
    model_graph_train['FORECAST_UNITS'] = UNITS
    model_graph_train['_ROW_CREATED_AT'] = str(datetime.datetime.utcnow())
    model_graph_train['_ROW_UPDATED_AT'] = str(datetime.datetime.utcnow())
    model_graph_train['FORECAST_VALUE'] = None

    model_graph_test['ACTUAL_VALUE'] = test_data_arima['adjusted_kpi']
    model_graph_test['FORECAST_DATE'] = test_data_arima.index.values
    model_graph_test['REQUEST_ID'] = test_data_arima['request_id'][0]
    model_graph_test['FORECAST_PARAM'] = METRICS
    model_graph_test['FORECAST_TECHNIQUE'] = 'ARIMA'
    model_graph_test['FORECAST_UNITS'] = UNITS
    model_graph_test['_ROW_CREATED_AT'] = str(datetime.datetime.utcnow())
    model_graph_test['_ROW_UPDATED_AT'] = str(datetime.datetime.utcnow())
    model_graph_test['FORECAST_VALUE'] = test_data_arima['predicted_kpi']

    model_graph = model_graph_train.append(model_graph_test,ignore_index=True)

    for row in model_results.itertuples():
        row_forecast_metrics = ModelForecastMetrics(
            request_id = row.REQUEST_ID,
            forecast_avg_lift = row.FORECAST_AVG_LIFT,
            forecast_avg_train_error = row.FORECAST_AVG_TRAIN_ERROR,
            forecast_technique = row.FORECAST_TECHNIQUE,
            _row_created_at = datetime.datetime.utcnow(),
            _row_updated_at = datetime.datetime.utcnow(),
            forecast_actuals = row.FORECAST_ACTUALS,
            forecast_expected = row.FORECAST_EXPECTED,
            confidence = row.CONFIDENCE,
            abs_uplift = row.ABS_UPLIFT
        )
        session.add(row_forecast_metrics)

    model_graph = model_graph.where((pd.notnull(model_graph)), None)

    model_graph_rows = []
    for row in model_graph.itertuples():
        row_forecast_values = ModelForecasts(
            request_id = row.REQUEST_ID,
            actual_value = row.ACTUAL_VALUE,
            forecast_date = datetime.datetime.strptime(str(row.FORECAST_DATE), '%Y-%m-%d %H:%M:%S'),
            forecast_param = row.FORECAST_PARAM,
            forecast_technique = row.FORECAST_TECHNIQUE,
            forecast_units = row.FORECAST_UNITS,
            forecast_value = row.FORECAST_VALUE,

            _row_created_at = datetime.datetime.utcnow(),
            _row_updated_at = datetime.datetime.utcnow()
        )

        model_graph_rows.append(row_forecast_values)

    session.bulk_save_objects(model_graph_rows)
    session.commit()

    # Update request log
    log_message = ('ARIMA run completed, {} result rows inserted, '
                   'MAPE: {:.2f}'.format(len(model_graph_rows), mape_ARIMA))
    request_add_log_message(request_id, log_message, 0, session)
