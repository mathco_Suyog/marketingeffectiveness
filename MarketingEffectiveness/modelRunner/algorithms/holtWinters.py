from datetime import datetime
import datetime
import pandas as pd
import numpy as np
from modelRunner.models.fc_schema import ModelForecastMetrics, ModelForecasts
import statsmodels.api as sm
from statsmodels.tsa.stattools import adfuller
from statsmodels.tsa.holtwinters import ExponentialSmoothing
import impyute as impy
import itertools
from ..log_helpers import request_add_log_message
from ..modelRunException import MAPEException, DataSufficiencyException, NoOptimalSolutionException


class HWfns(object):
    @staticmethod
    def gridSearchHoltWinter(df,DepVar):
        """
        This function runs a grid search for Holt Winter's
        """
        # Initializing the parameters
        pParams = [7,30,90]
        tParams = ['add','mul']
        dParams = [True]
        sParams = [None]

        # Generate all different combinations of p, t, s and d quadruplets
        models = list(itertools.product(tParams, dParams, sParams, pParams))
        config = None
        least_error = np.inf

        # Create config instances
        for t, d, s, p in models:
            cfg = [t, d, s, p]
            try:
                modelFit = ExponentialSmoothing(df[DepVar], seasonal_periods=p, trend=t, seasonal=s, damped=d).fit()
                df['PREDICTED'] = modelFit.predict(start=0, end=len(df))

                current_error = np.mean(np.abs((df[DepVar] - df['PREDICTED']) / df[DepVar]))
                if (current_error < least_error) and (current_error > 0.10):
                    least_error = current_error
                    config = cfg
            except Exception:
                continue

        if config is None or least_error == np.inf:
            raise NoOptimalSolutionException(len(models))

        return config, least_error

    @staticmethod
    def model_fit_hw(df, dep_var, best_param):
        """
        This function runs Holt Winters and returns the model fit
        """
        
        model = ExponentialSmoothing(df[dep_var], trend=trend, damped=damped, seasonal=seasonal,
                                     seasonal_periods=seasonal_periods, dates=dates, freq='D', missing=missing)
        return model.fit()
    
    @staticmethod
    def mape(self,dep_var,model_fit):
        """
        This function runs moving average for test
        """
   
        residuals = model_fit.resid
        mape = np.mean(np.abs((residuals)/self[dep_var]))
        return(mape)
    
    @staticmethod
    def train_pred(self,self_var,model_fit):
        """
        This function predicts for train data
        """
        residuals = model_fit.resid
        if dep_var is not None:
            self['PREDICTED'] =model_fit.predict(start=0,end=len(self))
        else:
            self['PREDICTED'] =self['adjusted_kpi']-residuals
        return (self)

    @staticmethod
    def test_pred(df,dep_var,model_fit):

        """
        This function forecasts the on test data
        @params: 
            df: Test Data
            dep_var: Independent Variable
            model_fit: fitted ARIMA model
        """

        if dep_var is not None:
            df['predicted']=np.array(model_fit.forecast(len(df)))
        return (df)

    @staticmethod
    def decomposition_function(df,adjusted_kpi):
        """
        This function splits the data into trend, seasonal and random components
        """
        df_series = pd.Series(data=df[adjusted_kpi])
        numpyArrayKPI = np.array(df_series.resample('D').sum(),dtype=float)
        decomposition = sm.tsa.seasonal_decompose(numpyArrayKPI, model='multiplicative',freq=365)
        return(decomposition)

    @staticmethod
    def trend_resid_data(df,trend_series,residual_series,seasonal_series,observed_series):
        """
        This is used to get the data with just trend and residuals
        """
        # Multiplicative Seasonality            
        df_trend_resid = pd.DataFrame({'trend_residual':observed_series/seasonal_series, 'observed':observed_series})
        # Imputing the data using KNN imputation            
        # df_trend_resid = round(impy.fast_knn(df_trend_resid,k=3),0)
        # df_trend_resid.columns = ["observed","trend_residual"]
        df['train_date'] = df.index
        df_trend_resid = pd.concat([df.reset_index(drop=True),df_trend_resid],axis=1)
        df_trend_resid = df_trend_resid.set_index('train_date')
        return(df_trend_resid)

    @staticmethod
    def seasonalizing_data(df,predicted,seasonal_series):
        """
        Adding seasonality back into the data after predicting
        """
        df.insert(0, 'merge_key', range(0,len(df)))
        seasonal_comp = pd.DataFrame({'seasonal':seasonal_series})
        seasonal_comp.insert(0, 'merge_key', range(0,len(seasonal_comp)))
        df =  df.join(seasonal_comp,on=['merge_key'], how='left',lsuffix='_left', rsuffix='_right')
        # Multiplicative Seasonality
        df['predicted_kpi'] =  df[predicted] * df.seasonal
        return(df)

    @staticmethod
    def residual_approach(test_data):
        """
        Implementing residual approach
        """
        residual_df = test_data.groupby(['period'],as_index = False)['adjusted_kpi','predicted_kpi'].sum()
        test_residual_factor_df = residual_df[residual_df['period']=='Pre']
        residual_factor = (test_residual_factor_df['adjusted_kpi'].iloc[0] - test_residual_factor_df['predicted_kpi'].iloc[0]) / test_residual_factor_df['predicted_kpi'].iloc[0]
        test_absolute_value = residual_df[residual_df['period']=='Post']
        absolute_value = sum(test_absolute_value['predicted_kpi'], (test_absolute_value['predicted_kpi']*residual_factor))
        test_data['pct_contribution'] = test_data['predicted_kpi']/test_absolute_value['predicted_kpi'].sum()
        for i in range(len(test_data)):
            if test_data['period'][i] == 'Pre':
                test_data['pct_contribution'][i] = np.NaN
        test_data['absolute_value'] = absolute_value.iloc[0]
        test_data['pred_kpi'] = test_data['pct_contribution'] * test_data['absolute_value']
        test_data_pre = test_data[test_data['period']=='Pre']
        test_data_post = test_data[test_data['period']=='Post']
        return test_data_pre, test_data_post    

    @staticmethod
    def uplift(test_data):
        """
        A function to calculate the total lift in units (both positive or negative) for test timeframe
        """
        uplift = test_data['adjusted_kpi'].sum()-test_data['pred_kpi'].sum()
        return uplift

    @staticmethod
    def lift_pct(test_data):
        """
        A function to calculate the total lift % (both positive or negative) for test timeframe
        """
        lift = (test_data['adjusted_kpi'].sum()-test_data['pred_kpi'].sum())/test_data['pred_kpi'].sum()
        return lift

def holt_winters_code(train_data,test_data,data_sufficiency_key,train_data_adj,session):

    request_id = train_data.request_id.iloc[0]
    if data_sufficiency_key != 'Pass':
        DataSufficiencyException()

    # Creating copies of the train and test data
    model_train_hw = train_data.copy()
    model_test_hw = test_data.copy()

    # Setting index to test and train data
    model_train_hw = model_train_hw.set_index('train_date')
    model_test_hw = model_test_hw.set_index('test_date')


    decompose = HWfns.decomposition_function(model_train_hw,'adjusted_kpi')
    df_trend_resid = HWfns.trend_resid_data(model_train_hw,decompose.trend,decompose.resid,decompose.seasonal,
                                    decompose.observed)

    # Calculating the best  Holt Winters parameters for the least MAPE
    best_param, best_mape = HWfns.gridSearchHoltWinter(df_trend_resid,'trend_residual')
    # Predicting on the test data with optimized parameters

    model_fit = ExponentialSmoothing(df_trend_resid['trend_residual'], trend=best_param[0], damped=best_param[1], seasonal=best_param[2]
                                , seasonal_periods=best_param[3], dates=None, freq='D', missing='none').fit()
    test_pred_trend_resid = HWfns.test_pred(model_test_hw,'adjusted_kpi',model_fit)
    # Adding seasonality back into the model
    model_test_hw = HWfns.seasonalizing_data(test_pred_trend_resid,'predicted',decompose.seasonal)

    # Residual component correction
    model_test_hw_pre, model_test_hw_post = HWfns.residual_approach(model_test_hw)

    confidence = ''
    # Confidence Level
    if best_mape > 0.3:
        confidence = 'Low'
    else:
        confidence = 'High'

    # Metric calculation
    uplift = HWfns.uplift(model_test_hw_post)
    lift = HWfns.lift_pct(model_test_hw_post)
    avg_lift = uplift/len(model_test_hw_post)
    KPI = train_data['performance_metric'][0]

    param_mapping = {
        'total_sales': 'SALES',
        'total_customer': 'CUSTOMER COUNT',
        'total_orders': 'ORDER COUNT',
        'total_visits': 'VISIT COUNT',
        'conversion rate': 'CONVERSION RATE'
    }

    units_mapping = {
        'total_sales': 'USD',
        'conversion rate': 'PERCENTAGE'
    }

    METRICS = param_mapping.get(KPI, '')
    UNITS = units_mapping.get(KPI, '')

    model_results = pd.DataFrame(columns=['ID',
                                            'REQUEST_ID',
                                            'FORECAST_AVG_LIFT',
                                            'FORECAST_AVG_TRAIN_ERROR',
                                            'FORECAST_TECHNIQUE',
                                            '_ROW_CREATED_AT',
                                            '_ROW_UPDATED_AT',
                                            'FORECAST_ACTUALS',
                                            'FORECAST_EXPECTED',
                                            'CONFIDENCE',
                                            'ABS_UPLIFT'])

    model_graph_train = pd.DataFrame(columns=['ID',
                                                'REQUEST_ID',
                                                'ACTUAL_VALUE',
                                                'FORECAST_DATE',
                                                'FORECAST_PARAM',
                                                'FORECAST_TECHNIQUE',
                                                'FORECAST_UNITS',
                                                'FORECAST_VALUE',
                                                '_ROW_CREATED_AT',
                                                '_ROW_UPDATED_AT',
                                                'FORECAST_RESIDUAL_VALUE'])

    model_graph_test = pd.DataFrame(columns=['ID',
                                                'REQUEST_ID',
                                                'ACTUAL_VALUE',
                                                'FORECAST_DATE',
                                                'FORECAST_PARAM',
                                                'FORECAST_TECHNIQUE',
                                                'FORECAST_UNITS',
                                                'FORECAST_VALUE',
                                                '_ROW_CREATED_AT',
                                                '_ROW_UPDATED_AT',
                                                'FORECAST_RESIDUAL_VALUE'])

    if best_mape >= 0.5:
        raise MAPEException(best_mape)

    model_results = model_results.append({'ID':range(0 , len(model_graph_train)),
                                          'REQUEST_ID':train_data_adj['request_id'][0],
                                          'FORECAST_AVG_LIFT':lift,
                                          'FORECAST_AVG_TRAIN_ERROR':1-best_mape,
                                          'FORECAST_TECHNIQUE':'HOLT WINTERS',
                                          '_ROW_CREATED_AT':datetime.datetime.utcnow(),
                                          '_ROW_UPDATED_AT':datetime.datetime.utcnow(),
                                          'FORECAST_ACTUALS':model_test_hw_post['adjusted_kpi'].sum(),
                                          'FORECAST_EXPECTED':model_test_hw_post['pred_kpi'].sum(),
                                          'CONFIDENCE':confidence,
                                          'ABS_UPLIFT': uplift,
                                          'AVG_LIFT_PER_DAY':avg_lift
                                        },ignore_index=True)

    model_graph_train['ACTUAL_VALUE'] = model_test_hw_pre['adjusted_kpi']
    model_graph_train['FORECAST_DATE'] = model_test_hw_pre.index.values
    model_graph_train['REQUEST_ID'] = model_test_hw_pre['request_id'][0]
    model_graph_train['FORECAST_PARAM'] = METRICS
    model_graph_train['FORECAST_TECHNIQUE'] = 'HOLT WINTERS'
    model_graph_train['FORECAST_UNITS'] = UNITS
    model_graph_train['_ROW_CREATED_AT'] = str(datetime.datetime.utcnow())
    model_graph_train['_ROW_UPDATED_AT'] = str(datetime.datetime.utcnow())
    model_graph_train['FORECAST_VALUE'] = model_test_hw_pre['predicted_kpi']
    model_graph_train['FORECAST_RESIDUAL_VALUE'] = None

    model_graph_test['ACTUAL_VALUE'] = model_test_hw_post['adjusted_kpi']
    model_graph_test['FORECAST_DATE'] = model_test_hw_post.index.values
    model_graph_test['REQUEST_ID'] = model_test_hw_post['request_id'][0]
    model_graph_test['FORECAST_PARAM'] = METRICS
    model_graph_test['FORECAST_TECHNIQUE'] = 'HOLT WINTERS'
    model_graph_test['FORECAST_UNITS'] = UNITS
    model_graph_test['_ROW_CREATED_AT'] = str(datetime.datetime.utcnow())
    model_graph_test['_ROW_UPDATED_AT'] = str(datetime.datetime.utcnow())
    model_graph_test['FORECAST_VALUE'] = model_test_hw_post['predicted_kpi']
    model_graph_test['FORECAST_RESIDUAL_VALUE'] = model_test_hw_post['pred_kpi']

    model_graph = model_graph_train.append(model_graph_test, ignore_index=True)

    for row in model_results.itertuples():
        row_forecast_metrics = ModelForecastMetrics(
            request_id = row.REQUEST_ID,
            forecast_avg_lift = row.FORECAST_AVG_LIFT,
            forecast_avg_train_error = row.FORECAST_AVG_TRAIN_ERROR,
            forecast_technique = row.FORECAST_TECHNIQUE,
            _row_created_at = datetime.datetime.utcnow(),
            _row_updated_at = datetime.datetime.utcnow(),
            forecast_actuals = row.FORECAST_ACTUALS,
            forecast_expected = row.FORECAST_EXPECTED,
            confidence = row.CONFIDENCE,
            abs_uplift = row.ABS_UPLIFT,
            forecast_avg_per_day = row.AVG_LIFT_PER_DAY
        )
        session.add(row_forecast_metrics)

    model_graph_rows = []
    for row in model_graph.itertuples():
        row_forecast_values = ModelForecasts(
            request_id = row.REQUEST_ID,
            actual_value = row.ACTUAL_VALUE,
            forecast_date = datetime.datetime.strptime(str(row.FORECAST_DATE), '%Y-%m-%d %H:%M:%S'),
            forecast_param = row.FORECAST_PARAM,
            forecast_technique = row.FORECAST_TECHNIQUE,
            forecast_units = row.FORECAST_UNITS,
            forecast_value = row.FORECAST_VALUE,
            _row_created_at = datetime.datetime.utcnow(),
            _row_updated_at = datetime.datetime.utcnow(),
            forecast_residual_value = row.FORECAST_RESIDUAL_VALUE
        )

        model_graph_rows.append(row_forecast_values)

    session.bulk_save_objects(model_graph_rows)
    session.commit()

    # Update request log
    log_message = ('Holt Winters run completed, {} result rows inserted, '
                   'MAPE: {:.2f}'.format(len(model_graph_rows), best_mape))
    request_add_log_message(request_id, log_message, 0, session)