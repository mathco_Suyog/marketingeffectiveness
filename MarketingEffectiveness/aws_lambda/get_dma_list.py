import json

from fc_schema import VisitFilters, TransactionFilters
from sqlalchemy import and_ , or_

def get_dma_list(event, context, session):
    event_measure = event["queryStringParameters"]["measure_performance"]
    if event_measure.lower() == 'visits':
        query_table = VisitFilters
    else:
        query_table = TransactionFilters

    event_country = event["queryStringParameters"]["country"]
    event_regions = event["multiValueQueryStringParameters"]["region"]
    # print (event_regions)

    if (len(event_regions) == 1) and ('All' in event_regions):
        event_regions_query = or_(
            query_table.region != None,
            query_table.region == None
        )
    else:
        event_regions_query = (query_table.region.in_(event_regions))
        
    if len(event_regions) > 0:
        qry = (
            session
            .query(query_table.dma)
            .filter(and_(
                query_table.country == event_country,
                event_regions_query
                ))
            .distinct().order_by(query_table.dma).all())
    else:
        qry = (
        session
        .query(query_table.dma)
        .filter(and_(
            query_table.country == event_country
            ))
        .distinct().order_by(query_table.dma).all())

    out_list = [ list(x) for x in qry if x[0] is not None ]
    # print(len(out_list))
    out_list.sort()
    out_list.insert(0, ['All'])

    out_list = json.dumps(out_list, separators=(',',':'))

    response =  {
        'statusCode':200,
        'body': out_list,
        'headers':{},
        'isBase64Encoded':False
        }

    return response