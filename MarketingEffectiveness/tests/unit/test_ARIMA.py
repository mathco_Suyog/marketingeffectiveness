from modelRunner.algorithms.deSeasonalizedArima import *
import pytest
from modelRunner.modelRunException import MAPEException, DataSufficiencyException, NoOptimalSolutionException
import pandas as pd
import numpy as np
import datetime
import pickle
import itertools
from statsmodels.tsa.arima_model import ARIMA
import statsmodels.api as sm
from statsmodels.tsa.stattools import adfuller
import impyute as impy
from modelRunner.models.fc_schema import ModelForecastMetrics, ModelForecasts
import sqlalchemy
from sqlalchemy import and_

@pytest.mark.parametrize('request_id', ['4ec9943f-ad49-4db4-a09f-bcc1650d9850', '98bfbbdd-674d-4040-b42b-ada83d565f04', '431de060-2cea-4ed3-b845-d92d7573155e'])
def test_arima_code(session,request_id):
        id_ = request_id
        train_data = pd.read_csv("../data/train_data_actual_expected.csv")
        train_data['train_date'] = pd.to_datetime(train_data['train_date'])
        train_data.columns = map(str.lower,train_data.columns)
        train_data = train_data[(train_data['request_id'] == id_)]
        train_data = train_data.reset_index(drop=True)
        
        test_data = pd.read_csv("../data/test_data_actual_expected.csv")
        test_data['test_date'] = pd.to_datetime(test_data['test_date'])
        test_data.columns = map(str.lower,test_data.columns)
        test_data = test_data[(test_data['request_id'] == id_)]
        test_data = test_data.reset_index(drop=True)

        train_data_adj = pd.read_csv("../data/train_data_adj_actual_expected.csv")
        train_data_adj['train_date'] = pd.to_datetime(train_data_adj['train_date'])
        train_data_adj = train_data_adj.reset_index(drop=True)
        train_data_adj = train_data_adj.set_index('train_date')
        train_data_adj = train_data_adj[(train_data_adj['request_id'] == id_)]

        output_data = pd.read_csv("../data/model_results_actual_expected.csv")
        output_data.columns = map(str.lower,output_data.columns)
        output_data = output_data[(output_data['request_id']==id_)]

        de_seasonal_arima_code(train_data,test_data,"Pass",train_data_adj,session)

        model_forecast_metrics = (
                                 session
                                 .query(ModelForecastMetrics)
                                 .filter(and_(ModelForecastMetrics.request_id ==id_),
                                             (ModelForecastMetrics.forecast_technique == 'ARIMA'))
        )

        df = pd.read_sql(model_forecast_metrics.statement,model_forecast_metrics.session.bind)
        df = df[(df['request_id']==id_)]
        df = df.reset_index(drop=True)
        assert round(df.forecast_actuals,0).any() == round(output_data.forecast_actuals,0).any()
        assert round(df.forecast_expected,0).any() == round(output_data.forecast_expected,0).any()
        assert round(df.forecast_avg_per_day,0).any() == round(output_data.forecast_avg_per_day,0).any()
        assert round(df.abs_uplift,0).any() == round(output_data.abs_uplift,0).any()
        assert round(df.forecast_avg_train_error,4).any() == round(output_data.forecast_avg_train_error,4).any()
        assert round(df.forecast_avg_lift,4).any() == round(output_data.forecast_avg_lift,4).any()