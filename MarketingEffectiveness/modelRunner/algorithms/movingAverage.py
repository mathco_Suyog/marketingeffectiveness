from datetime import datetime
import datetime
import pandas as pd
import numpy as np
from modelRunner.models.fc_schema import ModelForecastMetrics, ModelForecasts
from ..log_helpers import request_add_log_message
from ..modelRunException import MAPEException, DataSufficiencyException, NoOptimalSolutionException

class MA(object):
    @staticmethod
    def grid_search_moving_average(self, dep_var):
        """
        This function runs a grid search for moving average
        """
        max_lag = 30
        least_mape = np.inf
        best_window = 2

        # use gridsearch to look for optimial moving average parameters
        for param in range(best_window, max_lag):
            results = self[dep_var].rolling(window=param).mean().shift()

            # if current run of MAPE is better than the best one so far, overwrite it
            error = np.abs((results-self[dep_var])/self[dep_var]).mean()
            current_mape = error
            if (current_mape < least_mape) and (current_mape > 0.1):
                least_mape = current_mape
                best_window = param
            else:
                continue

        if least_mape == np.inf:
            raise NoOptimalSolutionException(len(range(best_window, max_lag)))

        return best_window, least_mape

    # Running moving average on the train data
    @staticmethod
    def moving_average_train(self, dep_var, best_window):
        """
        This function runs moving average for train
        """
        rolling_average = self[dep_var].rolling(best_window).mean().shift()
        return rolling_average

    @staticmethod
    def moving_average_test(self, dep_var, best_window, test_data):
        """
        This function runs moving average for test
        """
        value = pd.Series([])
        series = self[dep_var]
        for pred in range(0, len(test_data)):
            pred = pd.Series(np.mean(series[len(series)-best_window:len(series)+1]))
            series = series.append(pred, ignore_index=True)
            value = value.append(pred, ignore_index=True)
        return round(value, 0)


def moving_average_code(train_data, test_data, data_sufficiency_key, session):
    """ Moving Average Model Runner """

    request_id = train_data.request_id.iloc[0]
    if data_sufficiency_key != 'Pass':
        raise DataSufficiencyException()

    # train_data_ma = train_data.copy()
    # test_data_ma = train_data.copy()
    #
    # best_window, mape_ma = MA.grid_search_moving_average(train_data_ma, 'adjusted_kpi')
    # predictions = []
    test_data_ma = test_data.copy()
    train_data_ma = train_data.copy()
    best_window, mape_ma = MA.grid_search_moving_average(train_data_ma, 'adjusted_kpi')
    predictions = MA.moving_average_test(train_data_ma, 'adjusted_kpi', best_window, test_data_ma)
    test_data_ma['predicted_kpi'] = predictions
    # confidence = ''

    if mape_ma > 0.3:
        confidence = 'Low'
    else:
        confidence = 'High'

    uplift = test_data_ma['adjusted_kpi'].sum()-test_data_ma['predicted_kpi'].sum()
    lift = (test_data_ma['adjusted_kpi'].sum()-test_data_ma['predicted_kpi'].sum()) / test_data_ma['predicted_kpi'].sum()
    kpi = train_data_ma['performance_metric'][0]

    param_mapping = {
        'total_sales': 'SALES',
        'total_customer': 'CUSTOMER COUNT',
        'total_orders': 'ORDER COUNT',
        'total_visits': 'VISIT COUNT',
        'conversion rate': 'CONVERSION RATE'
    }

    units_mapping = {
        'total_sales': 'USD',
        'conversion rate': 'PERCENTAGE'
    }

    metrics = param_mapping.get(kpi, '')
    units = units_mapping.get(kpi, '')

    model_results = pd.DataFrame(columns=['ID', 'REQUEST_ID', 'FORECAST_AVG_LIFT', 'FORECAST_AVG_TRAIN_ERROR',
                                          'FORECAST_TECHNIQUE', '_ROW_CREATED_AT', '_ROW_UPDATED_AT',
                                          'FORECAST_ACTUALS', 'FORECAST_EXPECTED', 'CONFIDENCE', 'ABS_UPLIFT'])

    model_graph_train = pd.DataFrame(columns=['ID', 'REQUEST_ID', 'ACTUAL_VALUE', 'FORECAST_DATE', 'FORECAST_PARAM',
                                              'FORECAST_TECHNIQUE', 'FORECAST_UNITS', 'FORECAST_VALUE',
                                              '_ROW_CREATED_AT', '_ROW_UPDATED_AT'])

    model_graph_test = pd.DataFrame(columns=['ID', 'REQUEST_ID', 'ACTUAL_VALUE', 'FORECAST_DATE', 'FORECAST_PARAM',
                                             'FORECAST_TECHNIQUE', 'FORECAST_UNITS', 'FORECAST_VALUE',
                                             '_ROW_CREATED_AT', '_ROW_UPDATED_AT'])

    if mape_ma >= 0.5:
        raise MAPEException(mape_ma)

    model_results = model_results.append({'ID': list(range(0, len(model_graph_train))),
                                          'REQUEST_ID': train_data_ma['request_id'][0],
                                          'FORECAST_AVG_LIFT': lift,
                                          'FORECAST_AVG_TRAIN_ERROR': 1-mape_ma,
                                          'FORECAST_TECHNIQUE': 'MOVING AVERAGE',
                                          '_ROW_CREATED_AT': datetime.datetime.utcnow(),
                                          '_ROW_UPDATED_AT': datetime.datetime.utcnow(),
                                          'FORECAST_ACTUALS': test_data_ma['adjusted_kpi'].sum(),
                                          'FORECAST_EXPECTED': test_data_ma['predicted_kpi'].sum(),
                                          'CONFIDENCE': confidence,
                                          'ABS_UPLIFT': uplift
                                          }, ignore_index=True)

    model_graph_train['ACTUAL_VALUE'] = train_data_ma[-len(test_data_ma):]['adjusted_kpi']
    model_graph_train['FORECAST_DATE'] = train_data_ma[-len(test_data_ma):]['train_date']
    model_graph_train['REQUEST_ID'] = train_data_ma['request_id'][0]
    model_graph_train['FORECAST_PARAM'] = metrics
    model_graph_train['FORECAST_TECHNIQUE'] = 'MOVING AVERAGE'
    model_graph_train['FORECAST_UNITS'] = units
    model_graph_train['_ROW_CREATED_AT'] = str(datetime.datetime.utcnow())
    model_graph_train['_ROW_UPDATED_AT'] = str(datetime.datetime.utcnow())
    model_graph_train['FORECAST_VALUE'] = None

    model_graph_test['ACTUAL_VALUE'] = test_data['adjusted_kpi']
    model_graph_test['FORECAST_DATE'] = test_data['test_date']
    model_graph_test['REQUEST_ID'] = test_data['request_id'][0]
    model_graph_test['FORECAST_PARAM'] = metrics
    model_graph_test['FORECAST_TECHNIQUE'] = 'MOVING AVERAGE'
    model_graph_test['FORECAST_UNITS'] = units
    model_graph_test['_ROW_CREATED_AT'] = str(datetime.datetime.utcnow())
    model_graph_test['_ROW_UPDATED_AT'] = str(datetime.datetime.utcnow())
    model_graph_test['FORECAST_VALUE'] = test_data_ma['predicted_kpi']

    model_graph = model_graph_train.append(model_graph_test, ignore_index=True)

    # Writing results to database
    for row in model_results.itertuples():
        row_forecast_metrics = ModelForecastMetrics(
            request_id=row.REQUEST_ID,
            forecast_avg_lift=row.FORECAST_AVG_LIFT,
            forecast_avg_train_error=row.FORECAST_AVG_TRAIN_ERROR,
            forecast_technique=row.FORECAST_TECHNIQUE,
            _row_created_at=datetime.datetime.utcnow(),
            _row_updated_at=datetime.datetime.utcnow(),
            forecast_actuals=row.FORECAST_ACTUALS,
            forecast_expected=row.FORECAST_EXPECTED,
            confidence=row.CONFIDENCE,
            abs_uplift=row.ABS_UPLIFT
        )
        session.add(row_forecast_metrics)

    model_graph_rows = []
    for row in model_graph.itertuples():
        row_forecast_values = ModelForecasts(
            request_id=row.REQUEST_ID,
            actual_value=row.ACTUAL_VALUE,
            forecast_date=datetime.datetime.strptime(str(row.FORECAST_DATE), '%Y-%m-%d %H:%M:%S'),
            forecast_param=row.FORECAST_PARAM,
            forecast_technique=row.FORECAST_TECHNIQUE,
            forecast_units=row.FORECAST_UNITS,
            forecast_value=row.FORECAST_VALUE,

            _row_created_at=datetime.datetime.utcnow(),
            _row_updated_at=datetime.datetime.utcnow()
        )

        model_graph_rows.append(row_forecast_values)
    session.bulk_save_objects(model_graph_rows)

    # Update request log
    log_message = ('Moving average model run completed, {} result rows inserted, '
                   'MAPE: {:.2f}'.format(len(model_graph_rows), mape_ma))
    request_add_log_message(request_id, log_message, 0, session)
