from modelRunner import log_helpers
from modelRunner.models.fc_schema import RequestLog, RequestDetails
import datetime


class TestLogHelpers:
    def test_add_message(self, session, setup_log_test):
        # GIVEN: a request ID in the DB
        # WHEN: a log message is added to the request id
        # THEN: the log message(s) are written to the DB
        # test_id = 'eb098af6-e0d3-4d47-97e0-f4ddb28b03f1'
        test_id = 'TEST-LOGTEST-TEST'
        qry = session.query(RequestLog).filter(RequestLog.request_id == test_id)

        log_messages = qry.all()
        log_msg_count = len(log_messages)

        log_helpers.request_add_log_message(test_id, 'Test Log Message 1 - This should not be in the DB', 0, session)
        log_helpers.request_add_log_message(test_id, 'Test Log Message 2 - This should not be in the DB', 0, session)

        log_messages = qry.all()
        new_msg_count = len(log_messages)

        assert (new_msg_count - log_msg_count) == 2

    def test_processing_start(self, session, setup_log_test):
        # GIVEN: a request id ready for processing
        # WHEN: processing of the request starts
        # THEN: request status is changed to processing (1), processing start time is updated to current time
        # test_id = 'eb098af6-e0d3-4d47-97e0-f4ddb28b03f1'
        test_id = 'TEST-LOGTEST-TEST'
        qry = session.query(RequestDetails).filter(RequestDetails.request_id == test_id)

        request_details = qry.one()

        old_status = request_details.status_code
        old_process_start_time = request_details.process_start_at_ts_utc
        assert old_status == 2
        assert old_process_start_time is None

        log_helpers.request_processing_start(test_id, session)

        qry = session.query(RequestDetails).filter(RequestDetails.request_id == test_id)

        request_details = qry.one()

        new_status = request_details.status_code
        new_process_start_time = request_details.process_start_at_ts_utc

        timestamp_diff = datetime.datetime.utcnow().timestamp() - new_process_start_time.replace(tzinfo=None).timestamp()

        assert new_status == 1
        assert (timestamp_diff > 0) and (timestamp_diff < 5)  # Processing time has been updated within the last 5 secs

    def test_model_run_completed(self, session, setup_log_test):
        # GIVEN: a request id
        # WHEN: a model run is completed
        # THEN: models_run_completed count for that request id is incremented by 1
        # test_id = 'eb098af6-e0d3-4d47-97e0-f4ddb28b03f1'
        test_id = 'TEST-LOGTEST-TEST'
        qry = session.query(RequestDetails).filter(RequestDetails.request_id == test_id)
        request_details = qry.one()
        old_count = request_details.models_run_completed

        log_helpers.model_run_completed(test_id, session)

        qry = session.query(RequestDetails).filter(RequestDetails.request_id == test_id)
        request_details = qry.one()
        new_count = request_details.models_run_completed

        assert (new_count - old_count) == 1

    def test_request_processing_complete(self, session, setup_log_test):
        # GIVEN: a request id
        # WHEN: request processing is completed
        # THEN: status code is changed to either 0 or 3 (SUCCESS / FAIL), process end time is updated to current time
        test_id = 'TEST-LOGTEST-TEST'

        log_helpers.request_processing_complete(test_id, session)

        qry = session.query(RequestDetails).filter(RequestDetails.request_id == test_id)
        request_details = qry.one()
        new_status = request_details.status_code
        new_process_end_time = request_details.process_end_at_ts_utc

        timestamp_diff = datetime.datetime.utcnow().timestamp() - new_process_end_time.replace(tzinfo=None).timestamp()

        assert new_status in (0, 3)
        assert (timestamp_diff > 0) and (timestamp_diff < 5)  # Processing time has been updated within the last 5 secs
