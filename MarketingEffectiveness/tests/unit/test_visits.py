from modelRunner.algorithms.testControlSYDM import *
from modelRunner.algorithms.testControlSMLY import *
import pytest
from modelRunner.modelRunException import DataSufficiencyException
import pandas as pd
from modelRunner.models.fc_schema import TestControlMetrics,TestControlStatsMetrics
from datetime import datetime

## checking for visits for Same Year Different Market
@pytest.mark.parametrize('request_id',['e3550412-1aad-4ae9-b15e-196277b7f220','06a85d4a-3fa5-45f4-9085-ed625ad225f4','8e7b252c-5dd3-41f0-a009-688fa410dfd3','cba52496-86f8-4295-8cb7-4f764ae249c5'])
def test_run_sydm(session,request_id):
    id_ = request_id
    input_df = pd.read_csv("../data/visits_input.csv")
    input_df['date']=pd.to_datetime(input_df['date'])
    input_df = input_df[input_df['request_id']==id_]

    output_metrics = pd.read_csv("../data/visits_metric.csv")
    output_metrics = output_metrics[output_metrics['request_id']==id_]

    output_stats = pd.read_csv("../data/visits_stats.csv")
    output_stats = output_stats[output_stats['request_id']==id_]

    performanceMetric = input_df.loc[input_df['request_id']==id_,'performance_metric'].iloc[0]
    run_sydm(input_df,performanceMetric,session)

    tc_metrics = (
                session
                .query(TestControlMetrics)
                .filter(TestControlMetrics.request_id == id_ )
                
                .first()
            )
    assert round(tc_metrics.abs_uplift,4) == round(output_metrics.abs_uplift.iloc[0],4)
    assert tc_metrics.abs_lift_per_day == output_metrics.abs_lift_per_day.iloc[0]
    assert tc_metrics.control_pre_vs_post == output_metrics.control_pre_vs_post.iloc[0]
    assert round(tc_metrics.pct_uplift,4) == round(output_metrics.pct_uplift.iloc[0],4)
    assert tc_metrics.t_test_p_value == output_metrics.t_test_p_value.iloc[0]
    assert tc_metrics.test_pre_vs_post == output_metrics.test_pre_vs_post.iloc[0]

    tc_stats_metrics = (
                       session
                       .query(TestControlStatsMetrics)
                       .filter(TestControlStatsMetrics.request_id == id_)
                    )
    stats_df = pd.read_sql(tc_stats_metrics.statement,tc_stats_metrics.session.bind)

    
    assert stats_df.loc[(stats_df['market']=='Test') & (stats_df['period']=='Pre') &
                        (stats_df['metric']=='Mean'),'value'].values[0]\
                         == output_stats.loc[(output_stats['market']=='Test') &
                                             (output_stats['period']=='Pre') &
                                             (output_stats['metric']=='Mean'),'value'].values[0]

    assert stats_df.loc[(stats_df['market']=='Test') & (stats_df['period']=='Pre') &
                        (stats_df['metric']=='SD'),'value'].values[0]\
                         == output_stats.loc[(output_stats['market']=='Test') &
                                             (output_stats['period']=='Pre') &
                                             (output_stats['metric']=='SD'),'value'].values[0]

    assert stats_df.loc[(stats_df['market']=='Test') & (stats_df['period']=='Pre') &
                        (stats_df['metric']=='t-stat'),'value'].values[0]\
                         == output_stats.loc[(output_stats['market']=='Test') &
                                             (output_stats['period']=='Pre') &
                                             (output_stats['metric']=='t-stat'),'value'].values[0]
    
    assert stats_df.loc[(stats_df['market']=='Test') & (stats_df['period']=='Pre') &
                        (stats_df['metric']=='p-value'),'value'].values[0]\
                         == output_stats.loc[(output_stats['market']=='Test') &
                                             (output_stats['period']=='Pre') &
                                             (output_stats['metric']=='p-value'),'value'].values[0]

    assert stats_df.loc[(stats_df['market']=='Test') & (stats_df['period']=='Post') &
                        (stats_df['metric']=='Mean'),'value'].values[0]\
                         == output_stats.loc[(output_stats['market']=='Test') &
                                             (output_stats['period']=='Post') &
                                             (output_stats['metric']=='Mean'),'value'].values[0]

    assert stats_df.loc[(stats_df['market']=='Test') & (stats_df['period']=='Post') &
                        (stats_df['metric']=='SD'),'value'].values[0]\
                         == output_stats.loc[(output_stats['market']=='Test') &
                                             (output_stats['period']=='Post') &
                                             (output_stats['metric']=='SD'),'value'].values[0]

    assert stats_df.loc[(stats_df['market']=='Test') & (stats_df['period']=='Post') &
                        (stats_df['metric']=='t-stat'),'value'].values[0]\
                         == output_stats.loc[(output_stats['market']=='Test') &
                                             (output_stats['period']=='Post') &
                                             (output_stats['metric']=='t-stat'),'value'].values[0]

    assert stats_df.loc[(stats_df['market']=='Test') & (stats_df['period']=='Post') &
                        (stats_df['metric']=='p-value'),'value'].values[0]\
                         == output_stats.loc[(output_stats['market']=='Test') &
                                             (output_stats['period']=='Post') &
                                             (output_stats['metric']=='p-value'),'value'].values[0]

    assert stats_df.loc[(stats_df['market']=='Control') & (stats_df['period']=='Pre') &
                        (stats_df['metric']=='Mean'),'value'].values[0]\
                         == output_stats.loc[(output_stats['market']=='Control') &
                                             (output_stats['period']=='Pre') &
                                             (output_stats['metric']=='Mean'),'value'].values[0]

    assert stats_df.loc[(stats_df['market']=='Control') & (stats_df['period']=='Pre') &
                        (stats_df['metric']=='SD'),'value'].values[0]\
                         == output_stats.loc[(output_stats['market']=='Control') &
                                             (output_stats['period']=='Pre') &
                                             (output_stats['metric']=='SD'),'value'].values[0]

    assert stats_df.loc[(stats_df['market']=='Control') & (stats_df['period']=='Post') &
                        (stats_df['metric']=='Mean'),'value'].values[0]\
                         == output_stats.loc[(output_stats['market']=='Control') &
                                             (output_stats['period']=='Post') &
                                             (output_stats['metric']=='Mean'),'value'].values[0]

    assert stats_df.loc[(stats_df['market']=='Control') & (stats_df['period']=='Post') &
                        (stats_df['metric']=='SD'),'value'].values[0]\
                         == output_stats.loc[(output_stats['market']=='Control') &
                                             (output_stats['period']=='Post') &
                                             (output_stats['metric']=='SD'),'value'].values[0]

def test_run_sydm_exception():
    with pytest.raises(Exception) as exc_info:
        run_sydm(pd.DataFrame(), None, None)
    exc_message = exc_info.value.args[0]
    assert exc_message == "Undefined performance metric encountered."


def test_data_exists_sydm_exception():
    with pytest.raises(DataSufficiencyException):
        sydm = Test_Control(pd.DataFrame(), 'total_visits')
        Test_Control.data_exists(sydm.df, sydm.performance_metric)

@pytest.mark.parametrize('request_id',['e3550412-1aad-4ae9-b15e-196277b7f220','06a85d4a-3fa5-45f4-9085-ed625ad225f4','8e7b252c-5dd3-41f0-a009-688fa410dfd3','cba52496-86f8-4295-8cb7-4f764ae249c5'])
def test_sydm_data_exists(request_id):
    id_ = request_id
    input_df = pd.read_csv("../data/visits_input.csv")
    input_df['date'] = pd.to_datetime(input_df['date'])
    input_df = input_df[input_df['request_id']==id_]

    performanceMetric = input_df.loc[input_df['request_id']==id_,'performance_metric'].iloc[0]
    perf_met={'Sales':'total_sales','Orders':'total_orders','Customers':'total_customer','Visits':'total_visits'}
    performance_metric=perf_met.get(performanceMetric,None)

    output_records = pd.read_csv("../data/visits_records.csv")
    output_records = output_records[output_records['request_id'] == id_]

    sydm = Test_Control(input_df, performance_metric)
    df = sydm.data_exists(sydm.df, sydm.performance_metric)
    assert df.shape[0] == output_records.data_exists.values[0]

@pytest.mark.parametrize('request_id',['e3550412-1aad-4ae9-b15e-196277b7f220','06a85d4a-3fa5-45f4-9085-ed625ad225f4','8e7b252c-5dd3-41f0-a009-688fa410dfd3','cba52496-86f8-4295-8cb7-4f764ae249c5'])
def test_sydm_subset(request_id):
    id_= request_id
    input_df = pd.read_csv(r"..\data\visits_input.csv")
    input_df['date'] = pd.to_datetime(input_df['date'])
    input_df = input_df[input_df['request_id']==id_]

    output_records = pd.read_csv(r"..\data\visits_records.csv")
    output_records = output_records[output_records['request_id']==id_]

    df_test_pre, df_test_post, df_control_pre, df_control_post = Test_Control.subset(input_df)
    assert df_test_pre.shape[0] == output_records.pre_test.values[0]
    assert df_test_post.shape[0] == output_records.post_test.values[0]
    assert df_control_pre.shape[0] == output_records.pre_control.values[0]
    assert df_control_post.shape[0] == output_records.post_control.values[0]

#**********************************************************************************************************************************#
## Checkig for Visits for Same Market Last Year

@pytest.mark.parametrize('request_id',['6b1919d7-e88f-4bc8-9876-829b310e65db'])
def test_run_smly(session,request_id):
    id_ = request_id
    input_df = pd.read_csv("../data/visits_input.csv")
    input_df['date'] = pd.to_datetime(input_df['date'])
    input_df = input_df[input_df['request_id']==id_]

    output_metrics = pd.read_csv("../data/visits_metric.csv")
    output_metrics = output_metrics[output_metrics['request_id']==id_]

    output_stats = pd.read_csv("../data/visits_stats.csv")
    output_stats = output_stats[output_stats['request_id']==id_]

    performanceMetric = input_df.loc[input_df['request_id']==id_,'performance_metric'].iloc[0]
    run_smly(input_df,performanceMetric,session)

    tc_metrics = (
                session
                .query(TestControlMetrics)
                .filter(TestControlMetrics.request_id == id_ )
                
                .first()
            )
    assert round(tc_metrics.abs_uplift,4) == round(output_metrics.abs_uplift.iloc[0],4)
    assert tc_metrics.abs_lift_per_day == output_metrics.abs_lift_per_day.iloc[0]
    assert tc_metrics.control_pre_vs_post == output_metrics.control_pre_vs_post.iloc[0]
    assert round(tc_metrics.pct_uplift,4) == round(output_metrics.pct_uplift.iloc[0],4)
    assert tc_metrics.t_test_p_value == output_metrics.t_test_p_value.iloc[0]
    assert tc_metrics.test_pre_vs_post == output_metrics.test_pre_vs_post.iloc[0]

    tc_stats_metrics = (
                       session
                       .query(TestControlStatsMetrics)
                       .filter(TestControlStatsMetrics.request_id == id_)
                    )
    stats_df = pd.read_sql(tc_stats_metrics.statement,tc_stats_metrics.session.bind)

    
    assert stats_df.loc[(stats_df['market']=='ThisYear') & (stats_df['period']=='Pre') &
                        (stats_df['metric']=='Mean'),'value'].values[0]\
                         == output_stats.loc[(output_stats['market']=='ThisYear') &
                                             (output_stats['period']=='Pre') &
                                             (output_stats['metric']=='Mean'),'value'].values[0]

    assert stats_df.loc[(stats_df['market']=='ThisYear') & (stats_df['period']=='Pre') &
                        (stats_df['metric']=='SD'),'value'].values[0]\
                         == output_stats.loc[(output_stats['market']=='ThisYear') &
                                             (output_stats['period']=='Pre') &
                                             (output_stats['metric']=='SD'),'value'].values[0]

    assert stats_df.loc[(stats_df['market']=='ThisYear') & (stats_df['period']=='Pre') &
                        (stats_df['metric']=='t-stat'),'value'].values[0]\
                         == output_stats.loc[(output_stats['market']=='ThisYear') &
                                             (output_stats['period']=='Pre') &
                                             (output_stats['metric']=='t-stat'),'value'].values[0]
    
    assert stats_df.loc[(stats_df['market']=='ThisYear') & (stats_df['period']=='Pre') &
                        (stats_df['metric']=='p-value'),'value'].values[0]\
                         == output_stats.loc[(output_stats['market']=='ThisYear') &
                                             (output_stats['period']=='Pre') &
                                             (output_stats['metric']=='p-value'),'value'].values[0]

    assert stats_df.loc[(stats_df['market']=='ThisYear') & (stats_df['period']=='Post') &
                        (stats_df['metric']=='Mean'),'value'].values[0]\
                         == output_stats.loc[(output_stats['market']=='ThisYear') &
                                             (output_stats['period']=='Post') &
                                             (output_stats['metric']=='Mean'),'value'].values[0]

    assert stats_df.loc[(stats_df['market']=='ThisYear') & (stats_df['period']=='Post') &
                        (stats_df['metric']=='SD'),'value'].values[0]\
                         == output_stats.loc[(output_stats['market']=='ThisYear') &
                                             (output_stats['period']=='Post') &
                                             (output_stats['metric']=='SD'),'value'].values[0]

    assert stats_df.loc[(stats_df['market']=='ThisYear') & (stats_df['period']=='Post') &
                        (stats_df['metric']=='t-stat'),'value'].values[0]\
                         == output_stats.loc[(output_stats['market']=='ThisYear') &
                                             (output_stats['period']=='Post') &
                                             (output_stats['metric']=='t-stat'),'value'].values[0]

    assert stats_df.loc[(stats_df['market']=='ThisYear') & (stats_df['period']=='Post') &
                        (stats_df['metric']=='p-value'),'value'].values[0]\
                         == output_stats.loc[(output_stats['market']=='ThisYear') &
                                             (output_stats['period']=='Post') &
                                             (output_stats['metric']=='p-value'),'value'].values[0]

    assert stats_df.loc[(stats_df['market']=='LastYear') & (stats_df['period']=='Pre') &
                        (stats_df['metric']=='Mean'),'value'].values[0]\
                         == output_stats.loc[(output_stats['market']=='LastYear') &
                                             (output_stats['period']=='Pre') &
                                             (output_stats['metric']=='Mean'),'value'].values[0]

    assert stats_df.loc[(stats_df['market']=='LastYear') & (stats_df['period']=='Pre') &
                        (stats_df['metric']=='SD'),'value'].values[0]\
                         == output_stats.loc[(output_stats['market']=='LastYear') &
                                             (output_stats['period']=='Pre') &
                                             (output_stats['metric']=='SD'),'value'].values[0]

    assert stats_df.loc[(stats_df['market']=='LastYear') & (stats_df['period']=='Post') &
                        (stats_df['metric']=='Mean'),'value'].values[0]\
                         == output_stats.loc[(output_stats['market']=='LastYear') &
                                             (output_stats['period']=='Post') &
                                             (output_stats['metric']=='Mean'),'value'].values[0]

    assert stats_df.loc[(stats_df['market']=='LastYear') & (stats_df['period']=='Post') &
                        (stats_df['metric']=='SD'),'value'].values[0]\
                         == output_stats.loc[(output_stats['market']=='LastYear') &
                                             (output_stats['period']=='Post') &
                                             (output_stats['metric']=='SD'),'value'].values[0]

def test_run_smly_exception():
    with pytest.raises(Exception) as exc_info:
        run_sydm(pd.DataFrame(), None, None)
    exc_message = exc_info.value.args[0]
    assert exc_message == "Undefined performance metric encountered."


def test_data_exists_smly_exception():
    with pytest.raises(DataSufficiencyException):
        sydm = ThisYear_LastYear(pd.DataFrame(), 'total_visits')
        Test_Control.data_exists(sydm.df, sydm.performance_metric)

@pytest.mark.parametrize('request_id',['6b1919d7-e88f-4bc8-9876-829b310e65db'])
def test_smly_data_exists(request_id):
    id_ = request_id
    input_df = pd.read_csv(r"..\data\visits_input.csv")
    input_df['date'] = pd.to_datetime(input_df['date'])
    input_df = input_df[input_df['request_id']==id_]

    performanceMetric = input_df.loc[input_df['request_id']==id_,'performance_metric'].iloc[0]
    perf_met={'Sales':'total_sales','Orders':'total_orders','Customers':'total_customer','Visits':'total_visits'}
    performance_metric=perf_met.get(performanceMetric,None)

    output_records = pd.read_csv(r"..\data\visits_records.csv")
    output_records = output_records[output_records['request_id'] == id_]

    smly = ThisYear_LastYear(input_df, performance_metric)
    df = smly.data_exists(smly.df, smly.performance_metric)
    assert df.shape[0] == output_records.data_exists.values[0]

@pytest.mark.parametrize('request_id',['6b1919d7-e88f-4bc8-9876-829b310e65db'])
def test_smly_subset(request_id):
    id_= request_id
    input_df = pd.read_csv(r"..\data\visits_input.csv")
    input_df['date'] = pd.to_datetime(input_df['date'])
    input_df = input_df[input_df['request_id']==id_]

    output_records = pd.read_csv(r"..\data\visits_records.csv")
    output_records = output_records[output_records['request_id']==id_]

    df_test_pre, df_test_post, df_control_pre, df_control_post = ThisYear_LastYear.subset(input_df)
    assert df_test_pre.shape[0] == output_records.pre_test.values[0]
    assert df_test_post.shape[0] == output_records.post_test.values[0]
    assert df_control_pre.shape[0] == output_records.pre_control.values[0]
    assert df_control_post.shape[0] == output_records.post_control.values[0]


