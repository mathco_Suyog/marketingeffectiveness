from rest_framework.decorators import api_view
from rest_framework.response import Response
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from snowflake.sqlalchemy import URL
from .models import fc_schema
from .models import cred
from .algorithms import movingAverage, arima, deSeasonalizedArima, ucm, holtWinters
from .algorithms.modelPreprocessing import model_preprocess
from .algorithms.dataFetch import data_fetcher
from .algorithms.testControlSYDM import run_sydm
from .algorithms.testControlSMLY import run_smly
from .log_helpers import request_processing_start, request_processing_complete, model_run_completed, \
    request_add_log_message
import traceback
from .modelRunException import *

# API Route


@api_view(['POST'])
def getresult(request):
    engine = create_engine(URL(
       account=cred.account,
       user=cred.user,
       password=cred.password,
       database=cred.database,
       schema=cred.schema,
       warehouse=cred.warehouse,
       role=cred.role
    ), echo=False)

    fc_schema.Base.metadata.bind = engine
    db_session = sessionmaker(bind=engine)
    session = db_session()
    session.execute('alter session set ABORT_DETACHED_QUERY=TRUE')

    event_request_id = request.data['request_id']

    # Start processing
    request_processing_start(event_request_id, session)

    # Data fetch
    request_add_log_message(event_request_id, 'BEGIN: DATA FETCH', 0, session)
    filters_row = session.query(fc_schema.RequestFilters).filter(
        fc_schema.RequestFilters.request_id == event_request_id).one()

    input_df, perf_metric = data_fetcher(filters_row, session)

    log_message = f'END: DATA FETCH. {len(input_df)} rows of data fetched.'
    request_add_log_message(event_request_id, log_message, 0, session)

    try:
        if filters_row.comparison_technique == 'Actual vs. Expected':
            # PREPROCESS
            try:
                request_add_log_message(event_request_id, 'BEGIN: DATA PREPROCESS.', 0, session)
                train_data, test_data, data_sufficiency_key, train_data_adj = model_preprocess(df=input_df, filters_row=filters_row)
                request_add_log_message(event_request_id, (f'END: DATA PREPROCESS. '
                                                           f'{len(train_data)} rows in Training data. '
                                                           f'{len(test_data)} rows in Testing data. '
                                                           f'Data Sufficiency: {data_sufficiency_key}'),
                                        0, session)
                if data_sufficiency_key == 'Fail':
                    raise DataSufficiencyException()
            except DataSufficiencyException as e:
                request_add_log_message(event_request_id, f'FAILURE: DATA PREPROCESS run failed. {e.args[0]}', 3,
                                        session)
                raise e
            except Exception as e:
                traceback.print_exc()
                request_add_log_message(event_request_id,
                                        f'FAILURE: DATA PREPROCESS run failed. '
                                        f'Unhandled Error {e.args[0]}', 3,
                                        session)
                raise e

            # ARIMA
            try:
                request_add_log_message(event_request_id, 'BEGIN: ARIMA model run started.', 0, session)
                deSeasonalizedArima.de_seasonal_arima_code(train_data, test_data, data_sufficiency_key, train_data_adj, session)
                model_run_completed(event_request_id, session)
            except ModelRunException as e:
                request_add_log_message(event_request_id, f'FAILURE: ARIMA model run failed. {str(e)}', 3,
                                        session)
            except Exception as e:
                traceback.print_exc()
                request_add_log_message(event_request_id,
                                        f'FAILURE: ARIMA model run failed.'
                                        f'Unhandled Error: {e.args[0]}', 3, session)
            finally:
                request_add_log_message(event_request_id, 'END: ARIMA model run completed.', 0, session)

            # HOLT WINTERS
            try:
                request_add_log_message(event_request_id, 'BEGIN: HOLT WINTERS model run started.', 0, session)
                holtWinters.holt_winters_code(train_data, test_data, data_sufficiency_key, train_data_adj, session)
                model_run_completed(event_request_id, session)
            except ModelRunException as e:
                request_add_log_message(event_request_id, f'FAILURE: HOLT WINTERS model run failed. {str(e)}',
                                        3, session)
            except Exception as e:
                traceback.print_exc()
                request_add_log_message(event_request_id,
                                        f'FAILURE: HOLT WINTERS model run failed.'
                                        f'Unhandled Error: {e.args[0]}', 3, session)
            finally:
                request_add_log_message(event_request_id, 'END: HOLT WINTERS model run completed.', 0, session)

            # UCM
            try:
                request_add_log_message(event_request_id, 'BEGIN: UCM model run started.', 0, session)
                ucm.ucm_code(train_data, test_data, data_sufficiency_key, train_data_adj, session)
                model_run_completed(event_request_id, session)
            except ModelRunException as e:
                request_add_log_message(event_request_id, f'FAILURE: UCM model run failed. {str(e)}',
                                        3, session)
            except Exception as e:
                traceback.print_exc()
                request_add_log_message(event_request_id,
                                        f'FAILURE: UCM model run failed.'
                                        f'Unhandled Error: {e.args[0]}', 3, session)
            finally:
                request_add_log_message(event_request_id, 'END: UCM model run completed.', 0, session)

        elif filters_row.comparison_technique == 'Same Year - Different Market':
            try:
                request_add_log_message(event_request_id, 'BEGIN: SAME YEAR DIFFERENT MARKET model run started.',
                                        0, session)
                run_sydm(data=input_df, performanceMetric=perf_metric, session=session)
                model_run_completed(event_request_id, session)
            except ModelRunException as e:
                request_add_log_message(event_request_id,
                                        f'FAILURE: SAME YEAR DIFFERENT MARKET model run failed. {str(e)}',
                                        3, session)
            except Exception as e:
                traceback.print_exc()
                request_add_log_message(event_request_id,
                                        f'FAILURE: SAME YEAR DIFFERENT MARKET model run failed. '
                                        f'Unhandled Error: {e.args[0]}',
                                        3, session)
            finally:
                request_add_log_message(event_request_id, 'END: SAME YEAR DIFFERENT MARKET model run completed.',
                                        0, session)

        elif filters_row.comparison_technique == 'Same Market - Last Year':
            try:
                request_add_log_message(event_request_id, 'BEGIN: SAME MARKET LAST YEAR model run started.', 0, session)
                run_smly(data=input_df, perf_metric=perf_metric, session=session)
                model_run_completed(event_request_id, session)
            except ModelRunException as e:
                request_add_log_message(event_request_id,
                                        f'FAILURE: SAME MARKET LAST YEAR model run failed. {str(e)}',
                                        3, session)
            except Exception as e:
                traceback.print_exc()
                request_add_log_message(event_request_id,
                                        f'FAILURE: SAME MARKET LAST YEAR model run failed. '
                                        f'Unhandled Error: {e.args[0]}',
                                        3, session)
            finally:
                request_add_log_message(event_request_id, 'END: SAME MARKET LAST YEAR model run completed.', 0, session)
    except Exception as e:
        traceback.print_exc()
        session.rollback()
    finally:
        message = request_processing_complete(event_request_id, session)
        request_add_log_message(event_request_id, f'END: Request Completed. {message} models run successfully.',
                                0, session)
        session.close()
        return Response("Done")

