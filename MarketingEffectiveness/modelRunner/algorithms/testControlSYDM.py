import pandas as pd
from scipy import stats
import numpy as np
from datetime import datetime
from itertools import product
from ..modelRunException import DataSufficiencyException

class Test_Control(object):
        
    def __init__(self,df,performance_metric):
        
        self.performance_metric = performance_metric
        self.df = Test_Control.data_exists(df,self.performance_metric)
        self.df_test_pre, self.df_test_post, self.df_control_pre, self.df_control_post = Test_Control.subset(self.df)
        
        #calculate the mean,sample size, standard deviation, standard error for a performance metric
        # for each of test_pre, test_post,control_pre and control_post which will be used to 
        # calculate the overall lift, t_test and p_value for each of pre and post period

        #Mean  
        self.test_pre_mean = self.df_test_pre[self.performance_metric].mean()
        self.control_pre_mean = self.df_control_pre[self.performance_metric].mean()
        self.test_post_mean = self.df_test_post[self.performance_metric].mean()
        self.control_post_mean = self.df_control_post[self.performance_metric].mean()
        self.mean_pre = abs(self.test_pre_mean - self.control_pre_mean)
        self.mean_post = abs(self.test_post_mean - self.control_post_mean)

        #Sample Size
        self.test_pre_len = len(self.df_test_pre)
        self.control_pre_len = len(self.df_control_pre)
        self.test_post_len = len(self.df_test_post)
        self.control_post_len = len(self.df_control_post)

        #Standard Deviation
        self.test_pre_sd = self.df_test_pre[self.performance_metric].std()
        self.control_pre_sd = self.df_control_pre[self.performance_metric].std()
        self.test_post_sd = self.df_test_post[self.performance_metric].std()
        self.control_post_sd = self.df_control_post[self.performance_metric].std()

        #Standard Error
        self.test_pre_se  = self.test_pre_sd / np.sqrt(self.test_pre_len)
        self.control_pre_se = self.control_pre_sd / np.sqrt(self.control_pre_len)
        self.test_post_se = self.test_post_sd / np.sqrt(self.test_post_len)
        self.control_post_se = self.control_post_sd / np.sqrt(self.control_post_len)

        #Degress of Freedom
        self.test_pre_dof = self.test_pre_len - 1
        self.control_pre_dof = self.control_pre_len - 1
        self.dof_pre = (self.test_pre_se**2 + self.control_pre_se**2)**2\
                        /np.sum([(self.test_pre_se**4 / self.test_pre_dof),(self.control_pre_se**4 / self.control_pre_dof)])
        
        self.test_post_dof = self.test_post_len - 1
        self.control_post_dof =self.control_post_len - 1
        self.dof_post = (self.test_post_se **2 + self.control_post_se**2)**2\
                         /np.sum([(self.test_post_se**4/self.test_post_dof), (self.control_post_se**4/self.control_post_dof)])
        
       # T statistic
        self.t_stat_pre = self.mean_pre / np.sqrt(np.sum([(self.test_pre_sd**2 / self.test_pre_len),(self.control_pre_sd**2/self.control_pre_len)]))
        self.t_stat_post = self.mean_post / np.sqrt(np.sum([(self.test_post_sd**2 /self.test_post_len),(self.control_post_sd**2 / self.control_post_len)]))

      # P value
        self.p_val_pre = (1-(stats.t.cdf(abs(self.t_stat_pre),self.dof_pre)))*2
        self.p_val_pre = max(round(self.p_val_pre,4),0.0001)

        self.p_val_post = (1-(stats.t.cdf(abs(self.t_stat_post),self.dof_post)))*2
        self.p_val_post = max(round(self.p_val_post,4),0.0001)

        #Lift and incremental lift
        self.lift, self.incremental_lift = Test_Control.model_results(self.df_test_pre,self.df_test_post,
                                                                      self.df_control_pre,self.df_control_post,
                                                                      self.performance_metric)
    
    @staticmethod    
    def data_exists(df,performance_metric):
        if df.empty:
            raise DataSufficiencyException()

        columns = ['date',"'flag'",performance_metric,'request_id','comparison_technique']
        df = (df.loc[df["'flag'"]!='ignore',columns]
              .groupby(['date',"'flag'",'request_id','comparison_technique'])[performance_metric]
              .sum()
               .reset_index()
            )
        return df
            
    @staticmethod
    def subset(df):
        df_test_pre = df[df["'flag'"] == 'Pre_Test']
        df_test_post = df[df["'flag'"] == 'Post_Test']
        df_control_pre = df[df["'flag'"] == 'Pre_Control']
        df_control_post = df[df["'flag'"] == 'Post_Control']
        if df_test_pre.empty or df_test_post.empty or df_control_pre.empty or df_control_post.empty:
            raise DataSufficiencyException()
        return (df_test_pre, df_test_post, df_control_pre, df_control_post)

    # @staticmethod
    # def p_value(tStatPre,dofPre):
    #     if p_val>0.05:
    #         status=['High']
    #     else:
    #         status=['Low']
    #     return(p_val,status)

    @staticmethod   
    def model_results(df_test_pre,df_test_post,df_control_pre,df_control_post,performance_metric):
        test_lift = df_test_post[performance_metric].sum()/df_test_pre[performance_metric].sum()-1
        control_lift = df_control_post[performance_metric].sum()/df_control_pre[performance_metric].sum()-1
        lift = test_lift - control_lift
        incremental_lift = lift * df_test_pre[performance_metric].sum()
        return (lift,incremental_lift)
    
def run_sydm(data, performanceMetric, session):
    perf_met={'Sales':'total_sales','Orders':'total_orders','Customers':'total_customer','Visits':'total_visits'}
    performance_metric=perf_met.get(performanceMetric,None)
    if performance_metric == None:
        raise Exception("Undefined performance metric encountered.")
    
    #create an object
    test_control=Test_Control(data,performance_metric)

    # Create test_control records table to push the results to  test_control_records sitting in Snowflake
    column_records=['request_id', 'experiment_param', 'experiment_date', 'experiment_period', 'experiment_selection_type'\
                    ,'param_units', 'param_value', '_row_created_at', '_row_updated_at', 'comparison_technique']

    param_units={'total_sales':'USD','total_orders':'Qty','total_customer':'People'}

    test_control_records=pd.DataFrame(columns = column_records,index = range(0, test_control.df.shape[0]))
    test_control_records['request_id'] = test_control.df['request_id']
    test_control_records['experiment_param'] = performance_metric
    test_control_records['experiment_date'] = test_control.df['date']
    test_control_records['experiment_period'] = [str(test_control.df["'flag'"][i]).split('_')[0] for i in range(test_control.df.shape[0])]
    test_control_records['experiment_selection_type'] = [str(test_control.df["'flag'"][i]).split('_')[1] for i in range(test_control.df.shape[0])]
    test_control_records['param_units'] = param_units.get(performance_metric,'')
    test_control_records['param_value'] = test_control.df[performance_metric]
    test_control_records['_row_created_at'] = datetime.utcnow()
    test_control_records['_row_updated_at'] = test_control_records['_row_created_at']
    test_control_records['comparison_technique'] = test_control.df['comparison_technique']

    test_control_records.to_sql('test_control_records',session.bind,if_exists = 'append',index = False)

    column_metrics=['_row_created_at','_row_updated_at','abs_uplift','abs_lift_per_day','confidence','control_pre_vs_post'\
                    ,'pct_uplift','request_id','t_test_confidence_interval','t_test_p_value','test_pre_vs_post']

    test_control_metrics=pd.DataFrame(columns = column_metrics,index = [0])
    test_control_metrics['_row_created_at'] = datetime.now()
    test_control_metrics['_row_updated_at'] = test_control_metrics['_row_created_at']
    test_control_metrics['abs_uplift'] = test_control.incremental_lift
    test_control_metrics['abs_lift_per_day'] = round(test_control.incremental_lift/((test_control.df_test_post.date.max()-test_control.df_test_post.date.min()).days+1),0)
    test_control_metrics['confidence'] = ''
    test_control_metrics['control_pre_vs_post'] = test_control.df_control_post[performance_metric].sum()
    test_control_metrics['pct_uplift'] = test_control.lift
    test_control_metrics['request_id'] = test_control.df['request_id']
    test_control_metrics['t_test_confidence_interval'] = 95
    test_control_metrics['t_test_p_value'] = test_control.p_val_pre
    test_control_metrics['test_pre_vs_post'] = test_control.df_test_post[performance_metric].sum()

    test_control_metrics.to_sql('test_control_metrics',session.bind,if_exists = 'append',index = False)

    market = ['Test', 'Control']
    period = ['Pre', 'Post']
    metric = ['Mean', 'SD', 't-stat', 'p-value']
    request_id = test_control.df.request_id.unique()[0]

    test_control_stats_metrics = {'request_id': [], 'market': [], 'period': [],'metric':[],'value':[],
                                  '_row_created_at':[],'_row_updated_at':[]}
    for i in product(market,period,metric):
        test_control_stats_metrics['request_id'].append(request_id)
        test_control_stats_metrics['market'].append(i[0])
        test_control_stats_metrics['period'].append(i[1])
        test_control_stats_metrics['metric'].append(i[2])
        test_control_stats_metrics['value'].append(0)
        test_control_stats_metrics['_row_created_at'].append(datetime.utcnow())
        test_control_stats_metrics['_row_updated_at'].append(datetime.utcnow())

    df=pd.DataFrame.from_dict(test_control_stats_metrics)
    df.loc[(df['market']=='Test') & (df['period']=='Pre') & (df['metric']=='Mean'),'value']=round(test_control.test_pre_mean,1)
    df.loc[(df['market']=='Test') & (df['period']=='Pre') & (df['metric']=='SD'),'value']=round(test_control.test_pre_sd,1)
    df.loc[(df['market']=='Test') & (df['period']=='Pre') & (df['metric']=='t-stat'),'value']=round(test_control.t_stat_pre,1)
    df.loc[(df['market']=='Test') & (df['period']=='Pre') & (df['metric']=='p-value'),'value']=test_control.p_val_pre
    df.loc[(df['market']=='Test') & (df['period']=='Post') & (df['metric']=='Mean'),'value']=round(test_control.test_post_mean,1)
    df.loc[(df['market']=='Test') & (df['period']=='Post') & (df['metric']=='SD'),'value']=round(test_control.test_post_sd,1)
    df.loc[(df['market']=='Test') & (df['period']=='Post') & (df['metric']=='t-stat'),'value']=round(test_control.t_stat_post,1)
    df.loc[(df['market']=='Test') & (df['period']=='Post') & (df['metric']=='p-value'),'value']=test_control.p_val_post
    df.loc[(df['market']=='Control') & (df['period']=='Pre') & (df['metric']=='Mean'),'value']=round(test_control.control_pre_mean,1)
    df.loc[(df['market']=='Control') & (df['period']=='Pre') & (df['metric']=='SD'),'value']=round(test_control.control_pre_sd,1)
    df.loc[(df['market']=='Control') & (df['period']=='Pre') & (df['metric']=='t-stat'),'value']=round(test_control.t_stat_pre,1)
    df.loc[(df['market']=='Control') & (df['period']=='Pre') & (df['metric']=='p-value'),'value']=test_control.p_val_pre
    df.loc[(df['market']=='Control') & (df['period']=='Post') & (df['metric']=='Mean'),'value']=round(test_control.control_post_mean,1)
    df.loc[(df['market']=='Control') & (df['period']=='Post') & (df['metric']=='SD'),'value']=round(test_control.control_post_sd,1)
    df.loc[(df['market']=='Control') & (df['period']=='Post') & (df['metric']=='t-stat'),'value']=round(test_control.t_stat_post,1)
    df.loc[(df['market']=='Control') & (df['period']=='Post') & (df['metric']=='p-value'),'value']=test_control.p_val_post

    df.to_sql('test_control_stats_metrics',session.bind,if_exists='append',index=False)   