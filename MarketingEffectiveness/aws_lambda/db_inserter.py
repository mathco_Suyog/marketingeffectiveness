import uuid
import json
import hashlib
import datetime
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from snowflake.sqlalchemy import URL
import cred
from fc_schema import Base, RequestDetails, RequestFilters

from pprint import pprint

from sqlalchemy.exc import ProgrammingError


def lambda_handler(event, context):
    """
    Handles the submission of data through the UI and updates 'REQUEST_DETAILS' and 'REQUEST_FILTERS' tables in
    Snowflake with relevant details.
    Returns the request id for this request.
    """

    print(event)

    engine = create_engine(URL(
        account=cred.account,
        user=cred.user,
        password=cred.password,
        database=cred.database,
        schema=cred.schema,
        warehouse=cred.warehouse,
        role=cred.role
    ), echo=False)

    Base.metadata.bind = engine
    DBSession = sessionmaker(bind=engine)
    session = DBSession()
    session.execute('alter session set ABORT_DETACHED_QUERY=TRUE')

    request_id = str(uuid.uuid4())

    # Default model run counts for different comparison techniques.
    model_run_count = {
        'Actual vs. Expected': 3,
        'Same Year - Different Market': 1,
        'Same Market - Last Year': 1
    }

    # Create the 'REQUEST_DETAILS' object
    row_request_details = RequestDetails(
        request_id=request_id,
        status_code=2,  # Status code - 2 = 'QUEUED'
        user_id=event.get('email', ''),  # The user's email id from frontend
        created_at_ts_utc=datetime.datetime.utcnow(),
        models_run_total=model_run_count.get(event.get('comparison_technique',''), 0),

        _row_created_at=datetime.datetime.utcnow(),
        _row_updated_at=datetime.datetime.utcnow()
    )

    # Create the 'REQUEST_FILTERS' object
    row_request_filters = RequestFilters(
        request_id=request_id,
        acquisition_channel_type=event.get('acquisition_channel_type', ''),
        acquisition_channel_paid_free=event.get('acquisition_channel_paid_free', ''),
        acquisition_channel_funnel=event.get('acquisition_channel_funnel', ''),
        business_channel=event.get('business_channel', ''),
        comparison_technique=event.get('comparison_technique', ''),

        customer_type=event.get('customer_type', ''),

        performance_metric=event.get('performance_metric', ''),

        product_category=event.get('product_category', ''),
        product_sub_category=event.get('product_sub_category', ''),

        request_data=json.dumps(event),  # The entire request parameters as a JSON
        # request_data=str(event),

        # TODO: Implement not including dates in the signature
        request_signature=hashlib.md5(json.dumps(event).encode()).hexdigest(),  # MD5 hash of all filters.

        test_end_date=event.get('test_end_date', None),
        test_start_date=event.get('test_start_date', None),
        test_geo_city=event.get('test_geo_city', ''),
        test_geo_country=event.get('test_geo_country', ''),
        test_geo_dma=event.get('test_geo_dma', ''),
        test_geo_region=event.get('test_geo_region', ''),
        test_product_material=event.get('test_product_material', ''),
        test_product_style=event.get('test_product_style', ''),

        visitor_type=event.get('visitor_type', ''),

        _row_created_at=datetime.datetime.utcnow(),
        _row_updated_at=datetime.datetime.utcnow()
    )

    def flatten(container):
        for i in container:
            if isinstance(i, (list, tuple)):
                for j in flatten(i):
                    yield j
            else:
                yield i
    
    row_request_filters.control_end_date=None
    row_request_filters.control_start_date=None
    row_request_filters.control_geo_city=''
    row_request_filters.control_geo_country=''
    row_request_filters.control_geo_dma=''
    row_request_filters.control_geo_region=''
    row_request_filters.control_product_material=''
    row_request_filters.control_product_style=''

    if row_request_filters.comparison_technique == 'Same Year - Different Market':

        row_request_filters.control_end_date=event.get('control_end_date', None)
        row_request_filters.control_start_date=event.get('control_start_date', None)
        row_request_filters.control_geo_city=event.get('control_geo_city', '')
        row_request_filters.control_geo_country=event.get('control_geo_country', '')
        row_request_filters.control_geo_dma=event.get('control_geo_dma', '')
        row_request_filters.control_geo_region=event.get('control_geo_region', '')
        row_request_filters.control_product_material=event.get('control_product_material', '')
        row_request_filters.control_product_style=event.get('control_product_style', '')
    
    elif row_request_filters.comparison_technique == 'Same Market - Last Year':
    
        row_request_filters.control_end_date=event.get('control_end_date', None)
        row_request_filters.control_start_date=event.get('control_start_date', None)

    else:
        None

    # Mandatory field check
    if not row_request_filters.test_product_material:
        row_request_filters.test_product_material = ''
    if not row_request_filters.test_product_style:
        row_request_filters.test_product_style = ''
    if not row_request_filters.product_category:
        row_request_filters.product_category = ''
    if not row_request_filters.product_sub_category:
        row_request_filters.product_sub_category = ''

    row_request_filters.control_geo_country = ','.join(list(flatten(row_request_filters.control_geo_country)))
    row_request_filters.control_geo_region = ','.join(list(flatten(row_request_filters.control_geo_region)))
    row_request_filters.control_geo_dma = ','.join(list(flatten(row_request_filters.control_geo_dma)))
    row_request_filters.control_geo_city = ','.join(list(flatten(row_request_filters.control_geo_city)))

    row_request_filters.product_category = ','.join(list(flatten(row_request_filters.product_category)))
    row_request_filters.product_sub_category = ','.join(list(flatten(row_request_filters.product_sub_category)))

    row_request_filters.test_geo_country = ','.join(list(flatten(row_request_filters.test_geo_country)))
    row_request_filters.test_geo_region = ','.join(list(flatten(row_request_filters.test_geo_region)))
    row_request_filters.test_geo_dma = ','.join(list(flatten(row_request_filters.test_geo_dma)))
    row_request_filters.test_geo_city = ','.join(list(flatten(row_request_filters.test_geo_city)))
    row_request_filters.acquisition_channel_type = ','.join(list(flatten(row_request_filters.acquisition_channel_type)))
    # for k, v in vars(row_request_filters).items():
    #     print(k, ':', v, type(v))

    # pprint(row_request_filters.__dict__)

    try:
        session.add(row_request_details)
        session.add(row_request_filters)
        session.commit()
    except ProgrammingError as e:
        session.rollback()
        # print(e)
        return {'status': 400, 'error_message': 'Bad input, could not write to database. There was a problem while '
                                                'writing filter selection to the database. This can happen if there '
                                                'are missing values in the required fields before submission. Please '
                                                'check the input data once again. If the problem persists, '
                                                'please reach out to the development team with a screen shot of the '
                                                'input form fields.'}


    return {'status': 200, 'request_id': request_id, 'request_status': row_request_details.status_code}


if __name__ == '__main__':
    print(lambda_handler(event={'email': 'jaspinder.singh@themathcompany.com', 'performance_metric': 'Visits', 'acquisition_channel_type': 'All', 
    'acquisition_channel_paid_free': 'All', 'acquisition_channel_funnel': 'All', 'business_channel': 'Total', 'comparison_technique': 'Actual vs. Expected', 
    'control_end_date': '2019-03-18', 'control_start_date': '2019-03-01', 'control_geo_city': [], 'control_geo_country': ['UNITED STATES'], 
    'control_geo_dma': [], 'control_geo_region': [], 'control_product_material': None, 'control_product_style': None, 'customer_type': 'All', 
    'product_category': None, 'product_sub_category': None, 'test_end_date': '2019-03-18', 'test_start_date': '2019-03-01', 'test_geo_city': [['All']], 
    'test_geo_country': ['UNITED STATES'], 'test_geo_dma': [['All']], 'test_geo_region': [['CALIFORNIA']], 'test_product_material': ['All'], 
    'test_product_style': ['All'], 'visitor_type': 'All'}, context=None))


