from datetime import datetime
import datetime
import pandas as pd
import numpy as np
from modelRunner.models.fc_schema import ModelForecastMetrics, ModelForecasts
import itertools
import statsmodels.api as sm
from statsmodels.tsa.stattools import adfuller
from statsmodels.tsa.arima_model import ARIMA
import impyute as impy
from ..log_helpers import request_add_log_message
from ..modelRunException import MAPEException, DataSufficiencyException, NoOptimalSolutionException


class deARIMAfns(object):
    @staticmethod
    def grid_search_ARIMA(df,dep_var,max_p=5,max_d=2,max_q=5,silent=True):
        """
        This function runs a grid search for ARIMA
         """   
        # Initializng the parameters
        p=range(0,max_p)
        d=range(0,max_d)
        q=range(0,max_q)

        # Generate all different combinations of p, q and q triplets
        pdq = list(itertools.product(p, d, q))
        best_mape = np.inf
        best_param = None

        # Gridsearch to look for optimial arima parameters
        for param in pdq:       
            try:
                results = ARIMA(df[dep_var],order=param,exog=None, dates=None, freq='D', missing='none')
                results = results.fit(disp=0)
                residuals = results.resid
                mape = np.mean(np.abs((residuals)/df[dep_var]))

            # If current run of MAPE is better than the best one so far, overwrite it
                if (mape < best_mape) and (mape > 0.1):
                    best_mape = mape
                    best_param = param

            except Exception as e:
                continue

        if best_param is None or best_mape == np.inf:
            raise NoOptimalSolutionException(len(pdq))

        return best_param, best_mape
        
    @staticmethod
    def model_fit_arima(df,dep_var,best_param):
        """
        This function runs ARIMA and returns the model fit
        """
        
        model_fit = ARIMA(df[dep_var], order=(best_param[0],best_param[1],best_param[2]), 
                          exog=None, dates=None, freq='D', missing='none').fit(disp=0)
        return(model_fit)
    
    @staticmethod
    def mape(self,dep_var,model_fit):
        """
        This function runs moving average for test
        """
   
        residuals = model_fit.resid
        mape = np.mean(np.abs((residuals)/self[dep_var]))
        return(mape)
    
    @staticmethod
    def train_pred(self,self_var,model_fit,diff):
        """
        This function predicts for train data
        """
        residuals = model_fit.resid
        if dep_var is not None:
            self['PREDICTED'] =model_fit.predict(start=diff,end=len(self))
        else:
            self['PREDICTED'] =self['adjusted_kpi']-residuals
        return (self)

    @staticmethod
    def test_pred(df,dep_var,model_fit):

        """
        This function forecasts the on test data
        @params: 
            df: Test Data
            dep_var: Independent Variable
            model_fit: fitted ARIMA model
        """

        if dep_var is not None:
            df['predicted']=model_fit.forecast(df.shape[0])[0] #- This was there.
            # df['predicted_kpi']=np.array(model_fit.forecast(len(df)))
                # except:
                #     pass
            # mape = np.mean(np.abs((df[dep_var]-df['predicted_kpi']) / df[dep_var]))
        else:
            # mape = np.inf
            pass
        return (df)

    @staticmethod
    def decomposition_function(df,adjusted_kpi):
        """
        This function splits the data into trend, seasonal and random components
        """
        df_series = pd.Series(data=df[adjusted_kpi])
        numpyArrayKPI = np.array(df_series.resample('D').sum(),dtype=float)           
        decomposition = sm.tsa.seasonal_decompose(numpyArrayKPI, model='multiplicative',freq=365)
        return(decomposition)

    @staticmethod
    def trend_resid_data(df,trend_series,residual_series,seasonal_series,observed_series):
        """
        This is used to get the data with just trend and residuals
        """
        # Multiplicative Seasonality            
        df_trend_resid = pd.DataFrame({'trend_residual':observed_series/seasonal_series, 'observed':observed_series})
        # Imputing the data using KNN imputation            
        # df_trend_resid = round(impy.fast_knn(df_trend_resid,k=3),0)
        # df_trend_resid.columns = ["observed","trend_residual"]
        df['train_date'] = df.index
        df_trend_resid = pd.concat([df.reset_index(drop=True),df_trend_resid],axis=1)
        df_trend_resid = df_trend_resid.set_index('train_date')
        return(df_trend_resid)

    @staticmethod
    def seasonalizing_data(df,predicted,seasonal_series):
        """
        Adding seasonality back into the data after predicting
        """
        df.insert(0, 'merge_key', range(0,len(df)))
        seasonal_comp = pd.DataFrame({'seasonal':seasonal_series})
        seasonal_comp.insert(0, 'merge_key', range(0,len(seasonal_comp)))
        df =  df.join(seasonal_comp,on=['merge_key'], how='left',lsuffix='_left', rsuffix='_right')
        # Multiplicative Seasonality
        df['predicted_kpi'] =  df[predicted] * df.seasonal
        return(df)

    @staticmethod
    def residual_approach(test_data):
        """
        Implementing residual approach
        """
        residual_df = test_data.groupby(['period'],as_index = False)['adjusted_kpi','predicted_kpi'].sum()
        test_residual_factor_df = residual_df[residual_df['period']=='Pre']
        residual_factor = (test_residual_factor_df['adjusted_kpi'].iloc[0] - test_residual_factor_df['predicted_kpi'].iloc[0]) / test_residual_factor_df['predicted_kpi'].iloc[0]
        test_absolute_value = residual_df[residual_df['period']=='Post']
        absolute_value = sum(test_absolute_value['predicted_kpi'], (test_absolute_value['predicted_kpi']*residual_factor))
        test_data['pct_contribution'] = test_data['predicted_kpi']/test_absolute_value['predicted_kpi'].sum()
        for i in range(len(test_data)):
            if test_data['period'][i] == 'Pre':
                test_data['pct_contribution'][i] = np.NaN
        test_data['absolute_value'] = absolute_value.iloc[0]
        test_data['pred_kpi'] = test_data['pct_contribution'] * test_data['absolute_value']
        test_data_pre = test_data[test_data['period']=='Pre']
        test_data_post = test_data[test_data['period']=='Post']
        return test_data_pre, test_data_post    

    @staticmethod
    def uplift(test_data):
        """
        A function to calculate the total lift in units (both positive or negative) for test timeframe
        """
        uplift = test_data['adjusted_kpi'].sum()-test_data['pred_kpi'].sum()
        return uplift

    @staticmethod
    def lift_pct(test_data):
        """
        A function to calculate the total lift % (both positive or negative) for test timeframe
        """
        lift = (test_data['adjusted_kpi'].sum()-test_data['pred_kpi'].sum())/test_data['pred_kpi'].sum()
        return lift

def de_seasonal_arima_code(train_data, test_data, data_sufficiency_key, train_data_adj, session):

    request_id = train_data.request_id.iloc[0]
    if data_sufficiency_key != 'Pass':
        raise DataSufficiencyException()

    # Creating copies of the train and test data
    modeling_train_data = train_data.copy()
    modeling_test_data = test_data.copy()

    # Setting index to test and train data
    modeling_train_data = modeling_train_data.set_index('train_date')
    modeling_test_data = modeling_test_data.set_index('test_date')

    decompose = deARIMAfns.decomposition_function(modeling_train_data,'adjusted_kpi')
    df_trend_resid = deARIMAfns.trend_resid_data(modeling_train_data,decompose.trend,decompose.resid,decompose.seasonal,
                                    decompose.observed)

    # Calculating the best  ARIMA parameters for the least MAPE
    best_param, best_mape = deARIMAfns.grid_search_ARIMA(df_trend_resid,'trend_residual')

    # Predicting on the test data with optimized parameters
    model_fit = ARIMA(df_trend_resid['trend_residual'], order=(best_param[0],best_param[1],best_param[2]), exog=None, dates=None, freq='D', missing='none').fit(disp=0)
    test_pred_trend_resid = deARIMAfns.test_pred(modeling_test_data,'adjusted_kpi',model_fit)

    # Adding seasonality back into the model
    modeling_test_data = deARIMAfns.seasonalizing_data(test_pred_trend_resid,'predicted',decompose.seasonal)

    # Residual component correction
    modeling_test_data_pre, modeling_test_data_post = deARIMAfns.residual_approach(modeling_test_data)

    confidence = ''
    # Confidence Level
    if best_mape > 0.3:
        confidence = 'Low'
    else:
        confidence = 'High'

    # Metric calculation
    uplift = deARIMAfns.uplift(modeling_test_data_post)
    lift = deARIMAfns.lift_pct(modeling_test_data_post)
    avg_lift = uplift/len(modeling_test_data_post)
    KPI = modeling_train_data['performance_metric'][0]

    param_mapping = {
        'total_sales': 'SALES',
        'total_customer': 'CUSTOMER COUNT',
        'total_orders': 'ORDER COUNT',
        'total_visits': 'VISIT COUNT',
        'conversion rate': 'CONVERSION RATE'
    }

    units_mapping = {
        'total_sales': 'USD',
        'conversion rate': 'PERCENTAGE'
    }

    METRICS = param_mapping.get(KPI, '')
    UNITS = units_mapping.get(KPI, '')

    model_results = pd.DataFrame(columns=['ID',
                                          'REQUEST_ID',
                                          'FORECAST_AVG_LIFT',
                                          'FORECAST_AVG_TRAIN_ERROR',
                                          'FORECAST_TECHNIQUE',
                                          '_ROW_CREATED_AT',
                                          '_ROW_UPDATED_AT',
                                          'FORECAST_ACTUALS',
                                          'FORECAST_EXPECTED',
                                          'CONFIDENCE',
                                          'ABS_UPLIFT'])

    model_graph_train = pd.DataFrame(columns=['ID',
                                             'REQUEST_ID',
                                             'ACTUAL_VALUE',
                                             'FORECAST_DATE',
                                             'FORECAST_PARAM',
                                             'FORECAST_TECHNIQUE',
                                             'FORECAST_UNITS',
                                             'FORECAST_VALUE',
                                             '_ROW_CREATED_AT',
                                             '_ROW_UPDATED_AT',
                                             'FORECAST_RESIDUAL_VALUE'])

    model_graph_test = pd.DataFrame(columns=['ID',
                                             'REQUEST_ID',
                                             'ACTUAL_VALUE',
                                             'FORECAST_DATE',
                                             'FORECAST_PARAM',
                                             'FORECAST_TECHNIQUE',
                                             'FORECAST_UNITS',
                                             'FORECAST_VALUE',
                                             '_ROW_CREATED_AT',
                                             '_ROW_UPDATED_AT',
                                             'FORECAST_RESIDUAL_VALUE'])

    if best_mape < 0.5:
        model_results = model_results.append({'ID':range(0 , len(model_graph_train)),
                                              'REQUEST_ID':train_data_adj['request_id'][0],
                                              'FORECAST_AVG_LIFT':lift,
                                              'FORECAST_AVG_TRAIN_ERROR':1-best_mape,
                                              'FORECAST_TECHNIQUE':'ARIMA',
                                              '_ROW_CREATED_AT':datetime.datetime.utcnow(),
                                              '_ROW_UPDATED_AT':datetime.datetime.utcnow(),
                                              'FORECAST_ACTUALS':modeling_test_data_post['adjusted_kpi'].sum(),
                                              'FORECAST_EXPECTED':modeling_test_data_post['pred_kpi'].sum(),
                                              'CONFIDENCE':confidence,
                                              'ABS_UPLIFT': uplift,
                                              'AVG_LIFT_PER_DAY':avg_lift
                                            },ignore_index=True)

    model_graph = None

    if best_mape >= 0.5:
        raise MAPEException(best_mape)

    model_graph_train['ACTUAL_VALUE'] = modeling_test_data_pre['adjusted_kpi']
    model_graph_train['FORECAST_DATE'] = modeling_test_data_pre.index.values
    model_graph_train['REQUEST_ID'] = modeling_test_data_pre['request_id'][0]
    model_graph_train['FORECAST_PARAM'] = METRICS
    model_graph_train['FORECAST_TECHNIQUE'] = 'ARIMA'
    model_graph_train['FORECAST_UNITS'] = UNITS
    model_graph_train['_ROW_CREATED_AT'] = str(datetime.datetime.utcnow())
    model_graph_train['_ROW_UPDATED_AT'] = str(datetime.datetime.utcnow())
    model_graph_train['FORECAST_VALUE'] = modeling_test_data_pre['predicted_kpi']
    model_graph_train['FORECAST_RESIDUAL_VALUE'] = None 

    model_graph_test['ACTUAL_VALUE'] = modeling_test_data_post['adjusted_kpi']
    model_graph_test['FORECAST_DATE'] = modeling_test_data_post.index.values
    model_graph_test['REQUEST_ID'] = modeling_test_data_post['request_id'][0]
    model_graph_test['FORECAST_PARAM'] = METRICS
    model_graph_test['FORECAST_TECHNIQUE'] = 'ARIMA'
    model_graph_test['FORECAST_UNITS'] = UNITS
    model_graph_test['_ROW_CREATED_AT'] = str(datetime.datetime.utcnow())
    model_graph_test['_ROW_UPDATED_AT'] = str(datetime.datetime.utcnow())
    model_graph_test['FORECAST_VALUE'] = modeling_test_data_post['predicted_kpi']
    model_graph_test['FORECAST_RESIDUAL_VALUE'] = modeling_test_data_post['pred_kpi']

    model_graph = model_graph_train.append(model_graph_test,ignore_index=True)

    for row in model_results.itertuples():
        row_forecast_metrics = ModelForecastMetrics(
            request_id = row.REQUEST_ID,
            forecast_avg_lift = row.FORECAST_AVG_LIFT,
            forecast_avg_train_error = row.FORECAST_AVG_TRAIN_ERROR,
            forecast_technique = row.FORECAST_TECHNIQUE,
            _row_created_at = datetime.datetime.utcnow(),
            _row_updated_at = datetime.datetime.utcnow(),
            forecast_actuals = row.FORECAST_ACTUALS,
            forecast_expected = row.FORECAST_EXPECTED,
            confidence = row.CONFIDENCE,
            abs_uplift = row.ABS_UPLIFT,
            forecast_avg_per_day = row.AVG_LIFT_PER_DAY
        )
        session.add(row_forecast_metrics)

    model_graph_rows = []
    for row in model_graph.itertuples():
        row_forecast_values = ModelForecasts(
            request_id = row.REQUEST_ID,
            actual_value = row.ACTUAL_VALUE,
            forecast_date = datetime.datetime.strptime(str(row.FORECAST_DATE), '%Y-%m-%d %H:%M:%S'),
            forecast_param = row.FORECAST_PARAM,
            forecast_technique = row.FORECAST_TECHNIQUE,
            forecast_units = row.FORECAST_UNITS,
            forecast_value = row.FORECAST_VALUE,
            _row_created_at = datetime.datetime.utcnow(),
            _row_updated_at = datetime.datetime.utcnow(),
            forecast_residual_value = row.FORECAST_RESIDUAL_VALUE
        )

        model_graph_rows.append(row_forecast_values)

    session.bulk_save_objects(model_graph_rows)
    session.commit()

    # Update request log
    log_message = ('ARIMA run completed, {} result rows inserted, '
                   'MAPE: {:.2f}'.format(len(model_graph_rows), best_mape))
    request_add_log_message(request_id, log_message, 0, session)