"""Schemas for all tables to be created."""

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, Date, UniqueConstraint, DateTime, ForeignKeyConstraint, Float, Sequence
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from snowflake.sqlalchemy import URL
import cred
import datetime

Base = declarative_base()


class RequestStatusCodes(Base):
    """
    Stores the status codes for all requests
    """
    __tablename__ = 'request_status_codes'

    id = Column(Integer(), Sequence('id_seq'), primary_key=True)

    status_message = Column(String(), nullable=False)
    status_type = Column(String(), nullable=False)

    _row_created_at = Column(DateTime(timezone=True), nullable=False, default=datetime.datetime.utcnow())
    _row_updated_at = Column(DateTime(timezone=True), nullable=False, default=datetime.datetime.utcnow(),
                             onupdate=datetime.datetime.utcnow())


class RequestDetails(Base):
    """
    Stores the basic informational details about a request.
    """

    __tablename__ = 'request_details'

    id = Column(Integer(), Sequence('id_seq'), primary_key=True)
    request_id = Column(String(), nullable=False)

    models_run_completed = Column(Integer(), nullable=False, default=0)
    models_run_total = Column(Integer(), nullable=False)

    created_at_ts_utc = Column(DateTime(timezone=True), default=datetime.datetime.utcnow())
    process_end_at_ts_utc = Column(DateTime(timezone=True))
    process_start_at_ts_utc = Column(DateTime(timezone=True))

    status_code = Column(Integer(), nullable=False)

    user_id = Column(String(), nullable=False)  # The user's email id from frontend

    _row_created_at = Column(DateTime(timezone=True), nullable=False, default=datetime.datetime.utcnow())
    _row_updated_at = Column(DateTime(timezone=True), nullable=False, default=datetime.datetime.utcnow(),
                             onupdate=datetime.datetime.utcnow())

    __table_args__ = (
        UniqueConstraint('request_id'),
        ForeignKeyConstraint(['status_code'], ['request_status_codes.id'])
    )


class RequestFilters(Base):
    """
    Stores the filter selections as retrieved by from the frontend
    """
    __tablename__ = 'request_filters'

    id = Column(Integer(), Sequence('id_seq'), primary_key=True)
    request_id = Column(String(), nullable=False)  # Unique GUID

    acquisition_channel_type = Column(String())  #
    acquisition_channel_paid_free = Column(String())  #
    acquisition_channel_funnel = Column(String())  #

    business_channel = Column(String(), nullable=False)  #

    comparison_technique = Column(String(), nullable=False)  #

    control_end_date = Column(Date())
    control_start_date = Column(Date())
    control_geo_city = Column(String())
    control_geo_country = Column(String())
    control_geo_dma = Column(String())
    control_geo_region = Column(String())
    control_product_material = Column(String())
    control_product_style = Column(String())

    customer_type = Column(String(), nullable=False)  #

    performance_metric = Column(String(), nullable=False)  #

    product_category = Column(String())  #
    product_sub_category = Column(String())  #

    request_data = Column(String())  # The entire request parameters as a JSON
    request_signature = Column(String())  # MD5 hash of all filters

    test_end_date = Column(Date(), nullable=False)
    test_start_date = Column(Date(), nullable=False)
    test_geo_city = Column(String(), nullable=False)
    test_geo_country = Column(String(), nullable=False)
    test_geo_dma = Column(String(), nullable=False)
    test_geo_region = Column(String(), nullable=False)
    test_product_material = Column(String(), nullable=False)
    test_product_style = Column(String(), nullable=False)

    visitor_type = Column(String())  #

    _row_created_at = Column(DateTime(timezone=True), nullable=False, default=datetime.datetime.utcnow())
    _row_updated_at = Column(DateTime(timezone=True), nullable=False, default=datetime.datetime.utcnow(),
                             onupdate=datetime.datetime.utcnow())

    __table_args__ = (
        UniqueConstraint('request_id'),
        ForeignKeyConstraint(['request_id'], ['request_details.request_id'])
    )


class RequestLog(Base):
    """
    Stores the request tracking log
    """
    __tablename__ = 'request_log'

    id = Column(Integer(), Sequence('id_seq'), primary_key=True)
    request_id = Column(String(), nullable=False)

    created_at_ts_utc = Column(DateTime(timezone=True), nullable=False, default=datetime.datetime.utcnow())

    log_message = Column(String(), nullable=False)
    log_status_code = Column(Integer(), nullable=False)

    _row_created_at = Column(DateTime(timezone=True), nullable=False, default=datetime.datetime.utcnow())
    _row_updated_at = Column(DateTime(timezone=True), nullable=False, default=datetime.datetime.utcnow(),
                             onupdate=datetime.datetime.utcnow())

    __table_args__ = (
        ForeignKeyConstraint(['log_status_code'], ['request_status_codes.id']),
        ForeignKeyConstraint(['request_id'], ['request_details.request_id'])
    )


class ModelForecasts(Base):
    """
    Stores the forecast results from the Models
    """
    __tablename__ = 'model_forecasts'

    id = Column(Integer(), Sequence('id_seq'), primary_key=True)
    request_id = Column(String(), nullable=False)

    actual_value = Column(Float(), nullable=False)
    forecast_date = Column(Date(), nullable=False)
    forecast_param = Column(String(), nullable=False)
    forecast_technique = Column(String(), nullable=False)
    forecast_units = Column(String())
    forecast_value = Column(Float(), nullable=True)
    forecast_residual_value = Column(Float(), nullable=True)

    _row_created_at = Column(DateTime(timezone=True), nullable=False, default=datetime.datetime.utcnow())
    _row_updated_at = Column(DateTime(timezone=True), nullable=False, default=datetime.datetime.utcnow(),
                             onupdate=datetime.datetime.utcnow())

    __table_args__ = (
        ForeignKeyConstraint(['request_id'], ['request_details.request_id']),
    )


class ModelForecastMetrics(Base):
    """
    Stores the metrics for the forecast models.
    """
    __tablename__ = 'model_forecast_metrics'

    id = Column(Integer(), Sequence('id_seq'), primary_key=True)
    request_id = Column(String(), nullable=False)

    forecast_avg_lift = Column(Float(), nullable=False)
    forecast_avg_train_error = Column(Float(), nullable=False)
    forecast_technique = Column(String(), nullable=False)
    forecast_avg_per_day = Column(Float(), nullable=False)

    forecast_actuals = Column(Float())
    forecast_expected = Column(Float())
    confidence = Column(String())
    abs_uplift = Column(Float())

    _row_created_at = Column(DateTime(timezone=True), nullable=False, default=datetime.datetime.utcnow())
    _row_updated_at = Column(DateTime(timezone=True), nullable=False, default=datetime.datetime.utcnow(),
                             onupdate=datetime.datetime.utcnow())

    # request = relationship('RequestDetails', backref='model_forecast_metrics', uselist=False)

    __table_args__ = (
        ForeignKeyConstraint(['request_id'], ['request_details.request_id']),
        UniqueConstraint('request_id', 'forecast_technique')
    )


class TestControlRecords(Base):
    """
    Stores the test vs control data
    """
    __tablename__ = 'test_control_records'

    id = Column(Integer(), Sequence('id_seq'), primary_key=True)
    request_id = Column(String(), nullable=False)

    comparison_technique = Column(String(), nullable=False)

    experiment_param = Column(String(), nullable=False)
    experiment_date = Column(Date(), nullable=False)
    experiment_period = Column(String(), nullable=False)
    experiment_selection_type = Column(String(), nullable=False)

    param_units = Column(String())
    param_value = Column(Float(), nullable=False)

    _row_created_at = Column(DateTime(timezone=True), nullable=False, default=datetime.datetime.utcnow())
    _row_updated_at = Column(DateTime(timezone=True), nullable=False, default=datetime.datetime.utcnow(),
                             onupdate=datetime.datetime.utcnow())

    __table_args__ = (
        ForeignKeyConstraint(['request_id'], ['request_details.request_id']),
    )


class TestControlMetrics(Base):
    """
    Stores the metrics for test vs control experiments.
    """
    __tablename__ = 'test_control_metrics'

    id = Column(Integer(), Sequence('id_seq'), primary_key=True)
    request_id = Column(String(), nullable=False)

    abs_uplift = Column(Float(), nullable=False)
    abs_lift_per_day = Column(Float(), nullable=False)
    confidence = Column(String(), nullable=False)
    control_pre_vs_post = Column(Float(), nullable=False)
    pct_uplift = Column(Float(), nullable=False)
    t_test_confidence_interval = Column(Float(), nullable=False)
    t_test_p_value = Column(Float(), nullable=False)
    test_pre_vs_post = Column(Float(), nullable=False)

    _row_created_at = Column(DateTime(timezone=True), nullable=False, default=datetime.datetime.utcnow())
    _row_updated_at = Column(DateTime(timezone=True), nullable=False, default=datetime.datetime.utcnow(),
                             onupdate=datetime.datetime.utcnow())

    # request = relationship('RequestDetails', backref='test_control_metrics', uselist=False)

    __table_args__ = (
        ForeignKeyConstraint(['request_id'], ['request_details.request_id']),
        UniqueConstraint('request_id')
    )


class TestControlStatsMetrics(Base):
    """
    Stores the values for additional stats metrics for test control
    """
    __tablename__ = 'test_control_stats_metrics'

    id = Column(Integer(), Sequence('id_seq'), primary_key=True)
    request_id = Column(String(), nullable=False)

    market = Column(String(), nullable=False)
    period = Column(String(), nullable=False)
    metric = Column(String(), nullable=False)
    value = Column(Float(), nullable=False)

    _row_created_at = Column(DateTime(timezone=True), nullable=False, default=datetime.datetime.utcnow())
    _row_updated_at = Column(DateTime(timezone=True), nullable=False, default=datetime.datetime.utcnow(),
                             onupdate=datetime.datetime.utcnow())

    __table_args__ = (
        ForeignKeyConstraint(['request_id'], ['request_details.request_id']),
    )


# ------------
# INPUT TABLES
# ------------

class VisitsInput (Base):
    """
    Stores the visits related data for use in models
    """
    __tablename__ = 'visits_input'

    id = Column(Integer(), Sequence('id_seq'), primary_key=True)

    date = Column(Date(), nullable=False)
    # Leaving these columns as Nullable = True, as there are lots of data rows with 'null' values.
    country = Column(String())
    region = Column(String())
    dma = Column(String())
    city = Column(String())
    last_acquisition_channel = Column(String())
    paid_free = Column(String())
    funnel_position = Column(String())
    visitor_status = Column(String())

    total_visits = Column(Integer(), nullable=False)
    total_orders = Column(Integer(), nullable=False)


class TransactionsInput(Base):
    """
    Stores input data for modelling.
    """
    __tablename__ = 'transactions_input'

    id = Column(Integer(), Sequence('id_seq'), primary_key=True)

    date = Column(Date(), nullable=False)
    # Leaving these columns as Nullable = True, as there are lots of data rows with 'null' values.
    country = Column(String())
    region = Column(String())
    dma = Column(String())
    city = Column(String())
    product_category = Column(String())
    product_subcategory = Column(String())
    material = Column(String())
    style = Column(String())

    customer_status = Column(String())
    visitor_status = Column(String())
    shop = Column(String())
    last_acquisition_channel = Column(String())

    paid_free = Column(String())
    funnel_position = Column(String())

    total_sales = Column(Float(), nullable=False)
    total_orders = Column(Integer(), nullable=False)
    total_new_customer = Column(Integer(), nullable=False)
    total_old_customer = Column(Integer(), nullable=False)
    total_customer = Column(Integer(), nullable=False)


# -------------
# Filter Tables
# -------------


class VisitFilters (Base):
    __tablename__ = 'visit_filters'

    id = Column(Integer(), Sequence('id_seq'), primary_key=True)
    country = Column(String())
    region = Column(String())
    dma = Column(String())
    city = Column(String())
    last_acquisition_channel = Column(String())
    paid_free = Column(String())
    funnel_position = Column(String())
    visitor_status = Column(String())


class TransactionFilters (Base):
    __tablename__ = 'transaction_filters'
    
    id = Column(Integer(), Sequence('id_seq'), primary_key=True)

    country = Column(String())
    region = Column(String()) 
    dma = Column(String()) 
    city = Column(String()) 
    product_category = Column(String()) 
    product_subcategory = Column(String()) 
    material = Column(String()) 
    style = Column(String()) 
    customer_status = Column(String()) 
    visitor_status = Column(String()) 
    shop = Column(String()) 
    last_acquisition_channel= Column(String()) 
    paid_free = Column(String()) 
    funnel_position = Column(String())


if __name__ == '__main__':
    engine = create_engine(URL(
        account=cred.account,
        user=cred.user,
        password=cred.password,
        database=cred.database,
        schema=cred.schema,
        warehouse=cred.warehouse,
        role=cred.role
    ), echo=True)

    Base.metadata.bind = engine
    DBSession = sessionmaker(bind=engine)
    session = DBSession()
    session.execute('ALTER SESSION SET ABORT_DETACHED_QUERY=TRUE')

    Base.metadata.create_all(engine)

