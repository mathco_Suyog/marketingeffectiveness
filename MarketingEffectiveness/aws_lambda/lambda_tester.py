from lambda_handler import lambda_handler

event_test_countries_1 = {
    'httpMethod':'GET',
    'resource': '/getallcountries',
    'queryStringParameters': {'measure_performance':'Sales'}
}
event_test_countries_2 = {
    'httpMethod':'GET',
    'resource': '/getallcountries',
    'queryStringParameters': {'measure_performance':'Visits'}
}

event_test_regions_1 = {
    'httpMethod':'GET',
    'resource': '/getregionslist',
    'queryStringParameters': {'measure_performance':'Sales', 'country': 'UNITED STATES'}
}

event_test_regions_2 = {
    'httpMethod':'GET',
    'resource': '/getregionslist',
    'queryStringParameters': {'measure_performance':'Visits', 'country': 'UNITED STATES'}
}

event_test_dma_1 = {
    'httpMethod':'GET',
    'resource': '/getdmalist',
    'queryStringParameters': {'measure_performance':'Sales', 'country': 'United States'}, 
    'multiValueQueryStringParameters': {'region':["New Jersey","North Dakota"]}
}

event_test_dma_2 = {
    'httpMethod':'GET',
    'resource': '/getdmalist',
    'queryStringParameters': {'measure_performance':'Sales', 'country': 'United States'},
    'multiValueQueryStringParameters': {'region':["All"]}
}

event_test_cities_1 = {
    'httpMethod':'GET',
    'resource': '/getcitieslist',
    'queryStringParameters': {'measure_performance':'Sales', 'country': 'United States'}, 
    'multiValueQueryStringParameters': {'region':["New Jersey","North Dakota"], 'dma':['All']}
}

event_test_cities_2 = {
    'httpMethod':'GET',
    'resource': '/getcitieslist',
    'queryStringParameters': {'measure_performance':'Sales', 'country': 'United States'},
    'multiValueQueryStringParameters': {'region':["All"], "dma":['Bakersfield', 'New York'] }
}

event_test_cities_3 = {
    'httpMethod':'GET',
    'resource': '/getcitieslist',
    'queryStringParameters': {'measure_performance':'Sales', 'country': 'United States'},
    'multiValueQueryStringParameters': {'region':["All"], "dma":['All'] }
}

event_test_cities_4 = {
    'httpMethod':'GET',
    'resource': '/getcitieslist',
    'queryStringParameters': {'measure_performance':'Sales', 'country': 'Australia'}, 
    'multiValueQueryStringParameters': {'region':["All"], 'dma':['All']}
}

event_test_material_1 = {
    'httpMethod':'GET',
    'resource': '/getproductmaterials',
    'queryStringParameters': {'measure_performance':'Sales', 'country': 'United States'},
    'multiValueQueryStringParameters': {'region':["All"], "dma":['Bakersfield', 'New York'], 'city':['All'] }
}

event_test_material_2 = {
    'httpMethod':'GET',
    'resource': '/getproductmaterials',
    'queryStringParameters': {'measure_performance':'Sales', 'country': 'United States'},
    'multiValueQueryStringParameters': { 'region':["All"], "dma":['All'], 'city':['New York'] }
}

event_test_style_1 = {
    'httpMethod':'GET',
    'resource': '/getproductstyle',
    'queryStringParameters': {'measure_performance':'Sales', 'country': 'United States', 'material': 'Tree'},
    'multiValueQueryStringParameters': {'region':["Arizona"], "dma":['Phoenix'], 'city':['Avondale'] }
}

event_test_style_2 = {
    'httpMethod':'GET',
    'resource': '/getproductstyle',
    'queryStringParameters': {'measure_performance':'Sales', 'country': 'Brazil', 'material': 'All'},
    'multiValueQueryStringParameters': {'region':["All"], "dma":['All'], 'city':['All'] }
}

event_test_dma_3 = {
    'httpMethod':'GET',
    'resource': '/getdmalist',
    'queryStringParameters': {'measure_performance':'Sales', 'country': 'INDIA'},
    'multiValueQueryStringParameters': {'region':["All"]}
}

event_test_prd_cats_1 = {
    'httpMethod':'GET',
    'resource': '/getproductcategories',
    'queryStringParameters': {'measure_performance':'Sales'}
}

event_test_prd_sub_cats_1 = {
    'httpMethod':'GET',
    'resource': '/getproductsubcategory',
    'queryStringParameters': {'measure_performance':'Sales', 'product_category': 'Shoes'}
}

event_test_prd_sub_cats_2 = {
    'httpMethod':'GET',
    'resource': '/getproductsubcategory',
    'queryStringParameters': {'measure_performance':'Sales', 'product_category': 'All'}
}

event_aus_getmaterials = {
    'resource': '/getproductmaterials', 
    'httpMethod': 'GET', 
    'queryStringParameters': {'city': 'All', 'country': 'Australia', 'dma': 'All', 'measure_performance': 'Sales', 'region': 'All'},
    'multiValueQueryStringParameters' : {'city': ['All'], 'country': ['Australia'], 'dma': ['All'], 'measure_performance': ['Sales'], 'region': ['All']}
}

event_us_getmaterials = {
    'resource': '/getproductmaterials', 
    'httpMethod': 'GET', 
    'queryStringParameters': {'city': 'All', 'country': 'United States', 'dma': 'All', 'measure_performance': 'Sales', 'region': 'All'},
    'multiValueQueryStringParameters' : {'city': ['All'], 'country': ['United States'], 'dma': ['All'], 'measure_performance': ['Sales'], 'region': ['All']}
}

event_test_prd_materials_2 = {
    'httpMethod':'GET',
    'resource': '/getproductmaterials',
    'queryStringParameters': {'measure_performance':'Sales', 'product_category': 'All'}
}

event_saved_filters = {
    'httpMethod':'GET',
    'resource': '/getsavedfilters',
    'queryStringParameters': {'request_id':'b4e6f7d3-af50-4bcb-952a-61383ea0699f'}
}

event_iterations = {
    'httpMethod':'GET',
    'resource': '/getiterationlist',
    'queryStringParameters': {'user_id':'jaspinder.singh@themathcompany.com'}
}

event_iterations_n = {
    'httpMethod':'GET',
    'resource': '/getiterationlist',
    'queryStringParameters': {'user_id':'jaspinder.singh@themathcompany.com', 'n':'6'}
}

event_request_status = {
    'httpMethod':'GET',
    'resource': '/requeststatus',
    'queryStringParameters': {'request_id':'c86a9e22-0914-4192-a7e6-cee063001afa'}
}

event_model_technique = {
    'httpMethod':'GET',
    'resource': '/getmodelruntechniques',
    'queryStringParameters': {'request_id':'1886a213-TEST-TEST-TEST-21c66fde3a1c', 'comparison_technique': 'Actual vs. Expected'}
}

event_model_technique_2 = {
    'httpMethod':'GET',
    'resource': '/getmodelruntechniques',
    'queryStringParameters': {'request_id':'1886a213-TEST-TEST-TEST-21c66fde3a1c', 'comparison_technique': 'SMDY'}
}

event_looker_test = {
    'httpMethod':'GET',
    'resource': '/getlookervizuri',
    'queryStringParameters': {'request_id': '1886a213-TEST-TEST-TEST-21c66fde3a1c', 'model_name': 'UCM'}
}

event_channel_details = {
    'httpMethod':'GET',
    'resource': '/getchanneldetails',
    'queryStringParameters': {'measure_performance': 'Sales'}
}

event_channel_details_2 = {
    'httpMethod':'GET',
    'resource': '/getchanneldetails',
    'queryStringParameters': {'measure_performance': 'Visits'}
}

if __name__ == '__main__':
    # pass
    # print(lambda_handler(event=event_model_technique, context=None), '\n', '-'*80, '\n')
    # print(lambda_handler(event=event_model_technique_2, context=None), '\n', '-'*80, '\n')
    # print(lambda_handler(event=event_looker_test, context=None), '\n', '-'*80, '\n')
    # print(lambda_handler(event=event_test_countries_1, context=None), '\n', '-'*80, '\n')
    # print(lambda_handler(event=event_test_regions_1, context=None), '\n', '-'*80, '\n')
    # print(lambda_handler(event=event_test_countries_2, context=None), '\n', '-'*80, '\n')
    # print(lambda_handler(event=event_test_regions_2, context=None), '\n', '-'*80, '\n')
    # print(lambda_handler(event=event_test_dma_1, context=None), '\n', '-'*80, '\n')
    # print(lambda_handler(event=event_test_dma_2, context=None), '\n', '-'*80, '\n')
    # print(lambda_handler(event=event_test_cities_1, context=None), '\n', '-'*80, '\n')
    # print(lambda_handler(event=event_test_cities_2, context=None), '\n', '-'*80, '\n')
    # print(lambda_handler(event=event_test_cities_3, context=None), '\n', '-'*80, '\n')
    # print(lambda_handler(event=event_test_cities_4, context=None), '\n', '-'*80, '\n')
    # print(lambda_handler(event=event_test_material_1, context=None), '\n', '-'*80, '\n')
    # print(lambda_handler(event=event_test_material_2, context=None), '\n', '-'*80, '\n')
    # print(lambda_handler(event=event_test_style_1, context=None), '\n', '-'*80, '\n')
    # print(lambda_handler(event=event_test_style_2, context=None), '\n', '-'*80, '\n')
    # print(lambda_handler(event=event_test_dma_3, context=None), '\n', '-'*80, '\n')
    # print(lambda_handler(event=event_test_prd_cats_1, context=None), '\n', '-'*80, '\n')
    # print(lambda_handler(event=event_test_prd_sub_cats_1, context=None), '\n', '-'*80, '\n')
    # print(lambda_handler(event=event_test_prd_sub_cats_2, context=None), '\n', '-'*80, '\n')
    # print(lambda_handler(event=event_us_getmaterials, context=None), '\n', '-'*80, '\n')
    # print(lambda_handler(event=event_aus_getmaterials, context=None), '\n', '-'*80, '\n')
    # print(lambda_handler(event=event_saved_filters, context=None), '\n', '-'*80, '\n')
    print(lambda_handler(event=event_iterations, context=None), '\n', '-'*80, '\n')
    print(lambda_handler(event=event_iterations_n, context=None), '\n', '-'*80, '\n')
    print(lambda_handler(event=event_request_status, context=None), '\n', '-'*80, '\n')
    # print(lambda_handler(event=event_channel_details, context=None), '\n', '-'*80, '\n')
    # print(lambda_handler(event=event_channel_details_2, context=None), '\n', '-'*80, '\n')
