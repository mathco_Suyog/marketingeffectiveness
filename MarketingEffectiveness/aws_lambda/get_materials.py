import json

from fc_schema import VisitFilters, TransactionFilters
from sqlalchemy import and_, or_

def get_materials(event, context, session):
    event_measure = event["queryStringParameters"]["measure_performance"]
    if event_measure.lower() == 'visits':
        query_table = VisitFilters
    else:
        query_table = TransactionFilters

    event_country = event["queryStringParameters"]["country"]
    event_regions = event["multiValueQueryStringParameters"]["region"]
    event_dmas = event["multiValueQueryStringParameters"]["dma"]
    event_cities = event["multiValueQueryStringParameters"]["city"]

    if (len(event_regions) == 1) and ('All' in event_regions):
        event_regions_query = or_(
            query_table.region != None,
            query_table.region == None
            )
    else:
        event_regions_query = (query_table.region.in_(event_regions))

    if (len(event_dmas) == 1) and ('All' in event_dmas):
        event_dmas_query = or_(
            query_table.dma == None,
            query_table.dma != None
        )
    else:
        event_dmas_query = (query_table.dma.in_(event_dmas))

    if (len(event_cities) == 1) and ('All' in event_cities):
        event_cities_query = or_(
            query_table.city == None,
            query_table.city != None
        )
    else:
        event_cities_query = (query_table.city.in_(event_cities))

    qry = (
        session
        .query(query_table.material)
        .filter(and_(
            query_table.country == event_country,
            event_regions_query,
            event_dmas_query,
            event_cities_query
            ))
        .distinct().order_by(query_table.material).all())

    out_list = [ list(x) for x in qry if x[0] is not None ]
    # print(len(out_list))
    out_list.sort()
    out_list.insert(0, ['All'])

    out_list = json.dumps(out_list, separators=(',',':'))

    response =  {
        'statusCode':200,
        'body': out_list,
        'headers':{},
        'isBase64Encoded':False
        }

    return response
