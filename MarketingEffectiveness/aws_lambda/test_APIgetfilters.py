import pytest
import fc_schema 
import cred
from get_countries import get_countries
from get_regions import get_regions
from get_dma_list import get_dma_list
from get_paid_free import get_paid_free
from get_funnel_position import get_funnel_position
from get_last_acquisition_channel import get_last_acquisition_channel
from get_product_categories import get_product_categories
from get_product_categories import get_product_sub_categories
from get_materials import get_materials
from get_cities import get_cities
from get_style import get_style
from get_looker_viz_uri import get_looker_viz_uri
from get_looker_viz_uri import get_model_run_techniques
from get_saved_filters import get_saved_filters
from get_request_info import get_request_status
import pandas as pd
from pandas import DataFrame
import json
import csv

@pytest.mark.parametrize("event",[({"queryStringParameters":{"measure_performance":"Sales", "country":"United States"}})])
def test_get_countries_sales(event, session):
    
	result = pd.read_csv(r'data/Sales_countries_DB.csv')
	js = result.to_json(orient="records")
	json.loads(js)
	
	countries = get_countries(event,None,session)
	result2 = countries["body"]
		
	result2 = result2.replace("[" , "")
	result2 = result2.replace("]" , "")
	result2 = result2.replace('"' , "")

	list_ = result2.split(',')
	
	assert len(result) == len(list_)

@pytest.mark.parametrize("event",[({"queryStringParameters":{"measure_performance":"Visits"}})])
def test_get_countries_visits(event, session):

	result = pd.read_csv(r'data/Visit_countries_DB.csv')
	js = result.to_json(orient="records")
	json.loads(js)
	
	countries = get_countries(event,None,session)
	result2 = countries["body"]
		
	result2 = result2.replace("[" , "")
	result2 = result2.replace("]" , "")
	result2 = result2.replace('"' , "")

	list_ = result2.split(',')

	assert len(result) == len(list_)

@pytest.mark.parametrize("event",[({"queryStringParameters":{"measure_performance":"Sales", "country":"United States"}})])
def test_get_regions_sales(event, session):

	result = pd.read_csv(r'data/Sales_Region_DB.csv')
	js = result.to_json(orient="records")
	json.loads(js)

	regions = get_regions(event,None,session)

	result2 = regions["body"]
		
	result2 = result2.replace("[" , "")
	result2 = result2.replace("]" , "")
	result2 = result2.replace('"' , "")

	list_ = result2.split(',')
	
	assert len(result) == len(list_)

@pytest.mark.parametrize("event",[({"queryStringParameters":{"measure_performance":"Visits", "country":"UNITED STATES"}})])
def test_get_regions_visits(event, session):

	result = pd.read_csv(r'data/Visits_Region_DB.csv')
	js = result.to_json(orient="records")
	json.loads(js)

	regions = get_regions(event,None,session)

	result2 = regions["body"]
		
	result2 = result2.replace("[" , "")
	result2 = result2.replace("]" , "")
	result2 = result2.replace('"' , "")

	list_ = result2.split(',')
	
	assert len(result) == len(list_)

@pytest.mark.parametrize("event",[({"queryStringParameters":{"measure_performance":"Sales"}})])
def test_get_product_categories(event, session):

	result = pd.read_csv(r'data/Test_Product_Category.csv')
	js = result.to_json(orient="records")
	json.loads(js)
	
	categories = get_product_categories(event,None,session)
	result2 = categories["body"]
		
	result2 = result2.replace("[" , "")
	result2 = result2.replace("]" , "")
	result2 = result2.replace('"' , "")

	list_ = result2.split(',')
	
	assert len(result) == len(list_)

@pytest.mark.parametrize("event",[({"queryStringParameters":{"measure_performance":"Sales", "country":"United States"},"multiValueQueryStringParameters":{"region":["New Jersey","North Dakota"]}})])
def test_get_dma_list(event, session):

	result = pd.read_csv(r'data/Test_DMA_NewJersey.csv')
	js = result.to_json(orient="records")
	json.loads(js)
	
	dma = get_dma_list(event,None,session)
	result2 = dma["body"]
		
	result2 = result2.replace("[" , "")
	result2 = result2.replace("]" , "")
	result2 = result2.replace('"' , "")

	list_ = result2.split(',')
	
	assert len(result) == len(list_)

@pytest.mark.parametrize("event",[({"queryStringParameters":{"measure_performance":"Sales"}})])
def test_get_paid_free(event, session):

	result = pd.read_csv(r'data/Paid_Free.csv')
	js = result.to_json(orient="records")
	json.loads(js)

	paid_free = get_paid_free(event,None,session)

	result2 = paid_free["body"]
		
	result2 = result2.replace("[" , "")
	result2 = result2.replace("]" , "")
	result2 = result2.replace('"' , "")

	list_ = result2.split(',')
	
	assert len(result) == len(list_)

@pytest.mark.parametrize("event",[({"queryStringParameters":{"measure_performance":"Sales", "paid_free":"Free"}})])
def test_get_funnel_position_free(event, session):

	result = pd.read_csv(r'data/Funnel_Position_Free.csv')
	js = result.to_json(orient="records")
	json.loads(js)
	

	funnel_position = get_funnel_position(event,None,session)

	result2 = funnel_position["body"]
		
	result2 = result2.replace("[" , "")
	result2 = result2.replace("]" , "")
	result2 = result2.replace('"' , "")

	list_ = result2.split(',')
	
	
	assert len(result) == len(list_)

@pytest.mark.parametrize("event",[({"queryStringParameters":{"measure_performance":"Sales", "paid_free":"Paid"}})])
def test_get_funnel_position_paid(event, session):

	result = pd.read_csv(r'data/Funnel_Position_Paid.csv')
	js = result.to_json(orient="records")
	json.loads(js)
	

	funnel_position1 = get_funnel_position(event,None,session)

	result2 = funnel_position1["body"]
		
	result2 = result2.replace("[" , "")
	result2 = result2.replace("]" , "")
	result2 = result2.replace('"' , "")

	list_ = result2.split(',')
	
	
	assert len(result) == len(list_)

@pytest.mark.parametrize("event",[({"queryStringParameters":{"measure_performance":"Sales"},"multiValueQueryStringParameters":{"paid_free":"Paid","funnel_position":"Bottom"}})])
def test_get_last_acq_channel_Bottom(event, session):

	result = pd.read_csv(r'data/LastAcquisitionChannel_Bottom.csv')
	js = result.to_json(orient="records")
	json.loads(js)

	last_acqu_channel = get_last_acquisition_channel(event,None,session)

	result2 = last_acqu_channel["body"]
		
	result2 = result2.replace("[" , "")
	result2 = result2.replace("]" , "")
	result2 = result2.replace('"' , "")

	list_ = result2.split(',')
	
	assert len(result) == len(list_)

@pytest.mark.parametrize("event",[({"multiValueQueryStringParameters": {"funnel_position": "Bottom", "paid_free": "Free"}, "queryStringParameters": {"measure_performance": "Sales"}})])
def test_get_last_acq_channel1(event, session):

	result = pd.read_csv(r'data/Last_Acq_Channels.csv')
	js = result.to_json(orient="records")
	json.loads(js)

	last_acqu_channel1 = get_last_acquisition_channel(event,None,session)

	result2 = last_acqu_channel1["body"]
		
	result2 = result2.replace("[" , "")
	result2 = result2.replace("]" , "")
	result2 = result2.replace('"' , "")

	list_ = result2.split(',')
	
	assert len(result) == len(list_)

@pytest.mark.parametrize("event",[({"queryStringParameters":{"measure_performance":"Sales","product_category":"Shoes"}})])
def test_get_product_subcategories(event, session):

	result = pd.read_csv(r'data/Prod_SubCat_Shoes.csv')
	js = result.to_json(orient="records")
	json.loads(js)

	prod_subcat = get_product_sub_categories(event,None,session)

	result2 = prod_subcat["body"]
		
	result2 = result2.replace("[" , "")
	result2 = result2.replace("]" , "")
	result2 = result2.replace('"' , "")

	list_ = result2.split(',')
	
	assert len(result) == len(list_)

@pytest.mark.parametrize("event",[({"queryStringParameters":{"measure_performance":"Sales","product_category":"Accessories"}})])
def test_get_product_subcategories1(event, session):

	result = pd.read_csv(r'data/Prod_SubCat_Accessories.csv')
	js = result.to_json(orient="records")
	json.loads(js)

	prod_subcat1 = get_product_sub_categories(event,None,session)

	result2 = prod_subcat1["body"]
		
	result2 = result2.replace("[" , "")
	result2 = result2.replace("]" , "")
	result2 = result2.replace('"' , "")

	list_ = result2.split(',')
	
	assert len(result) == len(list_)

@pytest.mark.parametrize("event",[({"queryStringParameters":{"measure_performance":"Sales","country":"United States"},"multiValueQueryStringParameters":{"region":"All","dma":"All"}})])
def test_get_cities(event, session):

	result = pd.read_csv(r'data/Test_City.csv')
	js = result.to_json(orient="records")
	json.loads(js)
	
	cities = get_cities(event,None,session)

	result2 = cities["body"]
		
	result2 = result2.replace("[" , "")
	result2 = result2.replace("]" , "")
	result2 = result2.replace('"' , "")

	list_ = result2.split(',')
	
	assert len(result) == len(list_)

@pytest.mark.parametrize("event",[({"queryStringParameters":{"measure_performance":"Sales","country":"Australia"},"multiValueQueryStringParameters":{"region":"All","dma":"All"}})])
def test_get_cities1(event, session):

	result = pd.read_csv(r'data/Test_City.csv')
	js = result.to_json(orient="records")
	json.loads(js)

	cities1 = get_cities(event,None,session)

	result2 = cities1["body"]
		
	result2 = result2.replace("[" , "")
	result2 = result2.replace("]" , "")
	result2 = result2.replace('"' , "")

	list_ = result2.split(',')
	
	assert len(result) == len(list_)

@pytest.mark.parametrize("event",[({"queryStringParameters":{"measure_performance":"Sales","country":"United States"},"multiValueQueryStringParameters":{"region":"All","dma":"All","city":"All"}})])
def test_get_materials(event, session):

	result = pd.read_csv(r'data/Product_Material.csv')
	js = result.to_json(orient="records")
	json.loads(js)

	prod_material = get_materials(event,None,session)

	result2 = prod_material["body"]
		
	result2 = result2.replace("[" , "")
	result2 = result2.replace("]" , "")
	result2 = result2.replace('"' , "")

	list_ = result2.split(',')
	
	assert len(result) == len(list_)

@pytest.mark.parametrize("event",[({"queryStringParameters":{"measure_performance":"Sales","country":"United States","material":"Tree"},"multiValueQueryStringParameters":{"region":"All","dma":"All","city":"All"}})])
def test_get_prod_style(event, session):

	result = pd.read_csv(r'data/Product_Style.csv')
	js = result.to_json(orient="records")
	json.loads(js)

	styles = get_style(event,None,session)

	result2 = styles["body"]
		
	result2 = result2.replace("[" , "")
	result2 = result2.replace("]" , "")
	result2 = result2.replace('"' , "")

	list_ = result2.split(',')
	
	assert len(result) == len(list_)

@pytest.mark.parametrize("event",[({"queryStringParameters":{"request_id":"1966b955-a165-478b-86de-e6438ecf5d25","model_name":"UCM","comparison_technique":"Actual vs. Expected"}})])
def test_get_looker_viz_uri(event, session):

	result = pd.read_csv(r'data/Looker_Viz.csv')
	js = result.to_json(orient="records")
	json.loads(js)

	looker_viz = get_looker_viz_uri(event,None,session)

	result2 = looker_viz["body"]
		
	result2 = result2.replace("[" , "")
	result2 = result2.replace("]" , "")
	result2 = result2.replace('"' , "")

	list_ = result2.split(',')
	
	assert len(result) == len(list_)

@pytest.mark.parametrize("event",[({"queryStringParameters":{"request_id":"caec954c-69e9-43ea-a82d-40485c03be16","comparison_technique":"SMLY"}})])
def test_get_model_run_techniques_SMLY(event, session):

	result = pd.read_csv(r'data/Model_Run.csv')
	js = result.to_json(orient="records")
	json.loads(js)

	modelrun = get_model_run_techniques(event,None,session)

	result2 = modelrun["body"]
		
	result2 = result2.replace("[" , "")
	result2 = result2.replace("]" , "")
	result2 = result2.replace('"' , "")

	list_ = result2.split(',')
	
	assert len(result) == len(list_)

@pytest.mark.parametrize("event",[({"queryStringParameters":{"request_id":"b48c9ebc-c5ff-40c4-9c6f-638129d0783c","comparison_technique":"Actual vs. Expected"}})])
def test_get_model_run_techniques(event, session):

	result = pd.read_csv(r'data/Model_Run_Actual vs Expected.csv')
	js = result.to_json(orient="records")
	json.loads(js)

	modelrun = get_model_run_techniques(event,None,session)

	result2 = modelrun["body"]

		
	result2 = result2.replace("[" , "")
	result2 = result2.replace("]" , "")
	result2 = result2.replace('"' , "")

	list_ = result2.split(',')
	
	assert len(result) == len(list_)

@pytest.mark.parametrize("event",[({"queryStringParameters":{"request_id":"1966b955-a165-478b-86de-e6438ecf5d25"}})])
def test_get_saved_filters(event, session):

	result = pd.read_csv(r'data/Saved_Filters.csv')
	js = result.to_json(orient="records")
	json.loads(js)

	saved_filter = get_saved_filters(event,None,session)
	#print(saved_filter["body"])

	result2 = saved_filter["body"]
		
	result2 = result2.replace("[" , "")
	result2 = result2.replace("]" , "")
	result2 = result2.replace('"' , "")

	list_ = result2.split(',')
	
	assert len(result) == len(list_)

@pytest.mark.parametrize("event",[({"queryStringParameters":{"request_id":"1966b955-a165-478b-86de-e6438ecf5d25"}})])
def test_get_request_status(event, session):

	result = pd.read_csv(r'data/Request_Status.csv')
	js = result.to_json(orient="records")
	json.loads(js)

	req_status = get_request_status(event,None,session)

	result2 = req_status["body"]
		
	result2 = result2.replace("{" , "")
	result2 = result2.replace("}" , "")
	result2 = result2.replace('"' , "")

	list_ = result2.split(',')
	
	assert len(result) == len(list_)