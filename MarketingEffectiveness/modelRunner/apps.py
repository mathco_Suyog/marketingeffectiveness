from django.apps import AppConfig


class ModelrunnerConfig(AppConfig):
    name = 'modelRunner'
