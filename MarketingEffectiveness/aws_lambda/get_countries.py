import json
from fc_schema import VisitFilters, TransactionFilters

def get_countries(event, context, session):
    event_measure = event["queryStringParameters"]["measure_performance"]
    if event_measure.lower() == 'visits':
        query_table = VisitFilters
    else:
        query_table = TransactionFilters
    
    qry = session.query(query_table.country).filter(query_table.country != None).distinct().order_by(query_table.country).all()


    out_list = [ list(x) for x in qry if x[0] is not None]
    out_list.sort()
    # out_list.insert(0, ['All'])

    # print(out_list)

    out_list = json.dumps(out_list, separators=(',',':'))

    response =  {
        'statusCode':200,
        'body': out_list,
        'headers':{},
        'isBase64Encoded':False
        }

    return response
