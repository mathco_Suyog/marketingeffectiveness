import json

from fc_schema import RequestFilters
# from sqlalchemy import and_
# from pprint import pprint


def get_saved_filters(event, context, session):

    event_request_id = event["queryStringParameters"]["request_id"]

    qry = (
        session
        .query(
            RequestFilters.performance_metric, RequestFilters.business_channel, RequestFilters.visitor_type, RequestFilters.customer_type,
            RequestFilters.comparison_technique, RequestFilters.product_category, RequestFilters.product_sub_category,
            RequestFilters.acquisition_channel_type, RequestFilters.acquisition_channel_funnel, RequestFilters.acquisition_channel_paid_free,
            RequestFilters.test_start_date, RequestFilters.test_end_date, RequestFilters.test_product_material, RequestFilters.test_product_style,
            RequestFilters.test_geo_country, RequestFilters.test_geo_region, RequestFilters.test_geo_dma, RequestFilters.test_geo_city,
            RequestFilters.control_product_material, RequestFilters.control_product_style, RequestFilters.control_geo_country,
            RequestFilters.control_geo_region, RequestFilters.control_geo_dma, RequestFilters.control_geo_city, RequestFilters.control_end_date,
            RequestFilters.control_start_date, RequestFilters.request_data
        )
        .filter(RequestFilters.request_id == event_request_id)
        .order_by(RequestFilters._row_created_at.desc())
        .limit(1)
    )

    result = qry.one()
    result_dict = {k: v for k, v in zip(result.keys(), result)}

    if result_dict['comparison_technique'] == 'Actual vs. Expected':
        result_dict['test_start_date'] = result_dict['test_start_date'].isoformat()
        result_dict['test_end_date'] = result_dict['test_end_date'].isoformat()
    else:
        result_dict['test_start_date'] = result_dict['test_start_date'].isoformat()
        result_dict['control_start_date'] = result_dict['control_start_date'].isoformat()
        result_dict['test_end_date'] = result_dict['test_end_date'].isoformat()
        result_dict['control_end_date'] = result_dict['control_end_date'].isoformat()

    # pprint(result_dict)

    response = {
        'statusCode': 200,
        'body': json.dumps({
            'request_id': '{}'.format(event_request_id),
            'request_filters': result_dict
            }, separators=(',', ':')),
        'headers': {},
        'isBase64Encoded': False
        }

    return response
