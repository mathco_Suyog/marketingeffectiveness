from sqlalchemy import and_, or_, case, literal_column
from dateutil.relativedelta import relativedelta
import pandas as pd
from ..models.fc_schema import VisitsInput


def actual_expected_data_fetcher_visits (filters_row, session):
    # Visitor Type ( All but no multiselect )
    if filters_row.visitor_type == 'All':
        filter_visitor_types_query = or_(VisitsInput.visitor_status.is_(None),
                                         VisitsInput.visitor_status.isnot(None))
    else:
        filter_visitor_types = [x.strip() for x in filters_row.visitor_type.split(',')]
        filter_visitor_types_query = VisitsInput.visitor_status.in_(filter_visitor_types)

    # Test Regions
    filter_test_regions_query = get_regions_from_filter_text(filters_row.test_geo_region,
                                                             filters_row.test_geo_country)

    # Control Regions
    filter_control_regions_query = get_regions_from_filter_text(filters_row.control_geo_region,
                                                                filters_row.control_geo_country)

    # Test DMAs
    filter_test_dmas_query = get_dmas_from_filter_text(filters_row.test_geo_dma, filter_test_regions_query)

    # Control DMAs
    filter_control_dmas_query = get_dmas_from_filter_text(filters_row.control_geo_dma, filter_control_regions_query)

    # Test cities
    filter_test_cities_query = get_cities_from_filter_text(filters_row.test_geo_city, filter_test_dmas_query)

    # Control cities
    filter_control_cities_query = get_cities_from_filter_text(filters_row.control_geo_city,
                                                              filter_control_dmas_query)
    # Channel
    if filters_row.acquisition_channel_type == 'All':
        filter_test_channels_query = or_(VisitsInput.last_acquisition_channel.isnot(None),
                                         VisitsInput.last_acquisition_channel.is_(None))
    else:
        filter_test_channels = [x.strip() for x in filters_row.acquisition_channel_type.split(',')]
        filter_test_channels_query = VisitsInput.last_acquisition_channel.in_(filter_test_channels)

    # paid/free
    if filters_row.acquisition_channel_paid_free == 'All':
        filter_test_channels_paid_free_query = or_(VisitsInput.paid_free.is_(None),
                                                   VisitsInput.paid_free.isnot(None))
    else:
        filter_test_channels_paid_free = [x.strip() for x in filters_row.acquisition_channel_paid_free.split(',')]
        filter_test_channels_paid_free_query = VisitsInput.paid_free.in_(filter_test_channels_paid_free)

    # funnel
    if filters_row.acquisition_channel_funnel == 'All':
        filter_test_funnels_query = or_(VisitsInput.funnel_position.is_(None),
                                        VisitsInput.funnel_position.isnot(None))
    else:
        filter_test_funnels = [x.strip() for x in filters_row.acquisition_channel_funnel.split(',')]
        filter_test_funnels_query = VisitsInput.funnel_position.in_(filter_test_funnels)

    qry = (
        session.query(VisitsInput)
        .filter(and_(
            filter_visitor_types_query,
                        
            VisitsInput.date >= str(filters_row.test_start_date -
                                    relativedelta(year=filters_row.test_start_date.year-1)),  # post start
            VisitsInput.date <= str(filters_row.test_end_date),                                 # post end
          
            filter_test_cities_query,
                 
            filter_test_channels_query,
            filter_test_channels_paid_free_query,
            filter_test_funnels_query
            )
            )
        )

    df = pd.read_sql(qry.statement, qry.session.bind)

    # print(qry.statement.compile(qry.session.bind, compile_kwargs={"literal_binds": True}))

    return df


# #----------------------------------------------LAST_YEAR_SAME_MARKET--------------------------------------------# #
def last_year_same_market_data_fetcher_visits(filters_row, session):
    # Visitor Type ( All but no multiselect )
    if filters_row.visitor_type == 'All':
        filter_visitor_types_query = or_(VisitsInput.visitor_status.is_(None),
                                         VisitsInput.visitor_status.isnot(None))
    else:
        filter_visitor_types = [x.strip() for x in filters_row.visitor_type.split(',')]
        filter_visitor_types_query = VisitsInput.visitor_status.in_(filter_visitor_types)

    # Test Regions
    filter_test_regions_query = get_regions_from_filter_text(filters_row.test_geo_region,
                                                             filters_row.test_geo_country)

    # Control Regions
    filter_control_regions_query = get_regions_from_filter_text(filters_row.control_geo_region,
                                                                filters_row.control_geo_country)

    # Test DMAs
    filter_test_dmas_query = get_dmas_from_filter_text(filters_row.test_geo_dma, filter_test_regions_query)

    # Control DMAs
    filter_control_dmas_query = get_dmas_from_filter_text(filters_row.control_geo_dma, filter_control_regions_query)

    # Test cities
    filter_test_cities_query = get_cities_from_filter_text(filters_row.test_geo_city, filter_test_dmas_query)

    # Control cities
    filter_control_cities_query = get_cities_from_filter_text(filters_row.control_geo_city,
                                                              filter_control_dmas_query)
    # Channel
    if filters_row.acquisition_channel_type == 'All':
        filter_test_channels_query = or_(VisitsInput.last_acquisition_channel.isnot(None),
                                         VisitsInput.last_acquisition_channel.is_(None))
    else:
        filter_test_channels = [x.strip() for x in filters_row.acquisition_channel_type.split(',')]
        filter_test_channels_query = VisitsInput.last_acquisition_channel.in_(filter_test_channels)

    # paid/free
    if filters_row.acquisition_channel_paid_free == 'All':
        filter_test_channels_paid_free_query = or_(VisitsInput.paid_free.is_(None),
                                                   VisitsInput.paid_free.isnot(None))
    else:
        filter_test_channels_paid_free = [x.strip() for x in filters_row.acquisition_channel_paid_free.split(',')]
        filter_test_channels_paid_free_query = VisitsInput.paid_free.in_(filter_test_channels_paid_free)

    # funnel
    if filters_row.acquisition_channel_funnel == 'All':
        filter_test_funnels_query = or_(VisitsInput.funnel_position.is_(None),
                                        VisitsInput.funnel_position.isnot(None))
    else:
        filter_test_funnels = [x.strip() for x in filters_row.acquisition_channel_funnel.split(',')]
        filter_test_funnels_query = VisitsInput.funnel_position.in_(filter_test_funnels)

    qry = (
        session
        .query(VisitsInput,
               case([
                  (and_(
                      VisitsInput.date >= str(filters_row.test_start_date),
                      VisitsInput.date <= str(filters_row.test_end_date)),
                   literal_column("'Post_ThisYear'")),
                  (and_(VisitsInput.date >= str(filters_row.control_start_date),
                        VisitsInput.date <= str(filters_row.control_end_date)),
                   literal_column("'Pre_ThisYear'")),
                  (and_(VisitsInput.date >= str(filters_row.test_start_date -
                                                relativedelta(year=filters_row.test_start_date.year-1)),
                        VisitsInput.date <= str(filters_row.test_end_date -
                                                relativedelta(year=filters_row.test_end_date.year-1))),
                   literal_column("'Post_LastYear'")),
                  (and_(VisitsInput.date >= str(filters_row.control_start_date -
                                                relativedelta(year=filters_row.control_start_date.year-1)),
                        VisitsInput.date <= str(filters_row.control_end_date -
                                                relativedelta(year=filters_row.control_end_date.year-1))),
                   literal_column("'Pre_LastYear'"))], else_=literal_column("'ignore'")).label("'flag'"))
        .filter(
            and_(
                filter_visitor_types_query,
                or_(

                    or_(
                        and_(
                            VisitsInput.date >= str(filters_row.test_start_date),     # post start This Year
                            VisitsInput.date <= str(filters_row.test_end_date)        # post end This Year
                        ),
                        and_(
                            VisitsInput.date >= str(filters_row.control_start_date),  # pre start This Year
                            VisitsInput.date <= str(filters_row.control_end_date)     # pre end This Year
                        )
                    ),
                    or_(
                        and_(
                            VisitsInput.date >= str(filters_row.test_start_date -
                                                    relativedelta(year=filters_row.test_start_date.year-1)),  # post start
                            VisitsInput.date <= str(filters_row.test_end_date -
                                                    relativedelta(year=filters_row.test_end_date.year-1))  # post end
                           ),
                        and_(
                            VisitsInput.date >= str(filters_row.control_start_date -
                                                    relativedelta(year=filters_row.control_start_date.year-1)),  # pre start
                            VisitsInput.date <= str(filters_row.control_end_date -
                                                    relativedelta(year=filters_row.control_end_date.year-1))     # pre end
                           )
                       )
                ),

                filter_test_cities_query,
                filter_test_channels_query,
                filter_test_channels_paid_free_query,
                filter_test_funnels_query
            )
        )
    )

    df = pd.read_sql(qry.statement, qry.session.bind)
    # print(qry.statement.compile(qry.session.bind, compile_kwargs={"literal_binds": True}))

    return df

# #----------------------------------------------SAME_YEAR_DIFF_MARKET----------------------------------# #


def sydm_visits (filters_row, session):
    # Visitor Type ( All but no multiselect )
    if filters_row.visitor_type == 'All':
        filter_visitor_types_query = or_(VisitsInput.visitor_status.is_(None),
                                         VisitsInput.visitor_status.isnot(None))
    else:
        filter_visitor_types = [x.strip() for x in filters_row.visitor_type.split(',')]
        filter_visitor_types_query = VisitsInput.visitor_status.in_(filter_visitor_types)

    # Test Regions
    filter_test_regions_query = get_regions_from_filter_text(filters_row.test_geo_region,
                                                             filters_row.test_geo_country)

    # Control Regions
    filter_control_regions_query = get_regions_from_filter_text(filters_row.control_geo_region,
                                                                filters_row.control_geo_country)

    # Test DMAs
    filter_test_dmas_query = get_dmas_from_filter_text(filters_row.test_geo_dma, filter_test_regions_query)

    # Control DMAs
    filter_control_dmas_query = get_dmas_from_filter_text(filters_row.control_geo_dma, filter_control_regions_query)

    # Test cities
    filter_test_cities_query = get_cities_from_filter_text(filters_row.test_geo_city, filter_test_dmas_query)

    # Control cities
    filter_control_cities_query = get_cities_from_filter_text(filters_row.control_geo_city,
                                                              filter_control_dmas_query)
    # Channel
    if filters_row.acquisition_channel_type == 'All':
        filter_test_channels_query = or_(VisitsInput.last_acquisition_channel.isnot(None),
                                         VisitsInput.last_acquisition_channel.is_(None))
    else:
        filter_test_channels = [x.strip() for x in filters_row.acquisition_channel_type.split(',')]
        filter_test_channels_query = VisitsInput.last_acquisition_channel.in_(filter_test_channels)

    # paid/free
    if filters_row.acquisition_channel_paid_free == 'All':
        filter_test_channels_paid_free_query = or_(VisitsInput.paid_free.is_(None),
                                                   VisitsInput.paid_free.isnot(None))
    else:
        filter_test_channels_paid_free = [x.strip() for x in filters_row.acquisition_channel_paid_free.split(',')]
        filter_test_channels_paid_free_query = VisitsInput.paid_free.in_(filter_test_channels_paid_free)

    # funnel
    if filters_row.acquisition_channel_funnel == 'All':
        filter_test_funnels_query = or_(VisitsInput.funnel_position.is_(None),
                                        VisitsInput.funnel_position.isnot(None))
    else:
        filter_test_funnels = [x.strip() for x in filters_row.acquisition_channel_funnel.split(',')]
        filter_test_funnels_query = VisitsInput.funnel_position.in_(filter_test_funnels)

    qry = (
        session.query(VisitsInput,
                      case([
                          (and_(
                              VisitsInput.date >= str(filters_row.test_start_date),
                              VisitsInput.date <= str(filters_row.test_end_date),
                              filter_test_cities_query
                          ), literal_column("'Post_Test'")),
                          (and_(
                              VisitsInput.date >= str(filters_row.control_start_date),
                              VisitsInput.date <= str(filters_row.control_end_date),
                              filter_test_cities_query
                          ), literal_column("'Pre_Test'")),
                          (and_(
                              VisitsInput.date >= str(filters_row.test_start_date),
                              VisitsInput.date <= str(filters_row.test_end_date),
                              filter_control_cities_query
                          ), literal_column("'Post_Control'")),
                          (and_(
                              VisitsInput.date >= str(filters_row.control_start_date),
                              VisitsInput.date <= str(filters_row.control_end_date),
                              filter_control_cities_query
                          ), literal_column("'Pre_Control'"))],
                          else_=literal_column("'ignore'")).label("'flag'"))
        .filter(
            and_(
                filter_visitor_types_query,
                or_(
                    and_(
                        VisitsInput.date >= str(filters_row.test_start_date),     # post start
                        VisitsInput.date <= str(filters_row.test_end_date)        # post end
                    ),
                    and_(
                        VisitsInput.date >= str(filters_row.control_start_date),  # pre start
                        VisitsInput.date <= str(filters_row.control_end_date)     # pre end
                    )
                ),
                or_(
                    filter_test_cities_query,
                    filter_control_cities_query
                ),
                filter_test_channels_query,
                filter_test_channels_paid_free_query,
                filter_test_funnels_query
            )
        )
    )

    df = pd.read_sql(qry.statement, qry.session.bind)

    # print(qry.statement.compile(qry.session.bind, compile_kwargs={"literal_binds": True}))

    return df

# ---------------------------------------------------------END OF SYDM-----------------------------------------#


def get_regions_from_filter_text(regions_filter_text, country_filter_text):
    if regions_filter_text == 'All':
        filter_regions_query = (VisitsInput.country == country_filter_text)
        # or_(TransactionsInput.region == None, TransactionsInput.region != None))
    else:
        filter_test_regions = [x.strip() for x in regions_filter_text.split(',')]
        filter_regions_query = and_(VisitsInput.country == country_filter_text,
                                    VisitsInput.region.in_(filter_test_regions))
    return filter_regions_query


def get_dmas_from_filter_text(dma_filter_text, region_filter_query):
    if dma_filter_text == 'All':
        filter_dmas_query = region_filter_query
    else:
        filter_test_dmas = [x.strip() for x in dma_filter_text.split(',')]
        filter_dmas_query = and_(VisitsInput.dma.in_(filter_test_dmas),
                                 region_filter_query)
    return filter_dmas_query


def get_cities_from_filter_text(city_filter_text, dma_filter_query):
    if city_filter_text == 'All':
        city_filter_query = dma_filter_query
    else:
        filter_test_cities = [x.strip() for x in city_filter_text.split(',')]
        city_filter_query = and_(VisitsInput.city.in_(filter_test_cities),
                                 dma_filter_query)

    return city_filter_query
