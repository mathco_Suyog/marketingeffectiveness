# -*- coding: utf-8 -*-
"""Looker requests and required functions only. Will augment once reqd."""
import json
import urllib.request
from pprint import pprint

import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning

requests.packages.urllib3.disable_warnings(InsecureRequestWarning)


class LookerLink(object):
    """Base Class with functionalities to perform all required Looker ops."""

    def __init__(self, token, secret, host):
        """Initialize session with required credentials."""
        self.token = token
        self.secret = secret
        self.host = host

        self.session = requests.Session()
        self.session.trust_env = False

        self.auth()

    def auth(self):
        """Read credentials from yml and also authorize session."""
        url = f"{self.host}" + "login"
        params = {"client_id": self.token, "client_secret": self.secret}
        r = self.session.post(url, params=params)
        access_token = r.json().get("access_token")
        # print(access_token)
        head = {"Authorization": f"token {access_token}"}
        self.head = head
        self.session.headers.update(head)

    def get_look(self, look_id, fields=""):
        """Get look_id body."""
        url = f"{self.host}looks/{look_id}"
        # print(url)
        params = {"fields": fields}
        r = self.session.get(url, params=params)
        if r.status_code == requests.codes.ok:
            return r.json()

    def get_query(self, query_id, fields=""):
        """Get query_id."""
        url = f"{self.host}queries/{query_id}"
        # print(url)
        params = {"fields": fields}
        r = self.session.get(url, params=params)
        if r.status_code == requests.codes.ok:
            return r.json()

    def create_query(self, query_body, fields=[]):
        """Post to queries."""
        url = f"{self.host}queries"
        # print(url)
        params = json.dumps(query_body)
        r = self.session.post(
            url, data=params, params=json.dumps({"fields": fields})
        )
        if r.status_code == requests.codes.ok:
            return r.json()

    def run_query(self, query_id, op_format="json"):
        """
        Get to queries/{query_id}/run/{op_format}.

        Out formats can be:
        json	    Plain json
        json_detail	Row data plus metadata
        csv	        Comma separated values with a header
        txt	        Tab separated values with a header
        html	    Simple html
        md	        Simple markdown
        xlsx	    MS Excel spreadsheet
        sql	        Returns the generated SQL rather than running the query
        png	        A PNG image of the visualization of the query
        jpg	        A JPG image of the visualization of the query
        """
        url = f"{self.host}queries/{query_id}/run/{op_format}"
        # print(url)
        params = {}
        r = self.session.get(url, params=params)
        if r.status_code == requests.codes.ok:
            return r.json()

    def run_inline_query(self, body={}, op_format="json"):
        """
        Post query body to queries/run/{op_format} to run directly.

        Out formats can be:
        json	    Plain json
        json_detail	Row data plus metadata
        csv	        Comma separated values with a header
        txt	        Tab separated values with a header
        html	    Simple html
        md	        Simple markdown
        xlsx	    MS Excel spreadsheet
        sql	        Returns the generated SQL rather than running the query
        png	        A PNG image of the visualization of the query
        jpg	        A JPG image of the visualization of the query
        """
        url = f"{self.host}queries/run/{op_format}"
        # print(url)
        params = json.dumps(body)
        r = self.session.post(url, data=params)
        # print(r.status_code)
        if r.status_code == requests.codes.ok:
            if op_format == "json":
                r = r.json()
            return r

    def run_look(self, look_id, op_format="json"):
        """
        Get to looks/{look_id}/run/{format}.

        Out formats can be:
        json	    Plain json
        """
        url = f"{self.host}looks/{look_id}/run/{op_format}"
        # print(url)
        r = self.session.get(url, stream=True)
        # print(res.read())
        if r.status_code == requests.codes.ok:
            return r.json()

    def update_look(self, look_id, body, fields=""):
        """Patch to looks/{look_id}."""
        url = f"{self.host}looks/{look_id}"
        # print(url)
        body = json.dumps(body)
        params = {"fields": fields}
        r = self.session.patch(url, data=body, params=params)
        if r.status_code == requests.codes.ok:
            return r.json()

    def create_look(self, look_body):
        """Post to looks."""
        url = f"{self.host}looks"
        # print(url)
        params = json.dumps(look_body)
        r = self.session.post(url, data=params)
        if r.status_code == requests.codes.ok:
            return r.json()

    def download_look(self, look_id, op_format="csv", filename=""):
        """Get request to looks/{look_id}/run/{op_format} and write to file."""
        url = f"{self.host}looks/{look_id}/run/{op_format}"
        # print(url)
        params = {}
        r = self.session.get(url, params=params, stream=True)
        print(r.status_code)
        if filename == "":
            filename = f"op_{look_id}"
        if r.status_code == requests.codes.ok:
            image_name = f"{filename}.csv"
            with open(image_name, "wb") as f:
                for chunk in r:
                    f.write(chunk)
        else:
            return r.json()

    def close_conn(self):
        """Close the current session."""
        self.session.close()
