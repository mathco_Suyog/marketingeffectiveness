import json
import urllib.parse

from fc_schema import RequestFilters, ModelForecasts, TestControlRecords

def get_looker_viz_uri(event, context, session):
    event_request_id = event["queryStringParameters"]["request_id"]
    event_model_name = event["queryStringParameters"]["model_name"]
    event_comparison_technique = event["queryStringParameters"]["comparison_technique"]

    if event_model_name == 'T TEST':
        if event_comparison_technique == 'Same Market - Last Year':
            event_model_name = 'T TEST SMLY'
        elif  event_comparison_technique == 'Same Year - Different Market':
            event_model_name = 'T TEST SYDM'

    # Map model_name with looker DASHBOARD id 
    # 'MODEL NAME': {looker_dashboard_id}
    model_looker_mapper = {
        'UCM': 130,
        'ARIMA': 133,
        'HOLT WINTERS': 135,
        'T TEST SMLY': 128,
        'T TEST SYDM': 126,
    }
    try:
        base_url = 'https://test.looker.com/embed/dashboards/{}?'.format(model_looker_mapper[event_model_name])
    except KeyError:
        no_model_response = {
            'error_message': 'Model not found in mapping.'
        }

        response =  {
        'statusCode':400,
        'body': json.dumps(no_model_response, separators=(',',':')),
        'headers':{},
        'isBase64Encoded':False
        }

        return response

    url_vars = {'Request ID': event_request_id}

    looker_uri = {}
    looker_uri['looker_uri'] = base_url + urllib.parse.urlencode(url_vars)
    looker_uri['request_id'] = event_request_id
    looker_uri['model_name'] = event_model_name

    response =  {
        'statusCode':200,
        'body': json.dumps(looker_uri, separators=(',',':')),
        'headers':{},
        'isBase64Encoded':False
        }

    return response


def get_model_run_techniques(event, context, session):
    event_request_id = event["queryStringParameters"]["request_id"]
    event_comparison_technique = event["queryStringParameters"]["comparison_technique"]


    if event_comparison_technique == 'Actual vs. Expected':
        technique_list = (
            session
            .query(ModelForecasts.forecast_technique)
            .filter(ModelForecasts.request_id == event_request_id)
            .distinct()
            .order_by(ModelForecasts.forecast_technique)
            .all()
        )
    
        response_dict = {
                'forecast_techniques': [x[0] for x in technique_list if x[0] != None]
            }

    else:
        request_row_count = (
            session
            .query(TestControlRecords.request_id)
            .filter(TestControlRecords.request_id == event_request_id)
            .distinct()
            .count()
        )

        if request_row_count > 0:
            response_dict = {
                'forecast_techniques': ['T TEST']
            }
            
        else:
            response_dict = {
                'forecast_techniques': []
            }

    response =  {
        'statusCode':200,
        'body': json.dumps(response_dict, separators=(',',':')),
        'headers':{},
        'isBase64Encoded':False
        }
    return response




