# incremental-lift

### virtual Env

pip install -r requirements.txt

clear

## Setup for Windows 10

### 1. Clone the repo

Clone the repo into a new folder by running.
Instructions follow a working directory in `D:\Repos`, but this can be any path of your choosing.

    D:\Repos\git clone https://gitlab.com/mathco-engineering/incremental-lift.git

This would create a new folder `incremental-lift` in your working directory.

### 2. Create a Virtual Environment

1. Make sure you have Python 3.6 installed.

        D:\Repos\python --version
 
    This should give you something like `Python 3.6.x`
    
2. Install _virtualenv_
        
        D:\Repos\pip install virtualenv
        
3. Create a virtual environment for the project

        D:\Repos\cd incremental-lift
        D:\Repos\incremental-lift\virtualenv venv
        
    This would create a folder called `venv` in your project folder.
    
4. Activate the environment
    
    To start using the new environment
    
        D:\Repos\incremental-lift>venv\Scripts\activate
    
    Your command prompt would change to
    
        (venv) D:\Repos\incremental-lift>
 
5. Install the requirements
 
        (venv) D:\Repos\incremental-lift>pip install -r requirements.txt
        
    Once this command finishes, you should have the virtual environment setup.
    You would have to activate it while working with tool.
 
 ### 3. Starting the server

The Server can be started using the command
        
    (venv) D:\Repos\incremental-lift>cd test
    
    (venv) D:\Repos\incremental-lift\test>python manage.py runserver
    
 
# Setting up Testing

## Install Pytest

    (venv) D:\Repos\incremental-lift>pip install pytest
    
## Install current module for testing locally
    
    (venv) D:\Repos\incremental-lift>cd test
    
    (venv) D:\Repos\incremental-lift\test>pip install -e .
    
## Run the tests
    
    (venv) D:\Repos\incremental-lift\test>cd tests\unit
    
    (venv) D:\Repos\incremental-lift\test\tests\unit>pytest