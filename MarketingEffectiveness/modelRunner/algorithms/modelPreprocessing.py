from datetime import timedelta
from dateutil.relativedelta import relativedelta
import pandas as pd
import numpy as np


def date_range(initial_date,final_date):
    delta = final_date - initial_date
    date_list = []
    for i in range(delta.days + 1):
        date_list.append((initial_date + timedelta(i)).strftime("%Y-%m-%d"))
    return date_list


def outlier_treatment(df, dep_var):
    # Calculating lower limit
    loval = round(np.percentile(df[dep_var], 1),0)
    # Calculating higher limit
    hival = round(np.percentile(df[dep_var], 99),0)
    # Calculating lower limit
    himean=df[dep_var].mean()+2*df[dep_var].std()
    # Calculating upper limit
    lomean=df[dep_var].mean()-2*df[dep_var].std()
    if(loval == hival) or (loval == 0) or (hival == 0) or (lomean == 0) or (himean == 0) or (lomean == himean):
        value = []
        for x in df[dep_var]:
            value.append(x) 
    elif(loval != hival) or (loval != 0) or (hival != 0):
        value = []
        for x in df[dep_var]:
            if loval > x:
                value.append(loval)
            elif hival < x:
                value.append(hival)
            else:
                value.append(x)
    else:
        value = []
        for x in df[dep_var]:
            if lomean > x:
                value.append(lomean)
            elif himean < x:
                value.append(himean)
            else:
                value.append(x)
    return value


def check_data_sufficiency(train_data, adjusted_kpi):

    data_sufficiency = train_data.groupby(adjusted_kpi).count()
    data_sufficiency = data_sufficiency.reset_index()

    data_sufficiency.columns = [adjusted_kpi, 'COUNT', 'request_count', 'perf_metric']
    data_sufficiency['CONTRIBUTION'] = data_sufficiency.COUNT/data_sufficiency.COUNT.sum()*100

    data_sufficiency = data_sufficiency[data_sufficiency[adjusted_kpi] == 0]

    if len(data_sufficiency) == 0:
        data_sufficiency_key = 'Pass'
    else:
        if data_sufficiency['CONTRIBUTION'].sum() >= 20:
            data_sufficiency_key = 'Fail'
        else:
            data_sufficiency_key = 'Pass'

    return data_sufficiency_key


def model_preprocess(df, filters_row):
    performance_metric_mapping={'Sales':'total_sales','Orders':'total_orders','Customers':'total_customer','Visits':'total_visits'}
    performance_metric = performance_metric_mapping.get(filters_row.performance_metric,None)

    request_id = filters_row.request_id

    # Test Dates
    test_start_date = filters_row.test_start_date - relativedelta(days=(14))
    test_end_date = filters_row.test_end_date

    # Train Dates
    train_start_date = filters_row.test_start_date - relativedelta(year = filters_row.test_start_date.year-2, days=(14))
    train_end_date = test_start_date - relativedelta(days=1)

    # Pre Period Test Dates 
    pre_test_start_date = filters_row.test_start_date - relativedelta(days=(14))
    pre_test_end_date = filters_row.test_start_date - relativedelta(days=1)

    # Post Period Test Dates
    post_test_start_date = filters_row.test_start_date 
    post_test_end_date = filters_row.test_end_date

    # Test train date ranges
    train_dates = date_range(train_start_date,train_end_date)
    test_dates = date_range(test_start_date,test_end_date)

    # Converting the dates to dataframe
    train_date_data = pd.DataFrame({'train_date':train_dates})
    train_date_data['train_date'] = pd.to_datetime((train_date_data['train_date']), format="%Y-%m-%d")
    test_date_data = pd.DataFrame({'test_date':test_dates})
    test_date_data['test_date'] = pd.to_datetime((test_date_data['test_date']), format="%Y-%m-%d")

    # Grouping the sales for a KPI at a date level
    sales_data = df[['date', performance_metric]]
    sales_data = sales_data.groupby('date').sum()
    sales_data = sales_data.reset_index()
    sales_data['request_id'] = request_id
    sales_data['performance_metric'] = performance_metric
    train_data = sales_data.copy()
    test_data = sales_data.copy()

    #  # Splitting data into test and train
    # Set index
    train_data = train_data.set_index(train_data['date'])
    train_data['date'] = pd.to_datetime((train_data['date']), format="%Y-%m-%d")
    test_data = test_data.set_index(test_data['date'])
    test_data['date'] = pd.to_datetime((test_data['date']), format="%Y-%m-%d")

    # Select observations between two datetimes
    train_data = train_data.loc[train_start_date:train_end_date]
    test_data = test_data.loc[test_start_date:test_end_date]

    test_data_pre = test_data.loc[pre_test_start_date:pre_test_end_date]
    test_data_pre['period'] = 'Pre'
    test_data_post = test_data.loc[post_test_start_date:post_test_end_date]
    test_data_post['period'] = 'Post'
    test_data = pd.concat([test_data_pre.reset_index(drop=True),test_data_post.reset_index(drop=True)],axis=0)
    test_data = test_data.set_index(test_data['date'])

    # Treating the data for outliers
    train_data_adj = outlier_treatment(train_data,performance_metric)
    train_data_adj = pd.Series(train_data_adj)
    test_data_adj = outlier_treatment(test_data,performance_metric)
    test_data_adj = pd.Series(test_data_adj)

    # Train and test data post outlier
    train_data['adjusted_kpi'] = train_data_adj.values
    train_data = train_data.drop([performance_metric],axis=1)
    test_data['adjusted_kpi'] = test_data_adj.values
    test_data = test_data.drop([performance_metric],axis=1)

    # Merging the the dataframe with train dates to get train data
    train_data = train_data.reset_index(drop=True)
    train_date_data = train_date_data.reset_index(drop=True)
    train_data.date = pd.to_datetime(train_data.date)
    train_data = train_date_data.merge(train_data, how='left', left_on='train_date', right_on='date')
    train_data = train_data.drop(['date'], axis=1)
    train_data['adjusted_kpi'].fillna(0, inplace=True)
    train_data['performance_metric'] = performance_metric
    train_data['request_id'] = request_id

    # Merging the the dataframe with test dates to get test data
    test_data = test_data.reset_index(drop=True)
    test_date_data = test_date_data.reset_index(drop=True)
    test_data = test_date_data.merge(test_data, how='left',left_on='test_date',right_on='date')
    test_data = test_data.drop(['date'],axis=1)
    test_data['adjusted_kpi'].fillna(0, inplace=True)
    test_data['performance_metric'] = performance_metric
    test_data['request_id'] = request_id

    # Test Data modifications to append pre period to train data
    test_data_adj = test_data.copy()
    test_data_adj = test_data_adj.drop(['period'],axis=1)
    test_data_adj = test_data_adj.set_index(test_data_adj['test_date'])
    test_data_adj = test_data_adj[(test_data_adj['test_date'] >= pre_test_start_date) & (test_data_adj['test_date'] <= pre_test_end_date)]
    test_data_adj = test_data_adj.reset_index(drop=True)
    test_data_adj['date'] = test_data_adj['test_date']
    test_data_adj = test_data_adj.drop(['test_date'],axis=1)
    # Train Data modifications to append pre period to train data
    train_data_adj = train_data.copy()
    train_data_adj = train_data_adj.reset_index(drop=True)
    train_data_adj['date'] = train_data_adj['train_date']
    train_data_adj = train_data_adj.drop(['train_date'],axis=1)
    train_data_adj = train_data_adj.append(test_data_adj,ignore_index =True)
    train_data_adj['train_date'] = train_data_adj['date']
    train_data_adj = train_data_adj.drop(['date'],axis=1)
    train_data_adj = train_data_adj.set_index(['train_date'])

    data_sufficiency_key = check_data_sufficiency(train_data.copy(), 'adjusted_kpi')

    return train_data, test_data, data_sufficiency_key, train_data_adj



