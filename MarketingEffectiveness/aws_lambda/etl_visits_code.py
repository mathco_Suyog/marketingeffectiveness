from snowflake.sqlalchemy import URL
from sqlalchemy import create_engine
import pandas as pd
import cred
import time
pd.set_option('display.max_columns', None)

class SnowflakeConn(object):
    """
    Create a connection to Snowflake.

    Has operations to insert and fetch data, run ad-hoc queries.
    """


    def __init__(self, creds):
        """Initialize sqlalchemy link."""
        self.engine = create_engine(
            URL(
                account=creds.input_data_account,
                user=creds.input_data_user,
                password=creds.input_data_password,
                database=creds.input_data_database,
            )
        )

        self.connection = self.engine.connect()

    def run_query(self, query, op_format="pandas"):
        """Run a query using the conn."""
        if op_format == "pandas":
            df = pd.read_sql_query(query, self.connection)
        else:
            df = pd.DataFrame()
        return df

    def close(self):
        """Close active link."""
        self.connection.close()

def process_request(event, context = None):
    print("Execution Process Started")
    start_time = time.time()
    lifttool = {'database' : 'lift_stats', 'schema':'public'}
    dbobj=SnowflakeConn(cred)
    dbobj.run_query('ALTER SESSION SET ABORT_DETACHED_QUERY=TRUE')

    visits_ex = dbobj.run_query("select count(*) from {db_name}.{schema}.visits_input".format(db_name = lifttool['database'], schema = lifttool['schema']))
    visits_ex = visits_ex.values.tolist()

    try:
        start_time_inp_visits = time.time()
        dbobj.run_query('drop table if exists {db_name}.{schema}.input_visits_table'.format(db_name = lifttool['database'], schema = lifttool['schema']))
        print("Previous input_visits_table  if any got dropped")

        visits_query1='''create table {db_name}.{schema}.input_visits_table as 
            select to_date(a.started_at_ts_utc) as Date,upper(trim(a.geo_network_country)) as Country, upper(trim(a.geo_network_region)) as Region,
            upper(trim(b.DMA)) as DMA,upper(trim(a.geo_network_city)) as City,upper(trim(a.channel_grouping)) as Last_Acquisition_Channel,
            upper(trim(d.Paid_Free)) as Paid_Free, upper(trim(d.Funnel_Position)) as Funnel_Position,
            case when total_new_visits='1' then 'New' else 'Returning' end as Visitor_Status,
            count(distinct a.id) as Total_Visits,
            count(distinct(order_name_first_transaction)) as Total_Orders
            from birdfacts.public.fact_online_sessions as a left join {db_name}.{schema}.dma_mapping as b 
            on upper(trim(a.geo_network_region))=upper(trim(b.region)) and upper(trim(a.geo_network_city))=upper(trim(b.city))
            left join birdfacts.wip.ga_channel_map as c on upper(trim(a.channel_grouping))=upper(trim(c.ga_default_grouping))
            left join birdfacts.wip.fact_account_number as d on upper(trim(c.ga_channel_grouped))=upper(trim(d.ga_channel_grouped))
            group by to_date(a.started_at_ts_utc),a.geo_network_country,a.geo_network_region,b.DMA, a.geo_network_city, a.channel_grouping,
            d.paid_free, d.funnel_position,a.total_new_visits'''.format(db_name = lifttool['database'], schema = lifttool['schema'])
        dbobj.run_query(visits_query1)
        end_time_inp_visits = time.time()
        print("The input_visits_table got created")
        print("Total time taken to create input_visits_table :{} minutes".format((end_time_inp_visits-start_time_inp_visits)/60))

        start_time_visits_inp = time.time()
        dbobj.run_query('''truncate table {db_name}.{schema}.visits_input;'''.format( db_name = lifttool['database'], schema = lifttool['schema']))
        print("visits_input table got truncated")

        visits_query2='''insert into {db_name}.{schema}.visits_input (date, country, region, dma, city, last_acquisition_channel, paid_free, funnel_position, visitor_status, total_visits, total_orders)
            select date, country, region, dma, city, last_acquisition_channel, paid_free, funnel_position, visitor_status, total_visits, total_orders
            from {db_name}.{schema}.input_visits_table'''.format(db_name = lifttool['database'], schema = lifttool['schema'])
        dbobj.run_query(visits_query2)
        end_time_visits_inp = time.time()
        print("visits_input table got filled")
        print("The time taken to truncate and fill in visits_input table:{} minutes".format((end_time_visits_inp-start_time_visits_inp)/60))

        start_time_visit_filters = time.time()
        dbobj.run_query('''truncate table {db_name}.{schema}.visit_filters;'''.format(db_name = lifttool['database'], schema = lifttool['schema']))
        print("visit_filters table got truncated")
        visits_query3='''insert into {db_name}.{schema}.visit_filters (country, region, dma, city, last_acquisition_channel, paid_free, funnel_position, visitor_status)
        select distinct country, region, dma, city, last_acquisition_channel, paid_free, funnel_position, visitor_status
        from {db_name}.{schema}.visits_input
        order by 1,2,3,4,5,6,7,8'''.format(db_name = lifttool['database'], schema = lifttool['schema'])
        dbobj.run_query(visits_query3)
        end_time_visit_filters = time.time()
        print("visit_filters table got filled")
        print("Time taken to truncate and fill in visit_filters table: {} minutes".format((end_time_visit_filters - start_time_visit_filters)/60))

    except Exception as e:
        print(e)
        return
    end_time = time.time()

    visits_present = dbobj.run_query("select count(*) from {db_name}.{schema}.visits_input".format(db_name = lifttool['database'], schema = lifttool['schema']))
    visits_present = visits_present.values.tolist()

    visits_records = visits_present[0][0] - visits_ex[0][0]
    print("The time taken to run the code {0} minutes".format((end_time-start_time)/60))
    print("The total number records appended today for visits:{0}".format(visits_records))
if __name__=="__main__":
    process_request("etl_visits")



