import pytest
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from snowflake.sqlalchemy import URL
import cred


@pytest.fixture(scope='module')
def connection():
    engine = create_engine(URL(
        account=cred.account,
        user=cred.user,
        password=cred.password,
        database=cred.database,
        schema=cred.schema,
        warehouse=cred.warehouse,
        role=cred.role
    ), echo=False)

    connection = engine.connect()
    yield connection
    connection.close()


@pytest.fixture(scope='function')
def session(connection):
    Session = sessionmaker()
    transaction = connection.begin()
    session = Session(bind=connection)
    yield session
    session.close()
    transaction.rollback()
