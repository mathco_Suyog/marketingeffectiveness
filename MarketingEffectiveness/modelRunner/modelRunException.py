class ModelRunException(Exception):

    def __init__(self, text):
        super().__init__(text)


class DataSufficiencyException(ModelRunException):
    def __init__(self):
        self._message = 'Data Sufficiency Failed. Not enough data available.'
        super().__init__(self._message)


class MAPEException(ModelRunException):
    def __init__(self, mape_value):
        self._mape_value = mape_value
        self._message = 'MAPE: {}, is not within threshold'.format(self._mape_value)
        super().__init__(self._message)

    def __str__(self):
        return self._message

    def __repr__(self):
        return 'MAPEException({})'.format(self._mape_value)


class NoOptimalSolutionException(ModelRunException):
    def __init__(self, run_count=0):
        self._message = 'No optimal solution found in {} runs'.format(run_count)
        super().__init__(self._message)
