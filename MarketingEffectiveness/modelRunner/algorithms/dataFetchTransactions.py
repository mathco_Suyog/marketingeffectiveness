from sqlalchemy import and_, or_, case, literal_column
from dateutil.relativedelta import relativedelta
import pandas as pd
from modelRunner.models.fc_schema import Base, TransactionsInput, VisitsInput, RequestFilters


def actual_expected_data_fetcher_transactions(filters_row, session):
    # Business Channel ( All but no multiselect )
    if filters_row.business_channel == 'Total':
        filter_business_channel_query = or_(TransactionsInput.shop == None, TransactionsInput.shop != None)
    else:
        channel_list = [x.strip() for x in filters_row.business_channel.split(',')]
        filter_business_channel_query = TransactionsInput.shop.in_(channel_list)

    # Visitor Type ( All but no multiselect )
    if filters_row.visitor_type == 'All':
        filter_visitor_types_query = or_(TransactionsInput.visitor_status != None,
                                         TransactionsInput.visitor_status == None)
    else:
        filter_visitor_types = [x.strip() for x in filters_row.visitor_type.split(',')]
        filter_visitor_types_query = TransactionsInput.visitor_status.in_(filter_visitor_types)

    # Customer Type ( All but no multiselect )
    if filters_row.customer_type == 'All':
        filter_customer_type_query = or_(TransactionsInput.customer_status == None,
                                         TransactionsInput.customer_status != None)
    else:
        filter_customer_types = [x.strip() for x in filters_row.customer_type.split(',')]
        filter_customer_type_query = TransactionsInput.customer_status.in_(filter_customer_types)

    # Product Category ( All but no multiselect )
    if filters_row.product_category == 'All':
        filter_product_category_query = or_(TransactionsInput.product_category == None,
                                            TransactionsInput.product_category != None)
    else:
        filter_product_categories = [x.strip() for x in filters_row.product_category.split(',')]
        filter_product_category_query = TransactionsInput.product_category.in_(filter_product_categories)

    # Product SubCategory ( All and multiselect )
    if filters_row.product_sub_category == 'All':
        filter_subcategory_query = or_(TransactionsInput.product_subcategory == None,
                                       TransactionsInput.product_subcategory != None)
    else:
        filter_subcategories = [x.strip() for x in filters_row.product_sub_category.split(',')]
        filter_subcategory_query = TransactionsInput.product_subcategory.in_(filter_subcategories)

    # Test Regions
    filter_test_regions_query = get_regions_from_filter_text(filters_row.test_geo_region,
                                                             filters_row.test_geo_country)

    # Control Regions
    filter_control_regions_query = get_regions_from_filter_text(filters_row.control_geo_region,
                                                                filters_row.control_geo_country)

    # Test DMAs
    filter_test_dmas_query = get_dmas_from_filter_text(filters_row.test_geo_dma, filter_test_regions_query)

    # Control DMAs
    filter_control_dmas_query = get_dmas_from_filter_text(filters_row.control_geo_dma, filter_control_regions_query)

    # Test cities
    filter_test_cities_query = get_cities_from_filter_text(filters_row.test_geo_city, filter_test_dmas_query)

    # Control cities
    filter_control_cities_query = get_cities_from_filter_text(filters_row.control_geo_city, filter_control_dmas_query)

    # Test Material
    if filters_row.test_product_material == 'All':
        filter_test_materials_query = or_(TransactionsInput.material == None, TransactionsInput.material != None)
    else:
        filter_test_materials = [x.strip() for x in filters_row.test_product_material.split(',')]
        filter_test_materials_query = TransactionsInput.material.in_(filter_test_materials)

    # Control Material
    if filters_row.control_product_material == 'All' or filters_row.control_product_material is None:
        filter_control_materials_query = or_(TransactionsInput.material != None, TransactionsInput.material == None)
    else:
        filter_control_materials = [x.strip() for x in filters_row.control_product_material.split(',')]
        filter_control_materials_query = TransactionsInput.material.in_(filter_control_materials)

    # Control Style
    if filters_row.control_product_style == 'All' or filters_row.control_product_style is None:
        filter_control_styles_query = or_(TransactionsInput.style != None, TransactionsInput.style == None)
    else:
        filter_control_styles = [x.strip() for x in filters_row.control_product_style.split(',')]
        filter_control_styles_query = TransactionsInput.style.in_(filter_control_styles)

    # Test Style
    if filters_row.test_product_style == 'All':
        filter_test_styles_query = or_(TransactionsInput.style != None, TransactionsInput.style == None)
    else:
        filter_test_styles = [x.strip() for x in filters_row.test_product_style.split(',')]
        filter_test_styles_query = TransactionsInput.style.in_(filter_test_styles)

    # Channel
    if filters_row.acquisition_channel_type == 'All':
        filter_test_channels_query = or_(TransactionsInput.last_acquisition_channel != None,
                                         TransactionsInput.last_acquisition_channel == None)
    else:
        filter_test_channels = [x.strip() for x in filters_row.acquisition_channel_type.split(',')]
        filter_test_channels_query = TransactionsInput.last_acquisition_channel.in_(filter_test_channels)

    # paid/free
    if filters_row.acquisition_channel_paid_free == 'All':
        filter_test_channels_paid_free_query = or_(TransactionsInput.paid_free != None,
                                                   TransactionsInput.paid_free == None)
    else:
        filter_test_channels_paid_free = [x.strip() for x in filters_row.acquisition_channel_paid_free.split(',')]
        filter_test_channels_paid_free_query = TransactionsInput.paid_free.in_(filter_test_channels_paid_free)

    # funnel
    if filters_row.acquisition_channel_funnel == 'All':
        filter_test_funnels_query = or_(TransactionsInput.funnel_position != None,
                                        TransactionsInput.funnel_position == None)
    else:
        filter_test_funnels = [x.strip() for x in filters_row.acquisition_channel_funnel.split(',')]
        filter_test_funnels_query = TransactionsInput.funnel_position.in_(filter_test_funnels)

    qry = (
        session.query(TransactionsInput)
        .filter(and_(
            filter_business_channel_query,
            filter_visitor_types_query,
            filter_customer_type_query,
            filter_product_category_query,
            filter_subcategory_query,

            TransactionsInput.date >= str(filters_row.test_start_date -
                                          relativedelta(year=filters_row.test_start_date.year-2, days= 14)),
            TransactionsInput.date <= str(filters_row.test_end_date),                                 # post end
          
            filter_test_cities_query,
            filter_test_materials_query,
            filter_test_styles_query,
     
            filter_test_channels_query,
            filter_test_channels_paid_free_query,
            filter_test_funnels_query
            )
        )
    )
    df = pd.read_sql(qry.statement, qry.session.bind)
    # print(qry.statement.compile(qry.session.bind, compile_kwargs={"literal_binds": True}))
    return df

##----------------------------------------------LAST_YEAR_SAME_MARKET--------------------------------------------##


def last_year_same_market_data_fetcher_transactions(filters_row, session):
    # Business Channel ( All but no multiselect )
    if filters_row.business_channel == 'Total':
        filter_business_channel_query = or_(TransactionsInput.shop == None, TransactionsInput.shop != None)
    else:
        channel_list = [x.strip() for x in filters_row.business_channel.split(',')]
        filter_business_channel_query = TransactionsInput.shop.in_(channel_list)

    # Visitor Type ( All but no multiselect )
    if filters_row.visitor_type == 'All':
        filter_visitor_types_query = or_(TransactionsInput.visitor_status != None,
                                         TransactionsInput.visitor_status == None)
    else:
        filter_visitor_types = [x.strip() for x in filters_row.visitor_type.split(',')]
        filter_visitor_types_query = TransactionsInput.visitor_status.in_(filter_visitor_types)

    # Customer Type ( All but no multiselect )
    if filters_row.customer_type == 'All':
        filter_customer_type_query = or_(TransactionsInput.customer_status == None,
                                         TransactionsInput.customer_status != None)
    else:
        filter_customer_types = [x.strip() for x in filters_row.customer_type.split(',')]
        filter_customer_type_query = TransactionsInput.customer_status.in_(filter_customer_types)

    # Product Category ( All but no multiselect )
    if filters_row.product_category == 'All':
        filter_product_category_query = or_(TransactionsInput.product_category == None,
                                            TransactionsInput.product_category != None)
    else:
        filter_product_categories = [x.strip() for x in filters_row.product_category.split(',')]
        filter_product_category_query = TransactionsInput.product_category.in_(filter_product_categories)

    # Product SubCategory ( All and multiselect )
    if filters_row.product_sub_category == 'All':
        filter_subcategory_query = or_(TransactionsInput.product_subcategory == None,
                                       TransactionsInput.product_subcategory != None)
    else:
        filter_subcategories = [x.strip() for x in filters_row.product_sub_category.split(',')]
        filter_subcategory_query = TransactionsInput.product_subcategory.in_(filter_subcategories)

    # Test Regions
    filter_test_regions_query = get_regions_from_filter_text(filters_row.test_geo_region,
                                                             filters_row.test_geo_country)

    # Control Regions
    filter_control_regions_query = get_regions_from_filter_text(filters_row.control_geo_region,
                                                                filters_row.control_geo_country)

    # Test DMAs
    filter_test_dmas_query = get_dmas_from_filter_text(filters_row.test_geo_dma, filter_test_regions_query)

    # Control DMAs
    filter_control_dmas_query = get_dmas_from_filter_text(filters_row.control_geo_dma, filter_control_regions_query)

    # Test cities
    filter_test_cities_query = get_cities_from_filter_text(filters_row.test_geo_city, filter_test_dmas_query)

    # Control cities
    filter_control_cities_query = get_cities_from_filter_text(filters_row.control_geo_city, filter_control_dmas_query)

    # Test Material
    if filters_row.test_product_material == 'All':
        filter_test_materials_query = or_(TransactionsInput.material == None, TransactionsInput.material != None)
    else:
        filter_test_materials = [x.strip() for x in filters_row.test_product_material.split(',')]
        filter_test_materials_query = TransactionsInput.material.in_(filter_test_materials)

    # Control Material
    if filters_row.control_product_material == 'All' or filters_row.control_product_material is None:
        filter_control_materials_query = or_(TransactionsInput.material != None, TransactionsInput.material == None)
    else:
        filter_control_materials = [x.strip() for x in filters_row.control_product_material.split(',')]
        filter_control_materials_query = TransactionsInput.material.in_(filter_control_materials)

    # Control Style
    if filters_row.control_product_style == 'All' or filters_row.control_product_style is None:
        filter_control_styles_query = or_(TransactionsInput.style != None, TransactionsInput.style == None)
    else:
        filter_control_styles = [x.strip() for x in filters_row.control_product_style.split(',')]
        filter_control_styles_query = TransactionsInput.style.in_(filter_control_styles)

    # Test Style
    if filters_row.test_product_style == 'All':
        filter_test_styles_query = or_(TransactionsInput.style != None, TransactionsInput.style == None)
    else:
        filter_test_styles = [x.strip() for x in filters_row.test_product_style.split(',')]
        filter_test_styles_query = TransactionsInput.style.in_(filter_test_styles)

    # Channel
    if filters_row.acquisition_channel_type == 'All':
        filter_test_channels_query = or_(TransactionsInput.last_acquisition_channel != None,
                                         TransactionsInput.last_acquisition_channel == None)
    else:
        filter_test_channels = [x.strip() for x in filters_row.acquisition_channel_type.split(',')]
        filter_test_channels_query = TransactionsInput.last_acquisition_channel.in_(filter_test_channels)

    # paid/free
    if filters_row.acquisition_channel_paid_free == 'All':
        filter_test_channels_paid_free_query = or_(TransactionsInput.paid_free != None,
                                                   TransactionsInput.paid_free == None)
    else:
        filter_test_channels_paid_free = [x.strip() for x in filters_row.acquisition_channel_paid_free.split(',')]
        filter_test_channels_paid_free_query = TransactionsInput.paid_free.in_(filter_test_channels_paid_free)

    # funnel
    if filters_row.acquisition_channel_funnel == 'All':
        filter_test_funnels_query = or_(TransactionsInput.funnel_position != None,
                                        TransactionsInput.funnel_position == None)
    else:
        filter_test_funnels = [x.strip() for x in filters_row.acquisition_channel_funnel.split(',')]
        filter_test_funnels_query = TransactionsInput.funnel_position.in_(filter_test_funnels)
    
    qry = (
        session.query(TransactionsInput,
                      case([
                          (and_(
                              TransactionsInput.date >= str(filters_row.test_start_date),
                              TransactionsInput.date <= str(filters_row.test_end_date)),
                           literal_column("'Post_ThisYear'")),
                          (and_(
                              TransactionsInput.date >= str(filters_row.control_start_date),
                              TransactionsInput.date <= str(filters_row.control_end_date)),
                           literal_column("'Pre_ThisYear'")),
                          (and_(
                              TransactionsInput.date >= str(filters_row.test_start_date -
                                                            relativedelta(year=filters_row.test_start_date.year-1)),
                              TransactionsInput.date <= str(filters_row.test_end_date -
                                                            relativedelta(year=filters_row.test_end_date.year-1))),
                           literal_column("'Post_LastYear'")),
                          (and_(
                              TransactionsInput.date >= str(filters_row.control_start_date -
                                                            relativedelta(year=filters_row.control_start_date.year-1)),
                              TransactionsInput.date <= str(filters_row.control_end_date -
                                                            relativedelta(year=filters_row.control_end_date.year-1))),
                           literal_column("'Pre_LastYear'"))], else_=literal_column("'ignore'")).label("'flag'"))
        .filter(and_(
            filter_business_channel_query,
            filter_visitor_types_query,
            filter_customer_type_query,
            filter_product_category_query,
            filter_subcategory_query,
            or_(
                and_(
                    TransactionsInput.date >= str(filters_row.test_start_date),     # post start
                    TransactionsInput.date <= str(filters_row.test_end_date)        # post end
                ),
                and_(
                    TransactionsInput.date >= str(filters_row.control_start_date),  # pre start
                    TransactionsInput.date <= str(filters_row.control_end_date)     # pre end
                ),
                or_(
                    and_(
                        TransactionsInput.date >= str(filters_row.test_start_date -
                                                      relativedelta(year=filters_row.test_start_date.year-1)),  #post start
                        TransactionsInput.date <= str(filters_row.test_end_date -
                                                      relativedelta(year=filters_row.test_end_date.year-1))  #post end
                       ),
                    and_(
                        TransactionsInput.date >= str(filters_row.control_start_date -
                                                      relativedelta(year=filters_row.control_start_date.year-1)),  # pre start
                        TransactionsInput.date <= str(filters_row.control_end_date -
                                                      relativedelta(year=filters_row.control_end_date.year-1))     # pre end
                       )
                   )
            ),
           
            filter_test_cities_query,
            filter_test_materials_query,
            filter_test_styles_query,
     
            filter_test_channels_query,
            filter_test_channels_paid_free_query,
            filter_test_funnels_query
            )
        )
    )

    df = pd.read_sql(qry.statement, qry.session.bind)
    # print(qry.statement.compile(qry.session.bind, compile_kwargs={"literal_binds": True}))
    return df

# #----------------------------------------------SAME_YEAR_DIFF_MARKET----------------------------------##

def sydm_transactions(filters_row, session):
    # Business Channel ( All but no multiselect )
    if filters_row.business_channel == 'Total':
        filter_business_channel_query = or_(TransactionsInput.shop == None, TransactionsInput.shop != None)
    else:
        channel_list = [x.strip() for x in filters_row.business_channel.split(',')]
        filter_business_channel_query = TransactionsInput.shop.in_(channel_list)

    # Visitor Type ( All but no multiselect )
    if filters_row.visitor_type == 'All':
        filter_visitor_types_query = or_(TransactionsInput.visitor_status != None,
                                         TransactionsInput.visitor_status == None)
    else:
        filter_visitor_types = [x.strip() for x in filters_row.visitor_type.split(',')]
        filter_visitor_types_query = TransactionsInput.visitor_status.in_(filter_visitor_types)

    # Customer Type ( All but no multiselect )
    if filters_row.customer_type == 'All':
        filter_customer_type_query = or_( TransactionsInput.customer_status == None,
                                          TransactionsInput.customer_status != None)
    else:
        filter_customer_types = [x.strip() for x in filters_row.customer_type.split(',')]
        filter_customer_type_query = TransactionsInput.customer_status.in_(filter_customer_types)

    # Product Category ( All but no multiselect )
    if filters_row.product_category == 'All':
        filter_product_category_query = or_(TransactionsInput.product_category == None,
                                            TransactionsInput.product_category != None)
    else:
        filter_product_categories = [x.strip() for x in filters_row.product_category.split(',')]
        filter_product_category_query = TransactionsInput.product_category.in_(filter_product_categories)

    # Product SubCategory ( All and multiselect )
    if filters_row.product_sub_category == 'All':
        filter_subcategory_query = or_(TransactionsInput.product_subcategory == None,
                                       TransactionsInput.product_subcategory != None)
    else:
        filter_subcategories = [x.strip() for x in filters_row.product_sub_category.split(',')]
        filter_subcategory_query = TransactionsInput.product_subcategory.in_(filter_subcategories)


    # Test Regions
    filter_test_regions_query = get_regions_from_filter_text(filters_row.test_geo_region,
                                                             filters_row.test_geo_country)

    # Control Regions
    filter_control_regions_query = get_regions_from_filter_text(filters_row.control_geo_region,
                                                                filters_row.control_geo_country)

    # Test DMAs
    filter_test_dmas_query = get_dmas_from_filter_text(filters_row.test_geo_dma, filter_test_regions_query)

    # Control DMAs
    filter_control_dmas_query = get_dmas_from_filter_text(filters_row.control_geo_dma, filter_control_regions_query)

    # Test cities
    filter_test_cities_query = get_cities_from_filter_text(filters_row.test_geo_city, filter_test_dmas_query)
    
    # Control cities
    filter_control_cities_query = get_cities_from_filter_text(filters_row.control_geo_city, filter_control_dmas_query)
    
    # Test Material
    if filters_row.test_product_material == 'All':
        filter_test_materials_query = or_(TransactionsInput.material == None, TransactionsInput.material != None)
    else:
        filter_test_materials = [x.strip() for x in filters_row.test_product_material.split(',')]
        filter_test_materials_query = TransactionsInput.material.in_(filter_test_materials)

    # Control Material
    if filters_row.control_product_material == 'All':
        filter_control_materials_query = or_(TransactionsInput.material != None,TransactionsInput.material == None)
    else:
        filter_control_materials = [x.strip() for x in filters_row.control_product_material.split(',')]
        filter_control_materials_query = TransactionsInput.material.in_(filter_control_materials)

    # Control Style
    if filters_row.control_product_style == 'All':
        filter_control_styles_query = or_(TransactionsInput.style != None, TransactionsInput.style == None)
    else:
        filter_control_styles = [x.strip() for x in filters_row.control_product_style.split(',')]
        filter_control_styles_query = TransactionsInput.style.in_(filter_control_styles)

    # Test Style
    if filters_row.test_product_style == 'All':
        filter_test_styles_query = or_(TransactionsInput.style != None, TransactionsInput.style == None)
    else:
        filter_test_styles = [x.strip() for x in filters_row.test_product_style.split(',')]
        filter_test_styles_query = TransactionsInput.style.in_(filter_test_styles)

    # Channel
    if filters_row.acquisition_channel_type == 'All':
        filter_test_channels_query = or_(TransactionsInput.last_acquisition_channel != None,
                                         TransactionsInput.last_acquisition_channel == None)
    else:
        filter_test_channels = [x.strip() for x in filters_row.acquisition_channel_type.split(',')]
        filter_test_channels_query = TransactionsInput.last_acquisition_channel.in_(filter_test_channels)

    # paid/free
    if filters_row.acquisition_channel_paid_free == 'All':
        filter_test_channels_paid_free_query = or_(TransactionsInput.paid_free != None,
                                                   TransactionsInput.paid_free == None)
    else:
        filter_test_channels_paid_free = [x.strip() for x in filters_row.acquisition_channel_paid_free.split(',')]
        filter_test_channels_paid_free_query = TransactionsInput.paid_free.in_(filter_test_channels_paid_free)

    # funnel
    if filters_row.acquisition_channel_funnel == 'All':
        filter_test_funnels_query = or_(TransactionsInput.funnel_position != None,
                                        TransactionsInput.funnel_position == None)
    else:
        filter_test_funnels = [x.strip() for x in filters_row.acquisition_channel_funnel.split(',')]
        filter_test_funnels_query = TransactionsInput.funnel_position.in_(filter_test_funnels)

    qry = (
        session
        .query(
            TransactionsInput,
            case([
                (and_(
                    TransactionsInput.date >= str(filters_row.test_start_date),
                    TransactionsInput.date <= str(filters_row.test_end_date),
                    filter_test_cities_query,
                    filter_test_materials_query,
                    filter_test_styles_query
                    ), literal_column("'Post_Test'")),
                (and_(
                    TransactionsInput.date >= str(filters_row.control_start_date),
                    TransactionsInput.date <= str(filters_row.control_end_date),
                    filter_test_cities_query,
                    filter_test_materials_query,
                    filter_test_styles_query
                ), literal_column("'Pre_Test'")),
                (and_(
                    TransactionsInput.date >= str(filters_row.test_start_date),
                    TransactionsInput.date <= str(filters_row.test_end_date),
                    filter_control_cities_query,
                    filter_control_materials_query,
                    filter_control_styles_query
                    ), literal_column("'Post_Control'")),
                (and_(
                    TransactionsInput.date >= str(filters_row.control_start_date),
                    TransactionsInput.date <= str(filters_row.control_end_date),
                    filter_control_cities_query,
                    filter_control_materials_query,
                    filter_control_styles_query
                    ), literal_column("'Pre_Control'"))],
                else_=literal_column("'ignore'")).label("'flag'"))
        .filter(and_(
            filter_business_channel_query),
            filter_visitor_types_query,
            filter_customer_type_query,
            filter_product_category_query,
            filter_subcategory_query,
            or_(
                and_(
                    TransactionsInput.date >= str(filters_row.test_start_date),     # post start
                    TransactionsInput.date <= str(filters_row.test_end_date)        # post end
                ),
                and_(
                    TransactionsInput.date >= str(filters_row.control_start_date),  # pre start
                    TransactionsInput.date <= str(filters_row.control_end_date)     # pre end
                )
            ),
            or_(
                and_(                                                               # Test geo and materials
                    filter_test_cities_query,
                    filter_test_materials_query,
                    filter_test_styles_query
                ),
                and_(                                                               # Control Geo and Materials
                    filter_control_cities_query,
                    filter_control_materials_query,
                    filter_control_styles_query
                )
            ),
            filter_test_channels_query,
            filter_test_channels_paid_free_query,
            filter_test_funnels_query
            )
        )

    df = pd.read_sql(qry.statement, qry.session.bind)

    # print(qry.statement.compile(qry.session.bind, compile_kwargs={"literal_binds": True}))

    return df


def get_regions_from_filter_text(regions_filter_text, country_filter_text):
    if regions_filter_text == 'All':
        filter_regions_query = (TransactionsInput.country == country_filter_text)
        # or_(TransactionsInput.region == None, TransactionsInput.region != None))
    else:
        filter_test_regions = [x.strip() for x in regions_filter_text.split(',')]
        filter_regions_query = and_(TransactionsInput.country == country_filter_text,
                                    TransactionsInput.region.in_(filter_test_regions))
    return filter_regions_query


def get_dmas_from_filter_text(dma_filter_text, region_filter_query):
    if dma_filter_text == 'All':
        filter_dmas_query = region_filter_query
    else:
        filter_test_dmas = [x.strip() for x in dma_filter_text.split(',')]
        filter_dmas_query = and_(TransactionsInput.dma.in_(filter_test_dmas),
                                 region_filter_query)
    return filter_dmas_query


def get_cities_from_filter_text(city_filter_text, dma_filter_query):
    if city_filter_text == 'All':
        city_filter_query = dma_filter_query
    else:
        filter_test_cities = [x.strip() for x in city_filter_text.split(',')]
        city_filter_query = and_(TransactionsInput.city.in_(filter_test_cities),
                                 dma_filter_query)

    return city_filter_query

