import json
from fc_schema import VisitFilters, TransactionFilters, Base

def get_paid_free(event, context, session):

    event_measure = event["queryStringParameters"]["measure_performance"]
    if event_measure.lower() == 'visits':
        query_table = VisitFilters
    else:
        query_table = TransactionFilters
    
    qry = session.query(query_table.paid_free).filter(query_table.paid_free != None).distinct().order_by(query_table.paid_free).all()


    out_list = [ list(x) for x in qry if x[0] is not None]
    out_list.sort()
    out_list.insert(0, ['All'])

    #print(out_list)

    out_list = json.dumps(out_list, separators=(',',':'))

    response =  {
        'statusCode':200,
        'body': out_list,
        'headers':{},
        'isBase64Encoded':False
        }

    return response

if __name__ == '__main__':
    get_paid_free("sales",None)
