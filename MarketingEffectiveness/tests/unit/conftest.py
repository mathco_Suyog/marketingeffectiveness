import pytest
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from snowflake.sqlalchemy import URL
from modelRunner.models import cred
from modelRunner.models.fc_schema import RequestFilters, RequestDetails
import datetime


@pytest.fixture(scope='module')
def connection():
    engine = create_engine(URL(
        account=cred.account,
        user=cred.user,
        password=cred.password,
        database=cred.database,
        schema=cred.schema,
        warehouse=cred.warehouse,
        role=cred.role
    ), echo=False)

    connection = engine.connect()
    yield connection
    connection.close()


@pytest.fixture(scope='function')
def session(connection):
    Session = sessionmaker()
    transaction = connection.begin()
    session = Session(bind=connection)
    yield session
    session.close()
    transaction.rollback()

@pytest.fixture(scope = 'function')
def setup_log_test(session):
    new_details = RequestDetails(
        request_id='TEST-LOGTEST-TEST',
        models_run_completed=0,
        models_run_total=1,
        created_at_ts_utc=datetime.datetime.now(),
        status_code=2,
        user_id='testuser@testdomain.com',
        _row_created_at=datetime.datetime.utcnow(),
        _row_updated_at=datetime.datetime.utcnow()
    )
    session.add(new_details)



@pytest.fixture(scope = 'function')
def setup_requestfilters(session):
    request_id = [RequestFilters(
            request_id = 'b7ade7cc-953b-4421-a019-487014a89c2d',
            acquisition_channel_type = 'All',
            acquisition_channel_paid_free = 'All',
            acquisition_channel_funnel = 'All',
            business_channel = 'Total',
            comparison_technique = 'Actual vs. Expected',
            control_end_date = '2018-05-02',
            control_start_date = '2018-04-10',
            control_geo_city = 'All',
            control_geo_country = 'United States',
            control_geo_dma = 'All',
            control_geo_region = 'All',
            control_product_material = 'All',
            control_product_style = 'All',
            customer_type = 'All',
            performance_metric = 'Sales',
            product_category = 'All',
            product_sub_category = 'All',
            request_data = 'None',
            request_signature = 'None',
            test_end_date = '2018-05-20',
            test_start_date = '2018-05-07',
            test_geo_city = 'All',
            test_geo_country = 'United States',
            test_geo_dma = 'Boston',
            test_geo_region = 'All',
            test_product_material = 'All',
            test_product_style = 'All',
            visitor_type = 'All',
            _row_created_at = datetime.datetime.utcnow(),
            _row_updated_at = datetime.datetime.utcnow()),
            RequestFilters(
            request_id = '8aab27c5-f0d5-4731-9f5d-b52f57289fe2',
            acquisition_channel_type = 'All',
            acquisition_channel_paid_free = 'All',
            acquisition_channel_funnel = 'All',
            business_channel = 'Total',
            comparison_technique = 'Same Year - Different Market',
            control_end_date = '2018-10-28',
            control_start_date = '2018-09-01',
            control_geo_city = 'All',
            control_geo_country = 'United States',
            control_geo_dma = 'Boston,Washington',
            control_geo_region = 'All',
            control_product_material =  'All',
            control_product_style =  'All',
            customer_type = 'All',
            performance_metric = 'Sales',
            product_category = 'All',
            product_sub_category = 'All',
            request_data = 'None',
            request_signature ='None',
            test_end_date = '2018-12-09',
            test_start_date = '2018-10-29',
            test_geo_city ='All',
            test_geo_country = 'United States',
            test_geo_dma ='New York',
            test_geo_region ='All',
            test_product_material = 'All',
            test_product_style = 'All',
            visitor_type = 'All',
            _row_created_at = datetime.datetime.utcnow(),
            _row_updated_at = datetime.datetime.utcnow()),
            RequestFilters(
            request_id = 'e49d8b7c-b910-417a-82e8-cd3c4a528df3',
            acquisition_channel_type ='All',
            acquisition_channel_paid_free ='All',
            acquisition_channel_funnel ='All',
            business_channel ='Total',
            comparison_technique ='Same Year - Different Market',
            control_end_date ='2019-03-13',
            control_start_date ='2019-02-22',
            control_geo_city = 'All',
            control_geo_country ='UNITED STATES',
            control_geo_dma ='All',
            control_geo_region ='CALIFORNIA',
            control_product_material = None,
            control_product_style = None,
            customer_type = 'All',
            performance_metric ='Visits',
            product_category = '',
            product_sub_category = '',
            request_data ='None',
            request_signature ='None',
            test_end_date ='2019-03-28',
            test_start_date ='2019-03-14',
            test_geo_city ='All',
            test_geo_country ='UNITED STATES',
            test_geo_dma ='All',
            test_geo_region ='CALIFORNIA',
            test_product_material = '',
            test_product_style ='',
            visitor_type = 'All',
            _row_created_at = datetime.datetime.utcnow(),
            _row_updated_at = datetime.datetime.utcnow()),
            RequestFilters(
            request_id = '3d2bf2d0-aa75-44fd-b243-3908f803058d',
            acquisition_channel_type = 'All',
            acquisition_channel_paid_free = 'All',
            acquisition_channel_funnel = 'All',
            business_channel = 'Total',
            comparison_technique = 'Same Market - Last Year',
            control_end_date = '2019-03-14',
            control_start_date = '2019-02-23',
            control_geo_city = '',
            control_geo_country = 'United States',
            control_geo_dma = '',
            control_geo_region = '',
            control_product_material = None,
            control_product_style = None,
            customer_type = 'All',
            performance_metric = 'Sales',
            product_category = 'All',
            product_sub_category = 'All',
            request_data = 'None',
            request_signature = 'None',
            test_end_date = '2019-03-25',
            test_start_date = '2019-03-15',
            test_geo_city = 'All',
            test_geo_country = 'United States',
            test_geo_dma = 'All',
            test_geo_region = 'California',
            test_product_material = 'All',
            test_product_style = 'All',
            visitor_type = 'All',
            _row_created_at = datetime.datetime.utcnow(),
            _row_updated_at = datetime.datetime.utcnow()),
            RequestFilters(
            request_id = '6b1919d7-e88f-4bc8-9876-829b310e65db',
            acquisition_channel_type = 'All',
            acquisition_channel_paid_free = 'All',
            acquisition_channel_funnel = 'All',
            business_channel = 'Total',
            comparison_technique = 'Same Market - Last Year',
            control_end_date = '2019-04-27',
            control_start_date = '2019-04-21',
            control_geo_city = 'All',
            control_geo_country = 'UNITED STATES',
            control_geo_dma = 'All',
            control_geo_region = 'All',
            customer_type = 'All',
            performance_metric = 'Visits',
            product_category = 'All',
            product_sub_category = 'All',
            request_data = 'None',
            request_signature = 'None',
            test_end_date = '2019-05-09',
            test_start_date = '2019-05-05',
            test_geo_city = 'All',
            test_geo_country = 'UNITED STATES',
            test_geo_dma = 'All',
            test_geo_region = 'All',
            test_product_material = 'All',
            test_product_style = 'All',
            visitor_type = 'All',
            _row_created_at = datetime.datetime.utcnow(),
            _row_updated_at = datetime.datetime.utcnow()
        )
    ]
    session.add_all(request_id)