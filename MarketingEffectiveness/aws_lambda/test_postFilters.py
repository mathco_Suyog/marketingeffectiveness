import pytest
from fc_schema import RequestFilters
from db_inserter import lambda_handler
# from modelRunner.models import cred

@pytest.mark.parametrize("event",[{"email": "sameena.shaik@themathcompany.com", "performance_metric": "Sales",
	 "acquisition_channel_type": ["All"], "acquisition_channel_paid_free": "All", "acquisition_channel_funnel": "All", 
	 "business_channel": "Total", "comparison_technique": "Actual vs. Expected", "control_end_date": "2018-10-28", 
	 "control_start_date": "2018-09-01", "control_geo_city": [["All"]], "control_geo_country": ["United States"], 
	 "control_geo_dma": [["Boston"], ["Washington"]], "control_geo_region": [["All"]], "control_product_material": ["All"],
	 "control_product_style": ["All"], "customer_type": "All", "product_category": ["All"], "product_sub_category": ["All"], 
	 "test_end_date": "2018-12-09", "test_start_date": "2018-10-29", "test_geo_city": [["All"]], 
	 "test_geo_country": ["United States"], "test_geo_dma": [["New York"]], "test_geo_region": [["All"]], 
	 "test_product_material": ["All"], "test_product_style": ["All"], "visitor_type": "All"},
	 {"email": "sameena.shaik@themathcompany.com", "performance_metric": "Sales",
	 "acquisition_channel_type": ["All"], "acquisition_channel_paid_free": "All", "acquisition_channel_funnel": "All", 
	 "business_channel": "Total", "comparison_technique": "Same Market - Last Year", "control_end_date": "2019-03-19", 
	 "control_start_date": "2019-03-13", "control_geo_city": [["All"]], "control_geo_country": ["United States"], 
	 "control_geo_dma": [["BOSTON"]], "control_geo_region": [["MASSACHUSETTS"]], "control_product_material": ["All"],
	 "control_product_style": ["All"], "customer_type": "All", "product_category": ["SHOES"], "product_sub_category": ["All"], 
	 "test_end_date": "2019-03-19", "test_start_date": "2019-03-13", "test_geo_city": [["All"]], 
	 "test_geo_country": ["United States"], "test_geo_dma": [["BOSTON"]], "test_geo_region": [["Massachusetts"]], 
	 "test_product_material": ["All"], "test_product_style": ["All"], "visitor_type": "All"},
	 {"email": "sameena.shaik@themathcompany.com", "performance_metric": "Orders",
	 "acquisition_channel_type": ["All"], "acquisition_channel_paid_free": "Paid", "acquisition_channel_funnel": "All", 
	 "business_channel": "Total", "comparison_technique": "Same Year - Different Market", "control_end_date": "2019-04-06", 
	 "control_start_date": "2019-03-18", "control_geo_city": [["All"]], "control_geo_country": ["Canada"], 
	 "control_geo_dma": [["All"]], "control_geo_region": [["British Columbia"]], "control_product_material": ["Wool"],
	 "control_product_style": ["Wool Runner"], "customer_type": "All", "product_category": ["SHOES"], "product_sub_category": ["Athletic"], 
	 "test_end_date": "2019-04-13", "test_start_date": "2019-04-07", "test_geo_city": [["All"]], 
	 "test_geo_country": ["Canada"], "test_geo_dma": [["All"]], "test_geo_region": [["Ontario"]], 
	 "test_product_material": ["Wool"], "test_product_style": ["Wool Runner"], "visitor_type": "All"}])

def test_submitFilters(event,session):

	d = lambda_handler(event, None)
	request_id = (d['request_id'])
	row = (session.query(RequestFilters)
		.filter(RequestFilters.request_id == request_id)).one()

	assert row.performance_metric == event['performance_metric']
	assert row.acquisition_channel_type == "".join(event['acquisition_channel_type'])
	assert row.acquisition_channel_paid_free == event['acquisition_channel_paid_free']
	assert row.acquisition_channel_funnel == event['acquisition_channel_funnel']
	assert row.customer_type == event['customer_type']
	assert row.product_category == "".join(event['product_category'])
	assert row.product_sub_category == "".join(event['product_sub_category'])
	assert row.test_end_date.strftime('%Y-%m-%d') == event['test_end_date']
	assert row.test_start_date.strftime('%Y-%m-%d') == event['test_start_date']
	assert row.test_geo_city == ','.join(str(r) for v in event['test_geo_city'] for r in v)
	assert row.test_geo_country == "".join(event['test_geo_country'])
	assert row.test_geo_dma == ','.join(str(r) for v in event['test_geo_dma'] for r in v)
	assert row.test_geo_region == ','.join(str(r) for v in event['test_geo_region'] for r in v)
	assert row.test_product_material == "".join(event['test_product_material'])
	assert row.test_product_style == "".join(event['test_product_style'])
	assert row.visitor_type == event['visitor_type']
	assert row.business_channel == event['business_channel']

	if row.comparison_technique == 'Actual vs. Expected':
		
		assert row.control_end_date is None
		assert row.control_start_date is None
		assert row.control_geo_city == ''
		assert row.control_geo_country == ''
		assert row.control_geo_dma == ''
		assert row.control_geo_region == ''
		assert row.control_product_material == ''
		assert row.control_product_style == ''
	
	elif row.comparison_technique == 'Same Market - Last Year':
		
		assert row.control_end_date.strftime('%Y-%m-%d') ==  event['control_end_date']
		assert row.control_start_date.strftime('%Y-%m-%d') == event['control_start_date']
		assert row.control_geo_city == ''
		assert row.control_geo_country == ''
		assert row.control_geo_dma == ''
		assert row.control_geo_region == ''
		assert row.control_product_material == ''
		assert row.control_product_style == ''

	else:
		
		assert row.control_end_date.strftime('%Y-%m-%d') == event['control_end_date']
		assert row.control_start_date.strftime('%Y-%m-%d') == event['control_start_date']
		assert row.control_geo_city == ','.join(str(r) for v in event['control_geo_city'] for r in v)
		assert row.control_geo_country == "".join(event['control_geo_country'])
		assert row.control_geo_dma == ','.join(str(r) for v in event['control_geo_dma'] for r in v) 
		assert row.control_geo_region == ','.join(str(r) for v in event['control_geo_region'] for r in v)
		assert row.control_product_material == "".join(event['control_product_material'])
		assert row.control_product_style == "".join(event['control_product_style'])
