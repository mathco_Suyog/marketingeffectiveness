from modelRunner.algorithms.testControlSYDM import *
import pytest
from modelRunner.modelRunException import DataSufficiencyException
from modelRunner.models import cred
import pandas as pd
from modelRunner.models.fc_schema import TestControlMetrics,TestControlStatsMetrics
from datetime import datetime



@pytest.mark.parametrize('request_id',['ab132a3d-f8b6-4814-9aaf-e639ba087d36',	'dfadbb9f-2cc5-4fe0-885a-bcfe83cb9c1c',	'b777645b-e7b5-40e9-a600-5c75349f4198',	'23fd00b3-ecd8-48fe-92ff-58b5614beb59',	'5ae0b25b-4e98-46ab-9c39-c4f7e9a9c0f4',	'df5f8af0-161a-4fa0-8004-85ff090615a5',	'c0d62e49-a25a-470e-a3e1-64ab75357273',	'd1478d29-9529-4956-83c6-f8762e63975a',	'e5ece287-e23b-4a55-92b1-f6a706961e5a',	'168a4750-2616-415d-bda2-0f99733a745f',	'840b2e03-b195-48b3-b8a6-42980461dd3a',	'953205fd-a925-4c0f-ba4e-0b8af2118c85'])
def test_run_sydm(session, request_id):
    id_ = request_id
    
    input_df = pd.read_csv('../data/sydm_input.csv')  
    input_df['date'] = pd.to_datetime(input_df['date'])
    input_df = input_df[input_df['request_id']==id_]

    output_metrics = pd.read_csv('../data/sydm_metrics.csv')
    output_metrics = output_metrics[output_metrics.request_id ==  id_]

    output_stats = pd.read_csv('../data/sydm_stats.csv')
    output_stats = output_stats[output_stats.request_id == id_]

    performanceMetric = input_df.loc[input_df['request_id']==id_,'performance_metric'].iloc[0]
    run_sydm(input_df,performanceMetric,session)

    tc_metrics = (
                session
                .query(TestControlMetrics)
                .filter(TestControlMetrics.request_id == id_ )

                .first()
            )

    assert round(tc_metrics.abs_uplift,4) == round(output_metrics.abs_uplift.iloc[0],4)
    assert tc_metrics.abs_lift_per_day == output_metrics.abs_lift_per_day.iloc[0]
    assert tc_metrics.control_pre_vs_post == output_metrics.control_pre_vs_post.iloc[0]
    assert round(tc_metrics.pct_uplift,4) == round(output_metrics.pct_uplift.iloc[0],4)
    assert tc_metrics.t_test_p_value == output_metrics.t_test_p_value.iloc[0]
    assert tc_metrics.test_pre_vs_post == output_metrics.test_pre_vs_post.iloc[0]

    tc_stats_metrics = (
                       session
                       .query(TestControlStatsMetrics)
                       .filter(TestControlStatsMetrics.request_id == id_)
                    )
    stats_df = pd.read_sql(tc_stats_metrics.statement,tc_stats_metrics.session.bind)

    
    assert stats_df.loc[(stats_df['market']=='Test') & (stats_df['period']=='Pre') &
                        (stats_df['metric']=='Mean'),'value'].values[0]\
                         == output_stats.loc[(output_stats['market']=='Test') &
                                             (output_stats['period']=='Pre') &
                                             (output_stats['metric']=='Mean'),'value'].values[0]

    assert stats_df.loc[(stats_df['market']=='Test') & (stats_df['period']=='Pre') &
                        (stats_df['metric']=='SD'),'value'].values[0]\
                         == output_stats.loc[(output_stats['market']=='Test') &
                                             (output_stats['period']=='Pre') &
                                             (output_stats['metric']=='SD'),'value'].values[0]

    assert stats_df.loc[(stats_df['market']=='Test') & (stats_df['period']=='Pre') &
                        (stats_df['metric']=='t-stat'),'value'].values[0]\
                         == output_stats.loc[(output_stats['market']=='Test') &
                                             (output_stats['period']=='Pre') &
                                             (output_stats['metric']=='t-stat'),'value'].values[0]
    
    assert stats_df.loc[(stats_df['market']=='Test') & (stats_df['period']=='Pre') &
                        (stats_df['metric']=='p-value'),'value'].values[0]\
                         == output_stats.loc[(output_stats['market']=='Test') &
                                             (output_stats['period']=='Pre') &
                                             (output_stats['metric']=='p-value'),'value'].values[0]

    assert stats_df.loc[(stats_df['market']=='Test') & (stats_df['period']=='Post') &
                        (stats_df['metric']=='Mean'),'value'].values[0]\
                         == output_stats.loc[(output_stats['market']=='Test') &
                                             (output_stats['period']=='Post') &
                                             (output_stats['metric']=='Mean'),'value'].values[0]

    assert stats_df.loc[(stats_df['market']=='Test') & (stats_df['period']=='Post') &
                        (stats_df['metric']=='SD'),'value'].values[0]\
                         == output_stats.loc[(output_stats['market']=='Test') &
                                             (output_stats['period']=='Post') &
                                             (output_stats['metric']=='SD'),'value'].values[0]

    assert stats_df.loc[(stats_df['market']=='Test') & (stats_df['period']=='Post') &
                        (stats_df['metric']=='t-stat'),'value'].values[0]\
                         == output_stats.loc[(output_stats['market']=='Test') &
                                             (output_stats['period']=='Post') &
                                             (output_stats['metric']=='t-stat'),'value'].values[0]

    assert stats_df.loc[(stats_df['market']=='Test') & (stats_df['period']=='Post') &
                        (stats_df['metric']=='p-value'),'value'].values[0]\
                         == output_stats.loc[(output_stats['market']=='Test') &
                                             (output_stats['period']=='Post') &
                                             (output_stats['metric']=='p-value'),'value'].values[0]

    assert stats_df.loc[(stats_df['market']=='Control') & (stats_df['period']=='Pre') &
                        (stats_df['metric']=='Mean'),'value'].values[0]\
                         == output_stats.loc[(output_stats['market']=='Control') &
                                             (output_stats['period']=='Pre') &
                                             (output_stats['metric']=='Mean'),'value'].values[0]

    assert stats_df.loc[(stats_df['market']=='Control') & (stats_df['period']=='Pre') &
                        (stats_df['metric']=='SD'),'value'].values[0]\
                         == output_stats.loc[(output_stats['market']=='Control') &
                                             (output_stats['period']=='Pre') &
                                             (output_stats['metric']=='SD'),'value'].values[0]

    assert stats_df.loc[(stats_df['market']=='Control') & (stats_df['period']=='Post') &
                        (stats_df['metric']=='Mean'),'value'].values[0]\
                         == output_stats.loc[(output_stats['market']=='Control') &
                                             (output_stats['period']=='Post') &
                                             (output_stats['metric']=='Mean'),'value'].values[0]

    assert stats_df.loc[(stats_df['market']=='Control') & (stats_df['period']=='Post') &
                        (stats_df['metric']=='SD'),'value'].values[0]\
                         == output_stats.loc[(output_stats['market']=='Control') &
                                             (output_stats['period']=='Post') &
                                             (output_stats['metric']=='SD'),'value'].values[0]


def test_run_sydm_exception():
    with pytest.raises(Exception) as exc_info:
        run_sydm(pd.DataFrame(), None, None)
    exc_message = exc_info.value.args[0]
    assert exc_message == "Undefined performance metric encountered."


def test_data_exists_sydm_exception():
    with pytest.raises(DataSufficiencyException):
        sydm = Test_Control(pd.DataFrame(), 'total_customer')
        Test_Control.data_exists(sydm.df, sydm.performance_metric)

@pytest.mark.parametrize('request_id',['ab132a3d-f8b6-4814-9aaf-e639ba087d36',	'dfadbb9f-2cc5-4fe0-885a-bcfe83cb9c1c',	'b777645b-e7b5-40e9-a600-5c75349f4198',	'23fd00b3-ecd8-48fe-92ff-58b5614beb59',	'5ae0b25b-4e98-46ab-9c39-c4f7e9a9c0f4',	'df5f8af0-161a-4fa0-8004-85ff090615a5',	'c0d62e49-a25a-470e-a3e1-64ab75357273',	'd1478d29-9529-4956-83c6-f8762e63975a',	'e5ece287-e23b-4a55-92b1-f6a706961e5a',	'168a4750-2616-415d-bda2-0f99733a745f',	'840b2e03-b195-48b3-b8a6-42980461dd3a',	'953205fd-a925-4c0f-ba4e-0b8af2118c85'])
def test_sydm_data_exists(request_id):
    id_ = request_id
    input_df = pd.read_csv(r"..\data\sydm_input.csv")
    input_df['date'] = pd.to_datetime(input_df['date'])
    input_df = input_df[input_df['request_id']==id_]

    performanceMetric = input_df.loc[input_df['request_id']==id_,'performance_metric'].iloc[0]
    perf_met={'Sales':'total_sales','Orders':'total_orders','Customers':'total_customer','Visits':'total_visits'}
    performance_metric=perf_met.get(performanceMetric,None)

    output_records = pd.read_csv(r"..\data\sydm_records.csv")
    output_records = output_records[output_records['request_id'] == id_]

    sydm = Test_Control(input_df, performance_metric)
    df = sydm.data_exists(sydm.df, sydm.performance_metric)
    assert df.shape[0] == output_records.data_exists.values[0]

@pytest.mark.parametrize('request_id',['ab132a3d-f8b6-4814-9aaf-e639ba087d36',	'dfadbb9f-2cc5-4fe0-885a-bcfe83cb9c1c',	'b777645b-e7b5-40e9-a600-5c75349f4198',	'23fd00b3-ecd8-48fe-92ff-58b5614beb59',	'5ae0b25b-4e98-46ab-9c39-c4f7e9a9c0f4',	'df5f8af0-161a-4fa0-8004-85ff090615a5',	'c0d62e49-a25a-470e-a3e1-64ab75357273',	'd1478d29-9529-4956-83c6-f8762e63975a',	'e5ece287-e23b-4a55-92b1-f6a706961e5a',	'168a4750-2616-415d-bda2-0f99733a745f',	'840b2e03-b195-48b3-b8a6-42980461dd3a',	'953205fd-a925-4c0f-ba4e-0b8af2118c85'])
def test_sydm_subset(request_id):
    id_= request_id
    input_df = pd.read_csv(r"..\data\sydm_input.csv")
    input_df['date'] = pd.to_datetime(input_df['date'])
    input_df = input_df[input_df['request_id']==id_]

    output_records = pd.read_csv(r"..\data\sydm_records.csv")
    output_records = output_records[output_records['request_id']==id_]

    df_test_pre, df_test_post, df_control_pre, df_control_post = Test_Control.subset(input_df)
    assert df_test_pre.shape[0] == output_records.pre_test.values[0]
    assert df_test_post.shape[0] == output_records.post_test.values[0]
    assert df_control_pre.shape[0] == output_records.pre_control.values[0]
    assert df_control_post.shape[0] == output_records.post_control.values[0]

    




    



