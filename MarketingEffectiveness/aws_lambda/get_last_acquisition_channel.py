import json

from fc_schema import VisitFilters, TransactionFilters
from sqlalchemy import and_ , or_


def get_last_acquisition_channel(event, context, session):
    event_measure = event["queryStringParameters"]["measure_performance"]
    if event_measure.lower() == 'visits':
        query_table = VisitFilters
    else:
        query_table = TransactionFilters

    event_paid_free = event["multiValueQueryStringParameters"]["paid_free"]
    event_funnel_position = event["multiValueQueryStringParameters"]["funnel_position"]

    if (len(event_funnel_position) == 1) and ('All' in event_funnel_position):
        event_funnel_position_query = or_(
            query_table.funnel_position is not None,
            query_table.funnel_position is None
        )
    else:
        event_funnel_position_query = (query_table.funnel_position.in_(event_funnel_position))
    
    if (len(event_paid_free) == 1) and ('All' in event_paid_free):
        event_paid_free_query = or_(
            query_table.paid_free is None,
            query_table.paid_free is not None
        )
    else:
        event_paid_free_query = (query_table.paid_free.in_(event_paid_free))
        
    qry = (
            session
            .query(query_table.last_acquisition_channel)
            .filter(and_(
                event_paid_free_query,
                event_funnel_position_query
                ))
            .distinct().order_by(query_table.last_acquisition_channel).all())


    out_list = [ list(x) for x in qry if x[0] is not None ]
    # print(len(out_list))
    out_list.sort()
    out_list.insert(0, ['All'])

    out_list = json.dumps(out_list, separators=(',',':'))

    response =  {
        'statusCode':200,
        'body': out_list,
        'headers':{},
        'isBase64Encoded':False
        }

    return response
