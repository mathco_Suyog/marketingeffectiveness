import json
from fc_schema import TransactionFilters, VisitFilters


def get_channel_details(event, context, session):
    event_measure = event["queryStringParameters"]["measure_performance"]
    if event_measure.lower() == 'visits':
        query_table = VisitFilters
    else:
        query_table = TransactionFilters
    pass

    last_acquisition_channel_list = (
        session
        .query(query_table.last_acquisition_channel)
        .distinct()
        .order_by(query_table.last_acquisition_channel)
        .all()
    )
    last_acquisition_channel_list = [x[0] for x in last_acquisition_channel_list if x[0] is not None]
    last_acquisition_channel_list.insert(0, 'All')
    print(last_acquisition_channel_list)

    paid_free_list = (
        session
        .query(query_table.paid_free)
        .distinct()
        .order_by(query_table.paid_free)
        .all()
    )
    paid_free_list = [x[0] for x in paid_free_list if x[0] is not None]
    paid_free_list.insert(0, 'All')
    print(paid_free_list)

    funnel_position_list = (
        session
        .query(query_table.funnel_position)
        .distinct()
        .order_by(query_table.funnel_position)
        .all()
    )
    funnel_position_list = [x[0] for x in funnel_position_list if x[0] is not None]
    funnel_position_list.insert(0, 'All')
    print(funnel_position_list)

    if event_measure.lower() == 'visits':
        shop_type_list = []
    else:
        shop_type_list = (
            session
            .query(query_table.shop)
            .distinct()
            .order_by(query_table.shop)
            .all()
        )
        shop_type_list = [x[0] for x in shop_type_list if x[0] is not None]
    
    shop_type_list.insert(0, 'Total')
    print(shop_type_list)

    visitor_type_list = (
        session
        .query(query_table.visitor_status)
        .distinct()
        .order_by(query_table.visitor_status)
        .all()
    )
    visitor_type_list = [x[0] for x in visitor_type_list if x[0] is not None]
    visitor_type_list.insert(0, 'All')
    print(visitor_type_list)

    if event_measure.lower() == 'visits':
        customer_type_list = []
    else:    
        customer_type_list = (
            session
            .query(query_table.customer_status)
            .distinct()
            .order_by(query_table.customer_status)
            .all()
        )
        customer_type_list = [x[0] for x in customer_type_list if x[0] is not None]

    customer_type_list.insert(0, 'All')
    print(customer_type_list)


    final_dict = {
        'last_acquisition_channels': last_acquisition_channel_list,
        'paid_free_options': paid_free_list,
        'funnel_positions': funnel_position_list,
        'business_channels': shop_type_list,
        'visitor_types': visitor_type_list,
        'customer_types': customer_type_list,
    }

    response =  {
        'statusCode':200,
        'body': json.dumps(final_dict, separators=(',',':')),
        'headers':{},
        'isBase64Encoded':False
        }

    return response
