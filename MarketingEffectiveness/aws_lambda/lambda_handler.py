from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from snowflake.sqlalchemy import URL
from fc_schema import Base
import cred

from get_countries import get_countries
from get_regions import get_regions
from get_dma_list import get_dma_list
from get_cities import get_cities
from get_materials import get_materials
from get_style import get_style
from get_product_categories import get_product_categories, get_product_sub_categories
from get_saved_filters import get_saved_filters
from get_request_info import get_iterations, get_request_status
from get_looker_viz_uri import get_looker_viz_uri, get_model_run_techniques
from get_basic_info import get_channel_details
from get_paid_free import get_paid_free
from get_funnel_position import get_funnel_position
from get_last_acquisition_channel import get_last_acquisition_channel


def lambda_handler (event, context):
    print(event)
    engine = create_engine(URL(
        account=cred.account,
        user=cred.user,
        password=cred.password,
        database=cred.database,
        schema=cred.schema,
        warehouse=cred.warehouse,
        role=cred.role
    ), echo=False)

    Base.metadata.bind = engine
    DBSession = sessionmaker(bind=engine)
    session = DBSession()
    session.execute('alter session set ABORT_DETACHED_QUERY=TRUE')

    if event['httpMethod'] == 'GET':
        if event['resource'] == '/getallcountries':
            return get_countries(event, context, session) 
        elif event['resource'] == '/getregionslist':
            return get_regions(event, context, session)
        elif event['resource'] == '/getdmalist':
            return get_dma_list(event, context, session)
        elif event['resource'] == '/getcitieslist':
            return get_cities(event, context, session)
        elif event['resource'] == '/getproductmaterials':
            return get_materials(event, context, session)
        elif event['resource'] == '/getproductstyle':
            return get_style(event, context, session) 
        elif event['resource'] == '/getproductcategories':
            return get_product_categories(event, context, session)
        elif event['resource'] == '/getproductsubcategory':
            return get_product_sub_categories(event, context, session)
        elif event['resource'] == '/getsavedfilters':
            return get_saved_filters(event, context, session)
        elif event['resource'] == '/getiterationlist':
            return get_iterations(event, context, session)
        elif event['resource'] == '/requeststatus':
            return get_request_status(event, context, session)
        elif event['resource'] == '/getlookervizuri':
            return get_looker_viz_uri(event, context, session)
        elif event['resource'] == '/getmodelruntechniques':
            return get_model_run_techniques(event, context, session)
        elif event['resource'] == '/getchanneldetails':
            return get_channel_details(event, context, session)
        elif event['resource'] == '/getpaidfree':
            return get_paid_free(event, context, session)
        elif event['resource'] == '/getfunnelposition':
            return get_funnel_position(event, context, session)
        elif event['resource'] == '/getlastacquisitionchannel':
            return get_last_acquisition_channel(event, context, session)

    return {
        "statusCode":400,
        "body":"Endpoint not defined"
    }

