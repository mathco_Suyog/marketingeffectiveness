import json
from fc_schema import VisitFilters, TransactionFilters
from sqlalchemy import and_, or_

def get_funnel_position(event, context, session):
    event_measure = event["queryStringParameters"]["measure_performance"]
    if event_measure.lower() == 'visits':
        query_table = VisitFilters
    else:
        query_table = TransactionFilters

    event_paid_free = event["queryStringParameters"]["paid_free"]

    if (event_paid_free == 'All'):
        event_paid_free_query = or_(
            query_table.paid_free == None,
            query_table.paid_free != None
        )
    else:
        event_paid_free_query = (query_table.paid_free == event_paid_free)
        
    qry = session.query(query_table.funnel_position).filter(event_paid_free_query).distinct().order_by(query_table.funnel_position).all()

    out_list = [ list(x) for x in qry if x[0] is not None ]
    out_list.sort()
    out_list.insert(0, ['All'])

    out_list = json.dumps(out_list, separators=(',',':'))

    response =  {
        'statusCode':200,
        'body': out_list,
        'headers':{},
        'isBase64Encoded':False
        }

    return response


