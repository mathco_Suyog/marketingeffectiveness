from .dataFetchVisits import actual_expected_data_fetcher_visits, last_year_same_market_data_fetcher_visits, sydm_visits
from .dataFetchTransactions import actual_expected_data_fetcher_transactions, \
    last_year_same_market_data_fetcher_transactions, sydm_transactions


def actual_expected_data_fetcher(filters_row, session):
    if filters_row.performance_metric in ['Visits', 'Conversion Rate']:
        return actual_expected_data_fetcher_visits(filters_row, session)
    else:
        return actual_expected_data_fetcher_transactions(filters_row, session)


def last_year_same_market_data_fetcher(filters_row, session):
    if filters_row.performance_metric in ['Visits', 'Conversion Rate']:
        return last_year_same_market_data_fetcher_visits(filters_row, session)
    else:
        return last_year_same_market_data_fetcher_transactions(filters_row, session)


def same_year_diff_market_data_fetcher(filters_row, session):
    if filters_row.performance_metric in ['Visits', 'Conversion Rate']:
        return sydm_visits(filters_row, session)
    else:
        return sydm_transactions(filters_row, session)


def data_fetcher(filters_row, session):
    """
    Reads filters from Snowflake based on the request_id (stored in event['queryStringParameters']['request_id']), and based on the performance metric 
    and comparison_technique, retrieves the filtered input dataset from Snowflake and stores it to S3.
    """       
    if filters_row.comparison_technique == 'Same Year - Different Market':
        df = same_year_diff_market_data_fetcher(filters_row, session)
    elif filters_row.comparison_technique == 'Same Market - Last Year':
        df = last_year_same_market_data_fetcher(filters_row, session)
    elif filters_row.comparison_technique == 'Actual vs. Expected':
        df = actual_expected_data_fetcher(filters_row, session)
    else:
        return None

    df['request_id'] = filters_row.request_id
    df['performance_metric'] = filters_row.performance_metric
    df['comparison_technique'] = filters_row.comparison_technique

    return df, filters_row.performance_metric
