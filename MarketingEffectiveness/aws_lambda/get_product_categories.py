import json
from fc_schema import TransactionFilters


def get_product_categories(event, context, session):
    """
    Returns the product categories in the transaction data.
    Visits do not have a product category associated with them so calling this function with "Visits" as performance metric would have no effect.
    """
    
    qry = session.query(TransactionFilters.product_category).distinct().order_by(TransactionFilters.product_category).all()

    out_list = [ list(x) for x in qry if x[0] is not None]
    out_list.sort()

    out_list.insert(0, ['All'])

    out_list = json.dumps(out_list, separators=(',',':'))

    response =  {
        'statusCode':200,
        'body': out_list,
        'headers':{},
        'isBase64Encoded':False
        }

    return response

def get_product_sub_categories(event, context, session):
    """
    Returns the sub categories in the transaction data for a given product.
    Visits do not have a product category associated with them so calling this function with "Visits" as performance metric would have no effect.
    """
    event_prd = event["queryStringParameters"]["product_category"]

    if event_prd == 'All':
        event_prd_cat = session.query(TransactionFilters.product_category).distinct().all()
        event_prd_cat = [x[0] for x in event_prd_cat if x[0] != None]
    else:
        event_prd_cat = []
        event_prd_cat.append(event_prd)
    
    qry = ( session
            .query(TransactionFilters.product_subcategory)
            .filter(TransactionFilters.product_category.in_(event_prd_cat))
            .distinct()
            .order_by(TransactionFilters.product_subcategory)
            .all()
    )

    out_list = [ list(x) for x in qry if x[0] is not None]
    out_list.sort()

    out_list.insert(0, ['All'])

    out_list = json.dumps(out_list, separators=(',',':'))

    response =  {
        'statusCode':200,
        'body': out_list,
        'headers':{},
        'isBase64Encoded':False
        }

    return response