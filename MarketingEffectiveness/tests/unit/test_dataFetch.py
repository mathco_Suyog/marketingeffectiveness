
import pandas as pd
import pandas.api.types as ptypes
from modelRunner.algorithms.dataFetchVisits import actual_expected_data_fetcher_visits, last_year_same_market_data_fetcher_visits, sydm_visits
from modelRunner.algorithms.dataFetchTransactions import actual_expected_data_fetcher_transactions, \
    last_year_same_market_data_fetcher_transactions, sydm_transactions
import pytest
from modelRunner.models import fc_schema
from dateutil.relativedelta import relativedelta


@pytest.mark.parametrize('request_id',['b7ade7cc-953b-4421-a019-487014a89c2d'])
def test_ave(session, request_id,setup_requestfilters):

	filters_row = session.query(fc_schema.RequestFilters).filter(
        fc_schema.RequestFilters.request_id == request_id).one()

	
	if filters_row.performance_metric in ['Visits', 'Conversion Rate']:
		df = actual_expected_data_fetcher_visits(filters_row, session)
		#df.to_csv('df_ave_visits.csv', encoding='utf-8')
		df2 = pd.read_csv(r'../data/ave_visits.csv')
	else:
		df = actual_expected_data_fetcher_transactions(filters_row, session)
		#df.to_csv('df_ave_transactions.csv', encoding='utf-8')
		df2 = pd.read_csv(r'../data/ave_transactions.csv')
	
	assert len(df) == len(df2)	
	dateAVE = (df['date'] >= filters_row.test_start_date -
                                          relativedelta(year=filters_row.test_start_date.year-2, 
                                          days=(filters_row.test_end_date - filters_row.test_start_date).days+1)
										  ) & (df['date'] <= filters_row.test_end_date)
	for row in dateAVE:
		assert row
	assert ptypes.is_string_dtype(df['city'])
	assert ptypes.is_numeric_dtype(df['total_orders'])


@pytest.mark.parametrize('request_id',['3d2bf2d0-aa75-44fd-b243-3908f803058d', '6b1919d7-e88f-4bc8-9876-829b310e65db'])
def test_smly(session, request_id, setup_requestfilters):

	filters_row = session.query(fc_schema.RequestFilters).filter(
        fc_schema.RequestFilters.request_id == request_id).one()

	if filters_row.performance_metric in ['Visits', 'Conversion Rate']:
		df = last_year_same_market_data_fetcher_visits(filters_row, session)
		#df.to_csv('smly_visits.csv', encoding='utf-8')
		df2 = pd.read_csv(r'../data/smly_visits.csv')
	else:
		df = last_year_same_market_data_fetcher_transactions(filters_row, session)
		#df.to_csv('smly_transactions.csv', encoding='utf-8')
		df2 = pd.read_csv(r'../data/smly_transactions.csv')

	assert len(df) == len(df2)
	for row in df["'flag'"]:
		assert row != 'ignore'
	assert ptypes.is_string_dtype(df['city'])
	assert ptypes.is_numeric_dtype(df['total_orders'])

@pytest.mark.parametrize('request_id',['8aab27c5-f0d5-4731-9f5d-b52f57289fe2', 'e49d8b7c-b910-417a-82e8-cd3c4a528df3'])
def test_sydm(session, request_id, setup_requestfilters):

	filters_row = session.query(fc_schema.RequestFilters).filter(
        fc_schema.RequestFilters.request_id == request_id).one()
	
	if filters_row.performance_metric in ['Visits', 'Conversion Rate']:
		df = sydm_visits(filters_row, session)
		df2 = pd.read_csv(r'../data/sydm_visits.csv')
	else:
		df = sydm_transactions(filters_row, session)
		#df.to_csv('sydm_transactions.csv', encoding='utf-8')
		df2 = pd.read_csv(r'../data/sydm_transactions.csv')

	assert len(df) == len(df2)
	dateSYDM = (df['date'] >= filters_row.control_start_date) & (df['date'] <= filters_row.test_end_date)
	for row in dateSYDM:
		assert row
	assert ptypes.is_string_dtype(df['city'])
	assert ptypes.is_numeric_dtype(df['total_orders'])