from snowflake.sqlalchemy import URL
from sqlalchemy import create_engine
import pandas as pd
from looker_api import LookerLink
import cred
import time
pd.set_option('display.max_columns', None)

class SnowflakeConn(object):
    """
    Create a connection to Snowflake.

    Has operations to insert and fetch data, run ad-hoc queries.
    """


    def __init__(self, creds):
        """Initialize sqlalchemy link."""
        self.engine = create_engine(
            URL(
                account=creds.input_data_account,
                user=creds.input_data_user,
                password=creds.input_data_password,
                database=creds.input_data_database,
            )
        )

        self.connection = self.engine.connect()

    def run_query(self, query, op_format="pandas"):
        """Run a query using the conn."""
        if op_format == "pandas":
            df = pd.read_sql_query(query, self.connection)
        else:
            df = pd.DataFrame()
        return df

    def close(self):
        """Close active link."""
        self.connection.close()
 


def process_request(event, context = None):    
    start_time = time.time()
    print("Execution Process Started")
    look_id = cred.filter_look
    token = cred.looker_token
    host = cred.looker_url
    secret = cred.looker_secret
    lifttool = {'database' : 'lift_stats', 'schema':'public'}



    link = LookerLink(token=token, secret=secret, host=host)
    print("Connected to Looker to fetch query")
    query_id=link.get_look(look_id,"query_id")["query_id"]

    query_body = link.get_query(query_id)
        

    xx=link.run_inline_query(body=query_body,op_format="sql")
    snow_query = xx.content.decode('utf-8')

    # In case you face issues, comment out the below two snow_query lines usign ctrl+1
    snow_query = snow_query.replace('LIMIT 500', '')


    # run the sql query obtained from looker in snowflake
    dbobj=SnowflakeConn(cred)
    dbobj.run_query('ALTER SESSION SET ABORT_DETACHED_QUERY=TRUE')

    txn_ex = dbobj.run_query("select count(*) from {db_name}.{schema}.transactions_input".format(db_name = lifttool['database'], schema = lifttool['schema']))
    txn_ex = txn_ex.values.tolist()


    try:
        start_time_raw_input = time.time()
        dbobj.run_query('drop table if exists {db_name}.{schema}.raw_input_table'.format(db_name = lifttool['database'], schema = lifttool['schema']))
        print("Previous raw_input_table (if any) got dropped")

        snow_query1='create table {db_name}.{schema}.raw_input_table as'.format(db_name = lifttool['database'], schema = lifttool['schema'])+snow_query
        dbobj.run_query(snow_query1)
        end_time_raw_input = time.time()
        print("New raw_input_table got created")
        print("Time taken for raw_input_table to run: {} minutes".format((end_time_raw_input-start_time_raw_input)/60))

        start_time_inp_txn = time.time()
        dbobj.run_query('drop table if exists {db_name}.{schema}.input_transactions_table'.format(db_name = lifttool['database'], schema = lifttool['schema']))
        print("Previous input_transactions_table (if any) got dropped")

        snow_query2='''create table {db_name}.{schema}.input_transactions_table as
            with converted_visits as (
            select distinct to_date(a.started_at_ts_utc) as date, a.order_name_first_transaction as order_name, a.channel_grouping as last_acq_channel,
            case when a.total_new_visits =1 then 'New' else 'Returning' end as Visitor_status,b.paid_free,b.funnel_position
            from birdfacts.public.fact_online_sessions as a left join birdfacts.wip.ga_channel_map as c
            on a.channel_grouping=c.ga_default_grouping
            left join birdfacts.wip.fact_account_number as b
            on c.ga_channel_grouped=b.ga_channel_grouped
            inner join {db_name}.{schema}.raw_input_table as d
            on trim(to_varchar(a.order_name_first_transaction))=trim(to_varchar(d."dim_customer.order_name"))),

            cust_stat as (
            select * from (
            select distinct "dim_customer.customer_id",
                            "dim_customer.order_fulfilled_timestamp_1",
                            "fact_sales.is_new_customer",
                            row_number() over(partition by "dim_customer.customer_id" 
                                                order by "dim_customer.order_fulfilled_timestamp_1")as row_num
            from {db_name}.{schema}.raw_input_table
            where "dim_customer.shopify_app_id" !=1354745 
            and "dim_customer.order_name" is not null 
            and "dim_customer.order_name" not like 'XR-%' 
            and coalesce("dim_address.region",'') not like 'Armed%'
            order by "dim_customer.customer_id","dim_customer.order_fulfilled_timestamp_1")
            where row_num=1)

            select "dim_date_shops_products.local_date" as DATE,
                "dim_address.country" as COUNTRY,
                "dim_address.region" as REGION,
                DMA,
                City as CITY,
                "dim_date_shops_products.taxonomy_category" as PRODUCT_CATEGORY,
                "dim_date_shops_products.taxonomy_subcategory" as PRODUCT_SUBCATEGORY,
                "dim_date_shops_products.material_family" as MATERIAL,
                "dim_date_shops_products.taxonomy_style" as STYLE,
                customer_status as CUSTOMER_STATUS,
                VISITOR_STATUS,
                "dim_profit_center.sales_channel" as SHOP,
                last_acq_channel as LAST_ACQUISITION_CHANNEL,
                paid_free as PAID_FREE,
                Funnel_Position as FUNNEL_POSITION,
                sum("fact_sales.gross_product_sales") as TOTAL_SALES,
                count(distinct "dim_customer.order_id") as TOTAL_ORDERS,
                count(distinct "dim_customer.customer_id") as TOTAL_CUSTOMER
            from(
            select a.*, case when b."fact_sales.is_new_customer" ='Yes' then 'New'
                            when b."fact_sales.is_new_customer" is null then 'Returning'
                            else 'Returning'
                            end as customer_status,
                        case when a."dim_address.country" like '%United States%' then c.dma else null end as DMA,
            case when a."dim_address.country" like '%United States%' then c.city else a."dim_address.city" end as City,
            d.visitor_status,d.last_acq_channel,d.paid_free, d.funnel_position
            from(
            select *, case when length("dim_address.postal_code")=3 then lpad("dim_address.postal_code",5,0)
                        when length("dim_address.postal_code")=4 then lpad("dim_address.postal_code",5,0)
                        when length("dim_address.postal_code")=6 then left("dim_address.postal_code",5)
                        when length("dim_address.postal_code")=7 then left("dim_address.postal_code",5)
                        when length("dim_address.postal_code")=8 then left("dim_address.postal_code",5)
                        when length("dim_address.postal_code")=9 then left("dim_address.postal_code",5)
                        when length("dim_address.postal_code")=10 then left("dim_address.postal_code",5)
                        else "dim_address.postal_code" end as New_Postal_Code
            from {db_name}.{schema}.raw_input_table where "dim_customer.shopify_app_id" !=1354745 and
            "dim_customer.order_name" is not null and "dim_customer.order_name" not like 'XR-%' and
            coalesce("dim_address.region",'') not like 'Armed%') as a left join cust_stat as b
            on a."dim_customer.customer_id"=b."dim_customer.customer_id"
            and a."dim_customer.order_fulfilled_timestamp_1"=b."dim_customer.order_fulfilled_timestamp_1"
            left join {db_name}.{schema}.dma_mapping as c
            on a.new_postal_code=c.zip
            left join converted_visits as d
            on trim(to_varchar(a."dim_customer.order_name"))=trim(to_varchar(d.order_name)))
            group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15
            order by date desc '''.format(db_name = lifttool['database'], schema = lifttool['schema'])
        

        dbobj.run_query(snow_query2)
        end_time_inp_txn = time.time()
        print("input_transactions_table got created")
        print("Total time taken to create the input_transactions_table:{} minutes".format((end_time_inp_txn-start_time_inp_txn)/60))

        start_time_txn_inp = time.time()
        dbobj.run_query('''truncate table {db_name}.{schema}.transactions_input'''.format(db_name = lifttool['database'], schema = lifttool['schema']))
        print("transactions_input table got truncated")

        snow_query3='''insert into {db_name}.{schema}.transactions_input (date, country, region, dma, city, product_category, product_subcategory, material, style, customer_status, visitor_status, shop, last_acquisition_channel,
            paid_free, funnel_position, total_sales, total_orders, total_new_customer, total_old_customer, total_customer)
        select date, country, region, dma, city, product_category, product_subcategory, material, style, customer_status, visitor_status, shop, last_acquisition_channel,
            paid_free, funnel_position, total_sales, total_orders, 0 as total_new_customer, 0 as total_old_customer, total_customer
        from {db_name}.{schema}.input_transactions_table'''.format(db_name = lifttool['database'], schema = lifttool['schema'])
        dbobj.run_query(snow_query3)
        end_time_txn_inp = time.time()
        print("transactions_input table got filled")
        print("Time taken to truncate and insert values into transactions_input:{} minutes".format((end_time_txn_inp-start_time_txn_inp)/60))

        start_time_txn_filters = time.time()
        dbobj.run_query('''truncate table {db_name}.{schema}.transaction_filters'''.format(db_name = lifttool['database'], schema = lifttool['schema']))
        print("transaction_filters table got truncated")
        snow_query4='''insert into {db_name}.{schema}.transaction_filters (country, region, dma, city, product_category, product_subcategory, material, style, customer_status, 
                                                                visitor_status, shop, last_acquisition_channel, paid_free, funnel_position)
        select distinct country, region, dma, city, product_category, product_subcategory, material, style, customer_status, visitor_status, shop, last_acquisition_channel, paid_free, funnel_position
        from {db_name}.{schema}.transactions_input
        order by 1,2,3,4,5,6,7,8,9,10,11,12,13,14'''.format(db_name = lifttool['database'], schema = lifttool['schema'])
        dbobj.run_query(snow_query4)
        end_time_txn_filters = time.time()
        print("transaction_filters table got filled")
        print("Time taken to truncate and insert values into transaction_filters:{} minutes".format((end_time_txn_filters-start_time_txn_filters)/60))
    except Exception as e:
        print(e)
        return
    end_time=time.time()

    txn_present = dbobj.run_query("select count(*) from {db_name}.{schema}.transactions_input".format( db_name = lifttool['database'], schema = lifttool['schema']))
    txn_present = txn_present.values.tolist()

    txn_records = txn_present[0][0] - txn_ex[0][0]
    print("The time taken to run the code {0} minutes".format((end_time-start_time)/60))
    print("The total number records appended today for transactions:{0}".format(txn_records))
if __name__=="__main__":
    process_request("etl_transactions")

