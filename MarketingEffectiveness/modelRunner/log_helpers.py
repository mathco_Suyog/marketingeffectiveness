from .models.fc_schema import RequestLog, RequestDetails
import datetime


def request_add_log_message(request_id, message_text, message_code, session):
    #session.begin()
    try:
        row_request_log = RequestLog(
            request_id=request_id,
            log_message=message_text,
            log_status_code=message_code,
            _row_created_at=datetime.datetime.utcnow(),
            _row_updated_at=datetime.datetime.utcnow()
        )
        session.add(row_request_log)
        session.commit()
    except:
        session.rollback()
       

def request_processing_start(request_id, session):
    #session.begin()
    try:
        row_request_details = (
            session
            .query(RequestDetails)
            .filter(RequestDetails.request_id == request_id)
            .order_by(RequestDetails.created_at_ts_utc.desc())
            .one()
        )
        row_request_details.status_code = 1
        row_request_details.process_start_at_ts_utc = datetime.datetime.utcnow()
        session.commit()
    except:
        session.rollback()
      


def model_run_completed(request_id, session):
    # Update request details
    #session.begin()
    try:
        row_request_details = (
            session
            .query(RequestDetails)
            .filter(RequestDetails.request_id == request_id)
            .order_by(RequestDetails.created_at_ts_utc.desc())
            .one()
        )
        row_request_details.models_run_completed += 1
        session.commit()
    except:
        session.rollback()
       

def request_processing_complete(request_id, session):
    #session.begin()
    try:
        row_request_details = (
            session
            .query(RequestDetails)
            .filter(RequestDetails.request_id == request_id)
            .order_by(RequestDetails.created_at_ts_utc.desc())
            .one()
        )
        if row_request_details.models_run_completed >= row_request_details.models_run_total:
            row_request_details.status_code = 0
        else:
            row_request_details.status_code = 3

        row_request_details.process_end_at_ts_utc = datetime.datetime.utcnow()
        session.commit()
        return (f'{row_request_details.models_run_completed} out of {row_request_details.models_run_total} '
            f'completed processing')
    except:
        session.rollback()